using AuRooM.Application.Common.Models;
using AuRooM.Application.Specifications;
using AuRooM.Application.Specifications.UserSpecifications.Include;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Users.Queries.GetUsersList
{
    public class GetUsersListQueryHandler : IRequestHandler<GetUsersListQuery, Result<UserListVM>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetUsersListQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<UserListVM>> Handle(GetUsersListQuery request, CancellationToken cancellationToken)
        {
            var users = await _unitOfWork.Users.GetAsync(
                            include: new UserIncludeGroupAndRoles(),
                            paging: new PagingSpecification(request.Page, request.PageSize));

            var usersForView = users.Select(_mapper.Map<UserVM>)
                .ToList();

            var usersCount = await _unitOfWork.Users.CountAsync();
            var model = new UserListVM()
            {
                UserList = usersForView,
                PaginationInfo = new PaginationInfo(request.Page, usersCount, request.PageSize)
            };

            return Result<UserListVM>.Success(model);
        }
    }
}