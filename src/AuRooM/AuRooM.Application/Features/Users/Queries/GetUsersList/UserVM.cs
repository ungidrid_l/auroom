using System;
using System.ComponentModel.DataAnnotations;

namespace AuRooM.Application.Features.Users.Queries.GetUsersList
{
    public class UserVM
    {
        public Guid Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string DocumentInfo { get; set; }

        public string Group { get; set; }

        [Required]
        public string Role { get; set; }

        [Required]
        public string Status { get; set; }
    } 
}