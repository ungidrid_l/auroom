using AuRooM.Application.Common.Models;
using MediatR;

namespace AuRooM.Application.Features.Users.Queries.GetUsersList
{
    public class GetUsersListQuery : IRequest<Result<UserListVM>>
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}