using AuRooM.Application.Common.Models;
using System.Collections.Generic;

namespace AuRooM.Application.Features.Users.Queries.GetUsersList
{
    public class UserListVM
    {
        public IEnumerable<UserVM> UserList { get; set; }
        public PaginationInfo PaginationInfo { get; set; }
    }
}