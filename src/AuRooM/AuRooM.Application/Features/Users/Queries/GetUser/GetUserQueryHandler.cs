using AuRooM.Application.Common.Models;
using AuRooM.Application.Specifications.UserSpecifications.Filter;
using AuRooM.Application.Specifications.UserSpecifications.Include;
using AuRooM.Common.Helpers;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AuRooM.Domain.Settings;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Users.Queries.GetUser
{
    public class GetUserQueryHandler : IRequestHandler<GetUserQuery, Result<EditUserVM>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly RoleSettings _roleSettings;

        public GetUserQueryHandler(IUnitOfWork unitOfWork, IMapper mapper, IOptions<RoleSettings> roleSettings)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _roleSettings = roleSettings.Value;
        }

        public async Task<Result<EditUserVM>> Handle(GetUserQuery request, CancellationToken cancellationToken)
        {
            var user = await _unitOfWork.Users.FirstOrDefaultAsync(new UserByIdFilter(request.Id), new UserIncludeRoles());
            var editUserVm = _mapper.Map<EditUserVM>(user);

            editUserVm.Groups = (await _unitOfWork.AcademicGroups.GetKeyValuePairsAsync())
                .Select(_mapper.Map<SelectListItem>)
                .ToList();

            editUserVm.Roles = (await _unitOfWork.Roles.GetKeyValuePairsAsync())
                .Select(_mapper.Map<SelectListItem>)
                .ToList();

            editUserVm.Statuses = EnumHelper.EnumAsKeyValuePair<UserStatus>()
                .Select(_mapper.Map<SelectListItem>)
                .ToList();

            return Result<EditUserVM>.Success(editUserVm);
        }
    }
}