using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AuRooM.Application.Features.Users.Queries.GetUser
{
    public class EditUserVM
    {
        public Guid Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string DocumentInfo { get; set; }

        public Guid? GroupId { get; set; }

        [Required]
        public Guid RoleId { get; set; }

        [Required]
        public int Status { get; set; }

        public IEnumerable<SelectListItem> Roles { get; set; }

        public IEnumerable<SelectListItem> Groups { get; set; }
        
        public IEnumerable<SelectListItem> Statuses { get; set; }
    }
}