using AuRooM.Application.Common.Models;
using MediatR;
using System;

namespace AuRooM.Application.Features.Users.Queries.GetUser
{
    public class GetUserQuery : IRequest<Result<EditUserVM>>
    {
        public Guid Id { get; set; }
    }
}