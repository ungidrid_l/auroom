using AuRooM.Application.Common.Extensions;
using AuRooM.Application.Common.Models;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Settings;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Users.Commands.Edit
{
    public class EditUserCommandHandler : IRequestHandler<EditUserCommand, Result>
    {
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;
        private readonly RoleSettings _roleSettings;

        public EditUserCommandHandler(UserManager<User> userManager, IMapper mapper, IOptions<RoleSettings> roleSettings)
        {
            _userManager = userManager;
            _mapper = mapper;
            _roleSettings = roleSettings.Value;
        }

        public async Task<Result> Handle(EditUserCommand command, CancellationToken cancellationToken)
        {
            if ((command.RoleId == _roleSettings.TeacherRoleId || command.RoleId == _roleSettings.AdminRoleId)  
                && command.GroupId.HasValue)
                return Result.Failure("Selected role cannot have academic group selected");
           
            var userToUpdate = await _userManager.FindByIdAsync(command.Id.ToString());
            var currentRole = (await _userManager.GetRolesAsync(userToUpdate)).First();

            await _userManager.RemoveFromRoleAsync(userToUpdate, currentRole);
            
            var user = _mapper.Map(command, userToUpdate);
            await _userManager.UpdateNormalizedEmailAsync(user);
            await _userManager.UpdateNormalizedUserNameAsync(user);
            user.UserRoles.Add(new UserRole(user.Id, command.RoleId));

            var result = await _userManager.UpdateAsync(user);
            if (!result.Succeeded)
                Result.Failure(result.GetErrors());

            return Result.Success();
        }
    }
}