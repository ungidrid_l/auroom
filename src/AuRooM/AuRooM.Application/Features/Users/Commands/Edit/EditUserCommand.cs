using AuRooM.Application.Common.Models;
using MediatR;
using System;

namespace AuRooM.Application.Features.Users.Commands.Edit
{
    public class EditUserCommand : IRequest<Result>
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }
        
        public string DocumentInfo { get; set; }

        public Guid RoleId { get; set; }

        public Guid? GroupId { get; set; }

        public string Status { get; set; }
    }
}