using AuRooM.Application.Common.Models;
using MediatR;
using System;

namespace AuRooM.Application.Features.Users.Commands.Delete
{
    public class DeleteUserCommand : IRequest<Result>
    {
        public Guid Id { get; set; }
    }
}