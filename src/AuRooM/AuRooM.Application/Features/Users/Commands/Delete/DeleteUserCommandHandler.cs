using AuRooM.Application.Common.Models;
using AuRooM.Application.Specifications.UserSpecifications.Filter;
using AuRooM.Application.Specifications.UserSpecifications.Include;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;
using AuRooM.Domain.Settings;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Users.Commands.Delete
{
    public class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand, Result>
    {
        private readonly IUnitOfWork _unitOfWork;        
        private readonly RoleSettings _roleSettings;

        public DeleteUserCommandHandler(IUnitOfWork unitOfWork, IOptions<RoleSettings> roleSettings)
        {
            _unitOfWork = unitOfWork;            
            _roleSettings = roleSettings.Value;
        }

        public async Task<Result> Handle(DeleteUserCommand command, CancellationToken cancellationToken)
        {
            var userToDelete = await _unitOfWork.Users.FirstOrDefaultAsync(new UserByIdFilter(command.Id), new UserIncludeRoles());
            var userRoleIds = userToDelete.UserRoles.Select(x => x.RoleId);

            if (userRoleIds.Contains(_roleSettings.AdminRoleId))
                return Result.Failure("Admin can't delete himself");

            return Result.Success();
        }
    }
}