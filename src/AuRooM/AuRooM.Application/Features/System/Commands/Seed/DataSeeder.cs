using AuRooM.Domain.Entities;
using AuRooM.Domain.Settings;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.System.Commands.Seed
{
    public class DataSeeder
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleSettings _roleSettings;
        private readonly RoleManager<Role> _roleManager;

        public DataSeeder(RoleManager<Role> roleManager, UserManager<User> userManager, IOptions<RoleSettings> roleSettings)
        {
            _userManager = userManager;
            _roleSettings = roleSettings.Value;
            _roleManager = roleManager;
        }

        public async Task SeedAllAsync()
        {
            await new RoleSeeder(_roleManager, _roleSettings).SeedAsync();
            await new UserSeeder(_userManager, _roleSettings).SeedAsync();
            await new PermissionsSeeder(_roleManager).SeedAsync();
        }
    }
}