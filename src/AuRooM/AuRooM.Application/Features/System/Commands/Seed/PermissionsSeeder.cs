﻿using AuRooM.Application.Common.Permissions;
using AuRooM.Common.Constants;
using AuRooM.Common.Extensions;
using AuRooM.Domain.Constants;
using AuRooM.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.System.Commands.Seed
{
    public class PermissionsSeeder
    {
        private readonly RoleManager<Role> _roleManager;
        private readonly IDictionary<string, IEnumerable<string>> _currentPermissions;

        public PermissionsSeeder(RoleManager<Role> roleManager)
        {
            _roleManager = roleManager;
            _currentPermissions = new Dictionary<string, IEnumerable<string>>
            {
                [UserRoles.Admin] = new[]
                {
                    UserPermissions.ChangeStatus,
                    UserPermissions.LogOut,
                    UserPermissions.ViewUsers,
                    UserPermissions.EditUsers,
                    UserPermissions.RemoveUsers,
                    EventPermissions.RequestCreation,
                    EventPermissions.Subscribe,
                    EventPermissions.RequestChanges,
                    EventPermissions.ApproveChanges,
                    EventPermissions.BrowseAgenda,
                    EventPermissions.ViewEventRequests
                },
                [UserRoles.Teacher] = new[]
                {
                    UserPermissions.LogOut,
                    EventPermissions.RequestCreation,
                    EventPermissions.Subscribe,
                    EventPermissions.RequestChanges,
                    EventPermissions.ApproveChanges,
                    EventPermissions.BrowseAgenda,
                    EventPermissions.ViewRelatedEventRequests,
                    EventPermissions.ViewEventRequests
                },
                [UserRoles.Prefect] = new[]
                {
                    UserPermissions.LogOut,
                    EventPermissions.RequestCreation,
                    EventPermissions.Subscribe,
                    EventPermissions.RequestChanges,
                    EventPermissions.BrowseAgenda
                },
                [UserRoles.Student] = new[]
                {
                    UserPermissions.LogOut,
                    EventPermissions.RequestCreation,
                    EventPermissions.Subscribe,
                    EventPermissions.BrowseAgenda
                }
            };
        }

        public async Task SeedAsync()
        {
            var existingRoles = _roleManager.Roles.ToArray();
            await RemoveUnusedPermissionsAsync(existingRoles);
            await AddCurrentPermissionsAsync(existingRoles);
        }

        private async Task RemoveUnusedPermissionsAsync(IEnumerable<Role> roles)
        {
            foreach (var role in roles)
            {
                var existingPermissionClaims = (await _roleManager.GetClaimsAsync(role))
                    .Where(x => x.Type == CustomClaimTypes.Permission)
                    .ToArray();

                var permissionsToRemove = existingPermissionClaims.Excpet(_currentPermissions[role.Name], (x, y) => x.Value == y);

                foreach (var claim in permissionsToRemove)
                {
                    await _roleManager.RemoveClaimAsync(role, claim);
                }
            }
        }

        private async Task AddCurrentPermissionsAsync(IEnumerable<Role> roles)
        {
            foreach (var role in roles)
            {
                var existingPermissionClaims = (await _roleManager.GetClaimsAsync(role))
                    .Where(x => x.Type == CustomClaimTypes.Permission)
                    .ToArray();

                var permissionsToAdd = _currentPermissions[role.Name]
                    .Excpet(existingPermissionClaims, (x, y) => y.Value == x);

                foreach (var permission in permissionsToAdd)
                {
                    await _roleManager.AddClaimAsync(role, new Claim(CustomClaimTypes.Permission, permission));
                }
            }
        }
    }
}