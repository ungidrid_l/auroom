using AuRooM.Application.Common.Models;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Settings;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.System.Commands.Seed
{
    public class SeedIdentityCommandHandler : IRequestHandler<SeedIdentityCommand, Result>
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly IOptions<RoleSettings> _roleSettings;

        public SeedIdentityCommandHandler(
            UserManager<User> userManager,
            RoleManager<Role> roleManager,
            IOptions<RoleSettings> roleSettings)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _roleSettings = roleSettings;
        }

        public async Task<Result> Handle(SeedIdentityCommand request, CancellationToken cancellationToken)
        {
            var seeder = new DataSeeder(_roleManager, _userManager, _roleSettings);
            await seeder.SeedAllAsync();

            return Result.Success();
        }
    }
}