﻿using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Settings;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.System.Commands.Seed
{
    public class UserSeeder
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleSettings _roleSettings;

        public UserSeeder(UserManager<User> userManager, RoleSettings roleSettings)
        {
            _userManager = userManager;
            _roleSettings = roleSettings;
        }

        public async Task SeedAsync()
        {
           if ((await _userManager.FindByNameAsync("admin@admin.com")) == null)
           {
               var adminUser = new User
               {
                   Id = Guid.NewGuid(),
                   FirstName = "Admin",
                   LastName = "Admin",
                   UserName = "admin@admin.com",
                   Email = "admin@admin.com",
                   DocumentInfo = "admin",
                   UserStatus = UserStatus.Active
               };

               adminUser.UserRoles.Add(new UserRole(adminUser.Id, _roleSettings.AdminRoleId));
               await _userManager.CreateAsync(adminUser, "Admin123!");
           }
        }
    }
}