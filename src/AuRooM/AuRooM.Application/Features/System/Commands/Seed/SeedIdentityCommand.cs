using AuRooM.Application.Common.Models;
using MediatR;

namespace AuRooM.Application.Features.System.Commands.Seed
{
    public class SeedIdentityCommand : IRequest<Result>
    {
    }
}