﻿using AuRooM.Common.Extensions;
using AuRooM.Domain.Constants;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Settings;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.System.Commands.Seed
{
    public class RoleSeeder
    {
        private readonly RoleManager<Role> _roleManager;
        private readonly RoleSettings _roleSettings;
        private readonly ICollection<Role> _currentRoles;

        public RoleSeeder(RoleManager<Role> roleManager, RoleSettings roleSettings)
        {
            _roleManager = roleManager;
            _roleSettings = roleSettings;

            _currentRoles = new HashSet<Role>()
            {
                new Role(UserRoles.Admin)
                {
                    Id = _roleSettings.AdminRoleId
                },
                new Role(UserRoles.Student)
                {
                    Id = _roleSettings.StudentRoleId
                },
                new Role(UserRoles.Teacher)
                {
                    Id = _roleSettings.TeacherRoleId
                },
                new Role(UserRoles.Prefect)
                {
                    Id = _roleSettings.PrefectRoleId
                }
            };
        }

        public async Task SeedAsync()
        {
            var existingRoles = _roleManager.Roles.ToArray();
            await RemoveUnusedRolesAsync(existingRoles);
            await AddCurrentRolesAsync(existingRoles);
        }

        private async Task RemoveUnusedRolesAsync(IEnumerable<Role> existingRoles)
        {
            var rolesToRemove = existingRoles.Excpet(_currentRoles, (x, y) => x.Id == y.Id);

            foreach (var role in rolesToRemove)
            {
                await _roleManager.DeleteAsync(role);
            }
        }

        private async Task AddCurrentRolesAsync(IEnumerable<Role> existingRoles)
        {
            var rolesToAdd = _currentRoles.Excpet(existingRoles, (x, y) => x.Id == y.Id);

            foreach (var role in rolesToAdd)
            {
                await _roleManager.CreateAsync(role);
            }
        }
    }
}