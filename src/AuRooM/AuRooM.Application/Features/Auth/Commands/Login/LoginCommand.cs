﻿using AuRooM.Application.Common.Models;
using MediatR;

namespace AuRooM.Application.Features.Auth.Commands.Login
{
    public class LoginCommand : IRequest<Result>
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}
