﻿using AuRooM.Application.Common.Models;
using AuRooM.Application.Specifications.UserSpecifications.Filter;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Identity;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Auth.Commands.Login
{
    public class LoginCommandHandler : IRequestHandler<LoginCommand, Result>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly SignInManager<User> _signInManager;

        public LoginCommandHandler(IUnitOfWork unitOfWork, SignInManager<User> signInManager)
        {
            _unitOfWork = unitOfWork;
            _signInManager = signInManager;
        }

        public async Task<Result> Handle(LoginCommand request, CancellationToken cancellationToken)
        {
            var user = await _unitOfWork.Users.FirstOrDefaultAsync(new UserByEmailFilter(request.Email));

            if (user == null)
                return Result.Failure("Account with such username does not exist");

            var result = await _signInManager.CheckPasswordSignInAsync(user, request.Password, false);

            if (!result.Succeeded)
                return Result.Failure("Password is incorrect");

            if (user.UserStatus != UserStatus.Active)
                return Result.Failure("Your account hasn't been approved!");

            result = await _signInManager.PasswordSignInAsync(request.Email, request.Password, request.RememberMe, false);

            return result.Succeeded
                       ? Result.Success()
                       : Result.Failure("Invalid login attempt");
        }
    }
}
