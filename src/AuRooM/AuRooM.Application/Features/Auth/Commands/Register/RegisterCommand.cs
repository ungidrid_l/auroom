﻿using AuRooM.Application.Common.Models;
using MediatR;
using System;

namespace AuRooM.Application.Features.Auth.Commands.Register
{
    public class RegisterCommand : IRequest<Result>
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string DocumentInfo { get; set; }

        public Guid RoleId { get; set; }

        public Guid? GroupId { get; set; }
    }
}