﻿using AuRooM.Application.Common.Extensions;
using AuRooM.Application.Common.Models;
using AuRooM.Application.Specifications.UserSpecifications.Filter;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AuRooM.Domain.Settings;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Auth.Commands.Register
{
    public class RegisterCommandHandler : IRequestHandler<RegisterCommand, Result>
    {
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;
        private readonly RoleSettings _roleSettings;
        private readonly IUnitOfWork _unitOfWork;

        public RegisterCommandHandler(
            UserManager<User> userManager,
            IMapper mapper,
            IOptions<RoleSettings> roleSettings,
            IUnitOfWork unitOfWork)
        {
            _userManager = userManager;
            _mapper = mapper;
            _roleSettings = roleSettings.Value;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result> Handle(RegisterCommand request, CancellationToken cancellationToken)
        {
            if (request.RoleId == _roleSettings.TeacherRoleId && request.GroupId.HasValue)
            {
                return Result.Failure("Teacher cannot have academic group selected");
            }

            var userToCheckDocumentInfo = await _unitOfWork.Users.FirstOrDefaultAsync(new UserByDocumentInfoFilter(request.DocumentInfo));

            if (userToCheckDocumentInfo != null)
            {
                return Result.Failure("User with this document already exist");
            }

            var user = _mapper.Map<User>(request);
            user.UserStatus = UserStatus.Pending;
            user.UserRoles.Add(new UserRole(user.Id, request.RoleId));

            var result = await _userManager.CreateAsync(user, request.Password);
            return result.Succeeded 
                       ? Result.Success() 
                       : Result.Failure(result.GetErrors());
        }
    }
}