﻿using AuRooM.Application.Common.Models;
using MediatR;

namespace AuRooM.Application.Features.Auth.Commands.Logout
{
    public class LogoutCommand : IRequest<Result>
    {
    }
}
