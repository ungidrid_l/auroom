﻿using AuRooM.Application.Common.Models;
using MediatR;

namespace AuRooM.Application.Features.Auth.Queries.GetRegistrationModel
{
    public class GetRegistrationModelQuery : IRequest<Result<RegisterVM>>
     {
     }
 }