﻿using AuRooM.Application.Common.Models;
using AuRooM.Application.Specifications.RoleSpecifications.Filter;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Auth.Queries.GetRegistrationModel
{
    public class GetRegistrationModelQueryHandler : IRequestHandler<GetRegistrationModelQuery, Result<RegisterVM>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetRegistrationModelQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<RegisterVM>> Handle(GetRegistrationModelQuery request, CancellationToken cancellationToken)
        {
            var model = new RegisterVM
            {
                Groups = (await _unitOfWork.AcademicGroups.GetKeyValuePairsAsync())
                    .Select(_mapper.Map<SelectListItem>)
                    .ToList(),
                Roles = (await _unitOfWork.Roles.GetKeyValuePairsAsync(new AdminRoleFilter().Negate()))
                    .Select(_mapper.Map<SelectListItem>)
                    .ToList()
            };

            return Result<RegisterVM>.Success(model);
        }
    }
}