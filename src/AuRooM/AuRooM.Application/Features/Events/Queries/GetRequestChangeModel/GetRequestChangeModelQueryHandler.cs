﻿using AuRooM.Application.Common.Models;
using AuRooM.Application.Specifications.EventSpecifications.Filter;
using AuRooM.Application.Specifications.EventSpecifications.Include;
using AuRooM.Application.Specifications.UserSpecifications.Filter;
using AuRooM.Common.Helpers;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Events.Queries.GetRequestChangeModel
{
    public class GetRequestChangeModelQueryHandler : IRequestHandler<GetRequestChangeModelQuery, Result<RequestChangeVM>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetRequestChangeModelQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<RequestChangeVM>> Handle(GetRequestChangeModelQuery request, CancellationToken cancellationToken)
        {
            var eventForChange = await _unitOfWork.Events.FirstOrDefaultAsync(new EventByIdFilter(request.EventId), new EventIncludeForList());
            var model = _mapper.Map<RequestChangeVM>(eventForChange);

            model.Auditories = (await _unitOfWork.Auditories.GetKeyValuePairsAsync())
                    .Select(_mapper.Map<SelectListItem>)
                    .ToList();
            model.Groups = (await _unitOfWork.AcademicGroups.GetKeyValuePairsAsync())
                    .Select(_mapper.Map<SelectListItem>)
                    .ToList();
            model.EventTypes = EnumHelper.EnumAsKeyValuePair<EventType>()
                .Select(_mapper.Map<SelectListItem>)
                .ToList();
            model.Teachers = (await _unitOfWork.Users.GetKeyValuePairsAsync(new ActiveTeacherUsersFilter()))
                .Select(_mapper.Map<SelectListItem>)
                .ToList();

            return Result<RequestChangeVM>.Success(model);
        }
    }
}
