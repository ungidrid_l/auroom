﻿using AuRooM.Domain.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AuRooM.Application.Features.Events.Queries.GetRequestChangeModel
{
    public class RequestChangeVM
    {
        public Guid EventId { get; set; }
        public string Name { get; set; }
        public int EventType { get; set; }
        public bool IsAppliedForEventSet { get; set; }

        [Required]
        public TimePeriod Timing { get; set; }

        [Required]
        public TimePeriod Period { get; set; }

        [Required]
        public int Frequency { get; set; }

        [Required]
        public Guid AuditoryId { get; set; }

        public IEnumerable<Guid?> GroupIds { get; set; }
        public IEnumerable<Guid?> TeacherIds { get; set; }      

        public IEnumerable<SelectListItem> Auditories { get; set; }        
        public IEnumerable<SelectListItem> EventTypes { get; set; }
        public IEnumerable<SelectListItem> Groups { get; set; }
        public IEnumerable<SelectListItem> Teachers { get; set; }
    }
}
