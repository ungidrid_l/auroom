﻿using AuRooM.Application.Common.Models;
using MediatR;
using System;

namespace AuRooM.Application.Features.Events.Queries.GetRequestChangeModel
{
    public class GetRequestChangeModelQuery : IRequest<Result<RequestChangeVM>>
    {
        public Guid EventId { get; set; }

        public GetRequestChangeModelQuery(Guid id)
        {
            EventId = id;
        }
    }
}
