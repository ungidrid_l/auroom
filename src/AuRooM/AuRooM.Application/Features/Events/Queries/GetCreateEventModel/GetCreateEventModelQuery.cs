﻿using AuRooM.Application.Common.Models;
using MediatR;

namespace AuRooM.Application.Features.Events.Queries.GetCreateEventModel
{
    public class GetCreateEventModelQuery : IRequest<Result<CreateEventVM>>
    {
    }
}
