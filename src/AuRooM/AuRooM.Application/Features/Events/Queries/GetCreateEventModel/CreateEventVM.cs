﻿using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AuRooM.Application.Features.Events.Queries.GetCreateEventModel
{
    public class CreateEventVM
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public TimePeriod Timing { get; set; }

        [Required]
        public TimePeriod Period { get; set; }

        [Required]
        public int Frequency { get; set; } = 1;

        [Required]
        public EventType EventType { get; set; }
        
        [Required]
        public Guid AuditoryId { get; set; }

        public IEnumerable<Guid?> GroupIds { get; set; }

        [Required]
        [Display(Name = "Narrators")]
        public IEnumerable<Guid?> NarratorIds { get; set; }

        public IEnumerable<SelectListItem> Auditories { get; set; }
        public IEnumerable<SelectListItem> EventTypes { get; set; }
        public IEnumerable<SelectListItem> Groups { get; set; }
        public IEnumerable<SelectListItem> Teachers { get; set; }
        public IEnumerable<SelectListItem> Narrators { get; set; }
    }
}
