﻿using AuRooM.Application.Common.Models;
using AuRooM.Application.Specifications.UserSpecifications.Filter;
using AuRooM.Common.Helpers;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Events.Queries.GetCreateEventModel
{
    public class GetCreateEventModelQueryHandler : IRequestHandler<GetCreateEventModelQuery, Result<CreateEventVM>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetCreateEventModelQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<CreateEventVM>> Handle(GetCreateEventModelQuery request, CancellationToken cancellationToken)
        {
            var model = new CreateEventVM
            {
                Auditories = (await _unitOfWork.Auditories.GetKeyValuePairsAsync()).Select(_mapper.Map<SelectListItem>)
                    .ToList(),
                Groups = (await _unitOfWork.AcademicGroups.GetKeyValuePairsAsync()).Select(_mapper.Map<SelectListItem>)
                    .ToList(),
                EventTypes = EnumHelper.EnumAsKeyValuePair<EventType>()
                    .Select(_mapper.Map<SelectListItem>)
                    .ToList(),
                Teachers = (await _unitOfWork.Users.GetKeyValuePairsAsync(new ActiveTeacherUsersFilter()))
                    .Select(_mapper.Map<SelectListItem>)
                    .ToList(),
                Narrators = (await _unitOfWork.Users.GetKeyValuePairsAsync(new ActiveUsersFilter()))
                    .Select(_mapper.Map<SelectListItem>)
                    .ToList()
            };

            return Result<CreateEventVM>.Success(model);
        }
    }
}
