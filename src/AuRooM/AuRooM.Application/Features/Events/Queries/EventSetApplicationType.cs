﻿namespace AuRooM.Application.Features.Events.Queries.GetSubscribeViewModel
{
    public enum EventSetApplicationType
    {
        ThisEvent,
        AllEvents
    }
}