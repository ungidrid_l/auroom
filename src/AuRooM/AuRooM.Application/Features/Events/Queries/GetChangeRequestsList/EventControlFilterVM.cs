﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace AuRooM.Application.Features.Events.Queries.GetChangeRequestsList
{
    public class EventControlFilterVM
    {
        public IEnumerable<int> SelectedStatuses { get; set; } = new HashSet<int>();
        public IEnumerable<int> SelectedEventTypes { get; set; } = new HashSet<int>();
        public IEnumerable<Guid> SelectedAuditories { get; set; } = new HashSet<Guid>();

        public IEnumerable<SelectListItem> Statuses { get; set; }
        public IEnumerable<SelectListItem> EventTypes { get; set; }
        public IEnumerable<SelectListItem> Auditories { get; set; }
    }
}