﻿using AuRooM.Domain.Entities;
using System;
using System.Collections.Generic;

namespace AuRooM.Application.Features.Events.Queries.GetChangeRequestsList
{
    public class ScheduleChangeRequestVM
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public TimePeriod Timing { get; set; }
        public TimePeriod Period { get; set; }
        public int Frequency { get; set; }
        public string RequestStatus { get; set; }
        public string RequestType { get; set; }
        public string EventType { get; set; }

        public string AuditoryNumber { get; set; }

        public Guid EventId { get; set; }
        public string RequestedByName { get; set; }

        public ICollection<string> GroupTitles { get; set; }
    }
}
