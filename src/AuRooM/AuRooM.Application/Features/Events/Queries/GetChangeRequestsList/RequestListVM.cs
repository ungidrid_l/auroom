using AuRooM.Application.Common.Models;
using System.Collections.Generic;

namespace AuRooM.Application.Features.Events.Queries.GetChangeRequestsList
{
    public class RequestListVM
    {
        public IEnumerable<ScheduleChangeRequestVM> RequestList { get; set; }
        public PaginationInfo PaginationInfo { get; set; }
        public EventControlFilterVM EventControlFilterVm { get; set; }
    }
}