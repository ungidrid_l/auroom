﻿using AuRooM.Application.Common.Models;
using MediatR;
using System;
using System.Collections.Generic;

namespace AuRooM.Application.Features.Events.Queries.GetChangeRequestsList
{
    public class GetChangeRequestsListQuery : IRequest<Result<RequestListVM>>
    {
        public Guid UserId { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public IEnumerable<int> SelectedStatuses { get; set; }
        public IEnumerable<int> SelectedEventTypes { get; set; }
        public IEnumerable<Guid> SelectedAuditories { get; set; }
    }
}