﻿using AuRooM.Application.Common.Extensions;
using AuRooM.Application.Common.Models;
using AuRooM.Application.Common.Permissions;
using AuRooM.Application.Services.Interfaces;
using AuRooM.Application.Specifications;
using AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Filter;
using AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Include;
using AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Order;
using AuRooM.Application.Specifications.UserSpecifications.Filter;
using AuRooM.Application.Specifications.UserSpecifications.Include;
using AuRooM.Common.Helpers;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Events.Queries.GetChangeRequestsList
{
    public class GetChangeRequestsListQueryHandler : IRequestHandler<GetChangeRequestsListQuery, Result<RequestListVM>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public GetChangeRequestsListQueryHandler(IUnitOfWork unitOfWork, IMapper mapper, IUserService userService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _userService = userService;
        }

        public async Task<Result<RequestListVM>> Handle(GetChangeRequestsListQuery request, CancellationToken cancellationToken)
        {
            var filter = new MatchAllFilter<ScheduleChangeRequest>()
                .AndIf(!request.SelectedStatuses.Any(), new ScheduleChangeRequestByStatusesFilter((int)ChangeRequestStatus.Pending))
                .AndIf(request.SelectedStatuses.Any(), new ScheduleChangeRequestByStatusesFilter(request.SelectedStatuses.ToArray()))
                .AndIf(request.SelectedAuditories.Any(), new ScheduleChangeRequestByAuditoriesFilter(request.SelectedAuditories))
                .AndIf(request.SelectedEventTypes.Any(), new ScheduleChangeRequestByEventsTypeFitler(request.SelectedEventTypes))
                .AndIf(await _userService.HasPermissionAsync(request.UserId, EventPermissions.ViewRelatedEventRequests),
                    new ScheduleChangeRequestByUserIdFilter(request.UserId));

            var changes = await _unitOfWork.ScheduleChangeRequests.GetAsync(
                              filter,
                              new ScheduleChangeRequestIncludeForList(),
                              new PagingSpecification(request.Page, request.PageSize),
                              new ScheduleChangeRequestTimingStartOrder(OrderByType.Asc));

            var changesForView = changes.Select(x => _mapper.Map<ScheduleChangeRequestVM>(x))
                .ToList();

            var eventsCount = await _unitOfWork.ScheduleChangeRequests.CountAsync(filter);
            var model = new RequestListVM()
            {
                RequestList = changesForView,
                PaginationInfo = new PaginationInfo(request.Page, eventsCount, request.PageSize),
                EventControlFilterVm = new EventControlFilterVM
                {
                    Auditories = (await _unitOfWork.Auditories.GetKeyValuePairsAsync())
                        .Select(_mapper.Map<SelectListItem>)
                        .ToList(),
                    EventTypes = EnumHelper.EnumAsKeyValuePair<EventType>()
                        .Select(_mapper.Map<SelectListItem>)
                        .ToList(),
                    Statuses = EnumHelper.EnumAsKeyValuePair<ChangeRequestStatus>()
                        .Select(_mapper.Map<SelectListItem>)
                        .ToList(),
                    SelectedAuditories = request.SelectedAuditories,
                    SelectedEventTypes = request.SelectedEventTypes,
                    SelectedStatuses = request.SelectedStatuses
                }
            };
            return Result<RequestListVM>.Success(model);
        }
    }
}
