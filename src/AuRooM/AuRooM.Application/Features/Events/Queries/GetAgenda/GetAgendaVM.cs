﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AuRooM.Application.Features.Events.Queries.GetAgenda
{
    public class GetAgendaVM
    {
        public bool IsNotUserEvents { get; set; }
        public Guid? UserId { get; set; }
        public Guid? NarratorId { get; set; }
        public Guid? AuditoryId { get; set; }
        public DateTime DayForWeek { get; set; } = DateTime.Now;

        public ILookup<DayOfWeek, EventForAgendaVM> EventList { get; set; }
        public IEnumerable<SelectListItem> Narrators { get; set; }
        public IEnumerable<SelectListItem> Auditories { get; set; }
    }
}
