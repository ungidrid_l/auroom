﻿using AuRooM.Application.Common.Models;
using MediatR;
using System;

namespace AuRooM.Application.Features.Events.Queries.GetAgenda
{
    public class GetAgendaQuery : IRequest<Result<GetAgendaVM>>
    {
        public bool IsNotUserEvents { get; set; }
        public Guid? UserId { get; set; }
        public Guid? NarratorId { get; set; }
        public Guid? AuditoryId { get; set; }
        public DateTime DayForWeek { get; set; }
    }
}
