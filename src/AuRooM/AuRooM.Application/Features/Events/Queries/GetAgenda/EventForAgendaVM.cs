﻿using AuRooM.Domain.Entities;
using System;
using System.Collections.Generic;

namespace AuRooM.Application.Features.Events.Queries.GetAgenda
{
    public class EventForAgendaVM
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public TimePeriod Timing { get; set; }
        public string AuditoryNumber { get; set; }
        public ICollection<string> Teachers { get; set; }
    }
}
