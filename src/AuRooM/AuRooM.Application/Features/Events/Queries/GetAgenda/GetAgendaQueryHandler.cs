﻿using AuRooM.Application.Common.Models;
using AuRooM.Application.Specifications;
using AuRooM.Application.Specifications.EventSpecifications.Filter;
using AuRooM.Application.Specifications.EventSpecifications.Include;
using AuRooM.Application.Specifications.EventSpecifications.Order;
using AuRooM.Application.Specifications.UserSpecifications.Filter;
using AuRooM.Common.Extensions;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Events.Queries.GetAgenda
{
    public class GetAgendaQueryHandler : IRequestHandler<GetAgendaQuery, Result<GetAgendaVM>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAgendaQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<GetAgendaVM>> Handle(GetAgendaQuery request, CancellationToken cancellationToken)
        {
            request.IsNotUserEvents = request.UserId == Guid.Empty || request.IsNotUserEvents;

            var (weekStart, weekEnd) = request.DayForWeek.WeekRange();
            var userId = !request.IsNotUserEvents ? request.UserId : null;

            var filterSpec = new MatchAllFilter<Event>()
                .AndIf(userId.HasValue, new EventByUserIdFilter(userId))
                .AndIf(request.NarratorId.HasValue, new EventByNarratorIdFilter(request.NarratorId))
                .AndIf(request.AuditoryId.HasValue, new EventByAuditoryIdFilter(request.AuditoryId))
                .And(new EventByTimeRangeFilter(weekStart, weekEnd));

            var events = await _unitOfWork.Events.GetAsync(
                                                        filterSpec, 
                                                        new EventIncludeForList(),
                                                        ordering: new EventTimeStartOrder(OrderByType.Asc));

            var model = new GetAgendaVM
            {
                Auditories = (await _unitOfWork.Auditories.GetKeyValuePairsAsync()).Select(_mapper.Map<SelectListItem>)
                    .ToList(),
                Narrators = (await _unitOfWork.Users.GetKeyValuePairsAsync(new ActiveUsersFilter()))
                    .Select(_mapper.Map<SelectListItem>)
                    .ToList(),
                EventList = events.Select(_mapper.Map<EventForAgendaVM>)
                    .ToLookup(e => e.Timing.Start.DayOfWeek, e => e)
            };

            return Result<GetAgendaVM>.Success(model);
        }
    }
}
