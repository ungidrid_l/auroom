﻿using AuRooM.Application.Common.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Events.Queries.GetSubscribeViewModel
{
    public class GetEventSetApplicationTypeVmQueryHandler : IRequestHandler<GetEventSetApplicationTypeVmQuery, Result<EventSetApplicationTypeVm>>
    {
        public Task<Result<EventSetApplicationTypeVm>> Handle(GetEventSetApplicationTypeVmQuery request, CancellationToken cancellationToken)
        {
            var result = new EventSetApplicationTypeVm
            {
                EventId = request.EventId,
                CheckboxItems = new[]
                {
                    new CheckboxItem<EventSetApplicationType>
                    {
                        Id = EventSetApplicationType.ThisEvent,
                        IsSelected = true,
                        Name = "Only this even"
                    },
                    new CheckboxItem<EventSetApplicationType>
                    {
                        Id = EventSetApplicationType.AllEvents,
                        Name = "All events"
                    }
                }
            };

            return Task.FromResult(Result<EventSetApplicationTypeVm>.Success(result));
        }
    }
}