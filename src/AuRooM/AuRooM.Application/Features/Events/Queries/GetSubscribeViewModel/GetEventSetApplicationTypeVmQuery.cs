﻿using AuRooM.Application.Common.Models;
using MediatR;
using System;

namespace AuRooM.Application.Features.Events.Queries.GetSubscribeViewModel
{
    public class GetEventSetApplicationTypeVmQuery : IRequest<Result<EventSetApplicationTypeVm>>
    {
        public Guid EventId { get; set; }

        public GetEventSetApplicationTypeVmQuery(Guid id)
        {
            EventId = id;
        }
    }
}