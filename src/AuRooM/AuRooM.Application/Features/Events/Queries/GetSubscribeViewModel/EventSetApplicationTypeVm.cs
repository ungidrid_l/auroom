﻿using AuRooM.Application.Common.Models;
using System;
using System.Collections.Generic;

namespace AuRooM.Application.Features.Events.Queries.GetSubscribeViewModel
{
    public class EventSetApplicationTypeVm
    {
        public Guid EventId { get; set; }

        public EventSetApplicationType SelectedEventSetApplicationType { get; set; }
        public IEnumerable<CheckboxItem<EventSetApplicationType>> CheckboxItems { get; set; }

        public string SubmitAction { get; set; }
        public string SubmitController { get; set; }
    }
}