﻿using AuRooM.Application.Common.Models;
using AuRooM.Application.Features.Events.Queries.GetSubscribeViewModel;
using AuRooM.Application.Specifications.EventSpecifications.Filter;
using AuRooM.Application.Specifications.EventSpecifications.Include;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Events.Commands.Subscribe
{
    public class SubscribeToEventCommandHandler : IRequestHandler<SubscribeToEventCommand, Result>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public SubscribeToEventCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result> Handle(SubscribeToEventCommand request, CancellationToken cancellationToken)
        {
            var change = await _unitOfWork.Events.FirstOrDefaultAsync(
                             new EventByIdFilter(request.EventId),
                             new EventIncludeUserEvents());

            var events = request.SelectedEventSetApplicationType == EventSetApplicationType.AllEvents
                             ? await _unitOfWork.Events.GetAsync(
                                   new EventByEventSetIdFilter(change.EventSetId),
                                   new EventIncludeUserEvents())
                             : new[] {change};

            var userEvents = events.Where(x => x.UserEvents.All(y => y.UserId != request.UserId))
                .Select(
                    x => new UserEvent
                    {
                        UserId = request.UserId,
                        EventId = x.Id
                    });

            _unitOfWork.UserEvents.AddRange(userEvents);

            await _unitOfWork.CommitAsync();
            return Result.Success();
        }
    }
}