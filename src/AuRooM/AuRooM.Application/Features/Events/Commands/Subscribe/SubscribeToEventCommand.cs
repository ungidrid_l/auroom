﻿using AuRooM.Application.Common.Models;
using AuRooM.Application.Features.Events.Queries.GetSubscribeViewModel;
using MediatR;
using System;

namespace AuRooM.Application.Features.Events.Commands.Subscribe
{
    public class SubscribeToEventCommand : IRequest<Result>
    {
        public Guid EventId { get; set; }
        public Guid UserId { get; set; }
        public EventSetApplicationType SelectedEventSetApplicationType { get; set; }
    }
}
