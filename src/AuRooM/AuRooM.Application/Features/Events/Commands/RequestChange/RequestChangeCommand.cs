﻿using AuRooM.Application.Common.Models;
using AuRooM.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;

namespace AuRooM.Application.Features.Events.Commands.RequestChange
{
    public class RequestChangeCommand : IRequest<Result>
    {
        public Guid EventId { get; set; }
        public string Name { get; set; }
        public int EventType { get; set; }
        public TimePeriod Timing { get; set; }
        public TimePeriod Period { get; set; }
        public int Frequency { get; set; }
        public Guid AuditoryId { get; set; }
        public Guid RequestedById { get; set; }
        public bool IsAppliedForEventSet { get; set; }

        public IEnumerable<Guid?> GroupIds { get; set; }
        public IEnumerable<Guid?> TeacherIds { get; set; }
    }
}
