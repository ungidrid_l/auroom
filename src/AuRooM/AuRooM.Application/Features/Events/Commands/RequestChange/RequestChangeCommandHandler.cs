﻿using AuRooM.Application.Common.Models;
using AuRooM.Common.Extensions;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Events.Commands.RequestChange
{
    public class RequestChangeCommandHandler : IRequestHandler<RequestChangeCommand, Result>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public RequestChangeCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result> Handle(RequestChangeCommand request, CancellationToken cancellationToken)
        {
            if (!(request.Period.Start, request.Period.Finish).IsValidPeriod())
                return Result.Failure("The start of the period is bigger than the finish of the period");

            if (!request.Period.Finish.IsFutureDate())
                return Result.Failure("The end of the period cannot be in the past");

            var change = _mapper.Map<ScheduleChangeRequest>(request);
            change.RequestStatus = ChangeRequestStatus.Pending;
            change.RequestDate = DateTime.UtcNow;
            change.RequestType = ChangeRequestType.Update;

            if (request.GroupIds != null)
            {
                _unitOfWork.GroupScheduleChangeRequests.AddRange(
                    request.GroupIds.Select(
                        x => new GroupScheduleChangeRequest
                        { AcademicGroupId = x.Value, ScheduleChangeRequestId = change.Id }).ToList());
            }

            if (request.TeacherIds != null)
            {
                _unitOfWork.NarratorScheduleChangeRequests.AddRange(
                    request.TeacherIds.Select(
                        x => new NarratorScheduleChangeRequest
                        { UserId = x.Value, ScheduleChangeRequestId = change.Id }).ToList());
            }

            _unitOfWork.ScheduleChangeRequests.Add(change);
            await _unitOfWork.CommitAsync();
            return Result.Success();
        }
    }
}
