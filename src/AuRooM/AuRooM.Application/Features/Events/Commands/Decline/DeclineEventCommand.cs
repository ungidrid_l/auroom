﻿using AuRooM.Application.Common.Models;
using MediatR;
using System;

namespace AuRooM.Application.Features.Events.Commands.Decline
{
    public class DeclineEventCommand : IRequest<Result>
    {
        public Guid Id { get; set; }
        public Guid DeclinedById { get; set; }
    }
}
