﻿using AuRooM.Application.Common.Models;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Events.Commands.Decline
{
    public class DeclineEventCommandHandler : IRequestHandler<DeclineEventCommand, Result>
    {
        private readonly IUnitOfWork _unitOfWork;       

        public DeclineEventCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;          
        }

        public async Task<Result> Handle(DeclineEventCommand request, CancellationToken cancellationToken)
        {
            var change = await _unitOfWork.ScheduleChangeRequests.GetByIdAsync(request.Id);

            change.RequestStatus = ChangeRequestStatus.Declined;
            change.StatusChangedById = request.DeclinedById;
            await _unitOfWork.CommitAsync();

            return Result.Success();
        }
    }
}
