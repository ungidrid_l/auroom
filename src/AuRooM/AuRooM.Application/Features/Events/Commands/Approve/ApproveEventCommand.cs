﻿using AuRooM.Application.Common.Models;
using MediatR;
using System;

namespace AuRooM.Application.Features.Events.Commands.Approve
{
    public class ApproveEventCommand : IRequest<Result>
    {
        public Guid Id { get; set; }
        public Guid ApprovedById { get; set; }
    }
}
