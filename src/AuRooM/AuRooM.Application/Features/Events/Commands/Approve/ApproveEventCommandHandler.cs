﻿using AuRooM.Application.Common.Models;
using AuRooM.Application.Services.DTOs;
using AuRooM.Application.Services.Interfaces;
using AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Filter;
using AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Include;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Events.Commands.Approve
{
    public class ApproveEventCommandHandler : IRequestHandler<ApproveEventCommand, Result>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IEventService _eventService;

        public ApproveEventCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IEventService eventService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _eventService = eventService;
        }

        public async Task<Result> Handle(ApproveEventCommand request, CancellationToken cancellationToken)
        {
            var change = await _unitOfWork.ScheduleChangeRequests.FirstOrDefaultAsync(
                             new ScheduleChangeRequestByIdFilter(request.Id),
                             new ScheduleChangeRequestIncludeForCreate());

            change.RequestStatus = ChangeRequestStatus.Approved;
            change.StatusChangedById = request.ApprovedById;

            var eventSet = _mapper.Map<EventSet>(change);
            _unitOfWork.EventSets.Add(eventSet);

            var startDate = change.Period.Start < DateTime.Now ? DateTime.Now : change.Period.Start;

            for (var date = startDate; date <= change.Period.Finish.Date; date = date.AddDays(change.Frequency * 7))
            {
                var createEventInfo = _mapper.Map<CreateEventInfoDto>(change);
                createEventInfo.Date = date.Date;
                createEventInfo.EventSet = eventSet;
                await _eventService.CreateEvent(createEventInfo);
            }

            await _unitOfWork.CommitAsync();
            return Result.Success();
        }
    }
}