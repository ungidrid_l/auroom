﻿using AuRooM.Application.Common.Models;
using AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Filter;
using AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Include;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Events.Commands.ApproveDelete
{
    public class ApproveDeleteCommandHandler : IRequestHandler<ApproveDeleteCommand, Result>
    {
        private readonly IUnitOfWork _unitOfWork;

        public ApproveDeleteCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Result> Handle(ApproveDeleteCommand request, CancellationToken cancellationToken)
        {
            var changeRequest = await _unitOfWork.ScheduleChangeRequests.FirstOrDefaultAsync(
                                    new ScheduleChangeRequestByIdFilter(request.ScheduleChangeRequestId),
                                    include: new ScheduleChangeRequestIncludeForDelete());
            if (changeRequest.RequestType != ChangeRequestType.Delete)
                return Result.Failure("Wrong change request ID");

            if (changeRequest.IsAppliedForEventSet)
            {
                _unitOfWork.EventSets.Remove(changeRequest.Event.EventSet);
            }
            else
            {
                _unitOfWork.Events.Remove(changeRequest.Event);
            }

            changeRequest.StatusChangedById = request.ApprovedById;
            changeRequest.RequestStatus = ChangeRequestStatus.Approved;
            await _unitOfWork.CommitAsync();

            return Result.Success();
        }
    }
}