﻿using AuRooM.Application.Common.Models;
using MediatR;
using System;

namespace AuRooM.Application.Features.Events.Commands.ApproveDelete
{
    public class ApproveDeleteCommand : IRequest<Result>
    {
        public Guid ScheduleChangeRequestId { get; set; }
        public Guid ApprovedById { get; set; }
    }
}