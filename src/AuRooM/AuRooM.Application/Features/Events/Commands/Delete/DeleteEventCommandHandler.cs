﻿using AuRooM.Application.Common.Models;
using AuRooM.Application.Features.Events.Queries.GetSubscribeViewModel;
using AuRooM.Application.Specifications.EventSpecifications.Filter;
using AuRooM.Application.Specifications.EventSpecifications.Include;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Events.Commands.Delete
{
    public class DeleteEventCommandHandler : IRequestHandler<DeleteEventCommand, Result>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public DeleteEventCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result> Handle(DeleteEventCommand request, CancellationToken cancellationToken)
        {
            var @event = await _unitOfWork.Events.FirstOrDefaultAsync(
                             new EventByIdFilter(request.EventId),
                             include: new EventIncludeEventSet());
            if (@event == null)
                return Result.Failure("Event with specified ID does not exists");

            var changeRequest = _mapper.Map<ScheduleChangeRequest>(@event);
            _mapper.Map<DeleteEventCommand, ScheduleChangeRequest>(request, changeRequest);

            changeRequest.RequestStatus = ChangeRequestStatus.Pending;
            changeRequest.RequestDate = DateTime.Now;
            changeRequest.RequestType = ChangeRequestType.Delete;
            changeRequest.IsAppliedForEventSet = request.SelectedEventSetApplicationType == EventSetApplicationType.AllEvents;
            changeRequest.Period = changeRequest.IsAppliedForEventSet
                                       ? new TimePeriod(@event.EventSet.Period.Start, @event.EventSet.Period.Finish)
                                       : null;

            _unitOfWork.ScheduleChangeRequests.Add(changeRequest);
            await _unitOfWork.CommitAsync();
            return Result.Success();
        }
    }
}