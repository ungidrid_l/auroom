﻿using AuRooM.Application.Common.Models;
using MediatR;
using System;

namespace AuRooM.Application.Features.Events.Commands.ApproveRequestChange
{
    public class ApproveRequestChangeCommand : IRequest<Result>
    {
        public Guid Id { get; set; }
        public Guid ApprovedById { get; set; }
    }
}
