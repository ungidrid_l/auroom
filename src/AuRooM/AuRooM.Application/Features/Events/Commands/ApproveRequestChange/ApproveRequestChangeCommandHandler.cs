﻿using AuRooM.Application.Common.Models;
using AuRooM.Application.Services.DTOs;
using AuRooM.Application.Services.Interfaces;
using AuRooM.Application.Specifications.EventSpecifications.Filter;
using AuRooM.Application.Specifications.EventSpecifications.Include;
using AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Filter;
using AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Include;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.Features.Events.Commands.ApproveRequestChange
{
    public class ApproveRequestChangeCommandHandler : IRequestHandler<ApproveRequestChangeCommand, Result>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IEventService _eventService;

        public ApproveRequestChangeCommandHandler(
            IUnitOfWork unitOfWork, 
            IMapper mapper, 
            IEventService eventService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _eventService = eventService;
        }

        public async Task<Result> Handle(ApproveRequestChangeCommand request, CancellationToken cancellationToken)
        {
            var change = await _unitOfWork.ScheduleChangeRequests.FirstOrDefaultAsync(
                             new ScheduleChangeRequestByIdFilter(request.Id),
                             new ScheduleChangeRequestIncludeForUpdate());            

            var eventForChange = await _unitOfWork.Events.FirstOrDefaultAsync(
                             new EventByIdFilter(change.EventId.Value),
                             new EventIncludeForUpdate());

            var eventSet = eventForChange.EventSet;

            var eventsForChange = (change.IsAppliedForEventSet
                                       ? await _unitOfWork.Events.GetAsync(
                                             new EventByEventSetIdFilter(eventSet.Id),
                                             new EventIncludeForUpdate())
                                       : new[] {eventForChange}).ToList();

            if (change.IsAppliedForEventSet 
                    && (change.Period.Finish != eventSet.Period.Finish
                        || change.Frequency != eventSet.Frequency))
            {
                _mapper.Map<ScheduleChangeRequest, EventSet>(change, eventSet);

                var removedEventsResult = await RedistributeEvents(eventsForChange, change, eventSet);
                if (!removedEventsResult.Succeeded)
                    return Result.Failure(removedEventsResult.Errors);

                foreach (var removedEvent in removedEventsResult.Data)
                {
                    eventsForChange.Remove(removedEvent);
                }
            }

            foreach (var e in eventsForChange)
            {
                var result = await _eventService.UpdateEvent(e, change);
                if (!result.Succeeded)
                    return Result.Failure(result.Errors);
            }

            change.RequestStatus = ChangeRequestStatus.Approved;
            change.StatusChangedById = request.ApprovedById;
            
            await _unitOfWork.CommitAsync();
            return Result.Success();
        }

        private async Task<Result<IReadOnlyList<Event>>> RedistributeEvents(IEnumerable<Event> eventsForChange, ScheduleChangeRequest change, EventSet currentEventSet)
        {
            IReadOnlyList<Event> removedEvents = null;

            var startDate = currentEventSet.Period.Start < DateTime.Now ? DateTime.Now : currentEventSet.Period.Start;
            var delta = change.Frequency * 7;
            for (var date = startDate; date <= change.Period.Finish.Date; date = date.AddDays(delta))
            {
                var eventsForDelete = eventsForChange
                    .Where(x => x.Timing.Start.Date > date.Date && x.Timing.Finish.Date < date.AddDays(delta).Date)
                    .ToList();

                removedEvents = eventsForDelete;
                _unitOfWork.Events.RemoveRange(eventsForDelete);

                var existingEvent = eventsForChange.Where(x => x.Timing.Start.Date == date.Date).FirstOrDefault();

                if (existingEvent != null) continue;

                var createEventInfo = _mapper.Map<CreateEventInfoDto>(change);
                createEventInfo.Date = date.Date;
                createEventInfo.EventSet = currentEventSet;

                var creationResult = await _eventService.CreateEvent(createEventInfo);
                if (!creationResult.Succeeded)
                    return Result<IReadOnlyList<Event>>.Failure(creationResult.Errors);
            }

            return Result<IReadOnlyList<Event>>.Success(removedEvents);
        }
    }
}
