﻿using AuRooM.Application.Common.Models;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AuRooM.Application.Services.Interfaces;

namespace AuRooM.Application.Features.Events.Commands.Create
{
    public class CreateEventCommandHandler : IRequestHandler<CreateEventCommand, Result>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IEventService _eventService;

        public CreateEventCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IEventService eventService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _eventService = eventService;
        }

        public async Task<Result> Handle(CreateEventCommand request, CancellationToken cancellationToken)
        {
            var change = _mapper.Map<ScheduleChangeRequest>(request);

            var validationResult = await _eventService.ValidateEventForCreation(
                                       request.AuditoryId,
                                       request.Period.Start,
                                       request.Period.Finish,
                                       request.Timing.Start,
                                       request.Timing.Finish,
                                       request.Frequency);

            if (!validationResult.Succeeded) return validationResult;

            change.RequestStatus = ChangeRequestStatus.Pending;
            change.RequestDate = DateTime.Now;
            change.RequestType = ChangeRequestType.Create;

            if (request.GroupIds != null)
            {
                _unitOfWork.GroupScheduleChangeRequests.AddRange(
                    request.GroupIds.Select(
                        x => new GroupScheduleChangeRequest
                        { AcademicGroupId = x.Value, ScheduleChangeRequestId = change.Id }).ToList());
            }

            if (request.NarratorIds != null)
            {
                _unitOfWork.NarratorScheduleChangeRequests.AddRange(
                    request.NarratorIds.Select(
                        x => new NarratorScheduleChangeRequest
                        { UserId = x.Value, ScheduleChangeRequestId = change.Id }).ToList());
            }

            _unitOfWork.ScheduleChangeRequests.Add(change);
            await _unitOfWork.CommitAsync();                
            return Result.Success();
        }
    }
}
