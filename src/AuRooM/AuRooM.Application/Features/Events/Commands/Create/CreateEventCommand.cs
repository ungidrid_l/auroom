﻿using AuRooM.Application.Common.Models;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using MediatR;
using System;
using System.Collections.Generic;

namespace AuRooM.Application.Features.Events.Commands.Create
{
    public class CreateEventCommand : IRequest<Result>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public TimePeriod Timing { get; set; }
        public TimePeriod Period { get; set; }
        public int Frequency { get; set; }
        public DateTime RequestDate { get; set; }
        public ChangeRequestStatus RequestStatus { get; set; }
        public ChangeRequestType RequestType { get; set; }
        public EventType EventType { get; set; }
        public Guid AuditoryId { get; set; }
        public Guid? EventId { get; set; }
        public Guid RequestedById { get; set; }
        public Guid? ApprovedById { get; set; }
        public IEnumerable<Guid?> GroupIds { get; set; }
        public IEnumerable<Guid?> NarratorIds { get; set; }
    }
}
