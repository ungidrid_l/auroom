﻿using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Application.PipelineBehaviours
{
    public class LoggingBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        private readonly ILogger<LoggingBehaviour<TRequest, TResponse>> _logger;

        public LoggingBehaviour(ILogger<LoggingBehaviour<TRequest, TResponse>> logger) => _logger = logger;

        public async Task<TResponse> Handle(
            TRequest request,
            CancellationToken cancellationToken,
            RequestHandlerDelegate<TResponse> next)
        {
            var requestName = typeof(TRequest).Name;
            try
            {
                _logger.LogInformation("Entering {RequestName} Handler", requestName);
                return await next();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occured in {RequestName} Handler. Model: {@Model}", requestName, request);
                throw;
            }
            finally
            {
                _logger.LogInformation("Exiting {RequestName} Handler", requestName);
            }
        }
    }
}