﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;

namespace AuRooM.Application.Common.Extensions
{
    public static class IdentityResultExtensions
    {
        public static IEnumerable<string> GetErrors(this IdentityResult result)
        {
            return result.Errors.Select(x => x.Description).ToList();
        }
    }
}