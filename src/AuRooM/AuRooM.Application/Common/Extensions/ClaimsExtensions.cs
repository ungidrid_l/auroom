﻿using AuRooM.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace AuRooM.Application.Common.Extensions
{
    public static class ClaimsExtensions
    {
        public static bool ContainsPermission(this IEnumerable<Claim> claims, string permissionName)
        {
            var permissions = claims
                .Where(c => c.Type.Equals(CustomClaimTypes.Permission, StringComparison.OrdinalIgnoreCase))
                .Select(c => c.Value)
                .ToList();

            return permissions.Contains(permissionName);
        }
    }
}
