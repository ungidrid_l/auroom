﻿namespace AuRooM.Application.Common.Models
{
    public class CheckboxItem<T>
    {
        public T Id { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }
}