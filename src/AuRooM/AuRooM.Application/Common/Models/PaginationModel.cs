﻿namespace AuRooM.Application.Common.Models
{
    public class PaginationModel
    {
        public int PageSize { get; set; } = 7;
        public int Page { get; set; } = 1;
    }
}