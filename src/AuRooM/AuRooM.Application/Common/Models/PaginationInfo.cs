﻿using System;

namespace AuRooM.Application.Common.Models
{
    public class PaginationInfo
    {
        public int CurrentPage { get; }
        public int PagesCount { get; }

        public PaginationInfo(int currentPage, int itemsCount, int pageSize)
        {
            CurrentPage = currentPage;
            PagesCount = itemsCount == 0 ? 1 : (int)Math.Ceiling((double)itemsCount / pageSize);
        }
    }
}