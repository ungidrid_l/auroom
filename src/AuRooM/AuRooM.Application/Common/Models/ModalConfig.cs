﻿namespace AuRooM.Application.Common.Models
{
    public class ModalConfig
    {
        public string ModalId { get; set; }
        public string Header { get; set; }
        public string SubmitText { get; set; }

        public string BodyName { get; set; }
        public object BodyModel { get; set; }
    }
}