﻿using System.Collections.Generic;
using System.Linq;

namespace AuRooM.Application.Common.Models
{
    public class ResultBase
    {
        public bool Succeeded { get; set; }

        public string[] Errors { get; set; }

        internal ResultBase(bool succeeded, IEnumerable<string> errors)
        {
            Succeeded = succeeded;
            Errors = errors?.ToArray();
        }
    }

    public class Result : ResultBase
    {
        internal Result(bool succeeded, IEnumerable<string> errors) : base(succeeded, errors)
        {
        }

        public static Result Success() => new Result(true, null);

        public static Result Failure(IEnumerable<string> errors) => new Result(false, errors);

        public static Result Failure(string error)
        {
            return Failure(new[] {error});
        }
    }

    public class Result<T> : ResultBase
    {
        public T Data { get; set; }

        internal Result(bool succeeded, T data, IEnumerable<string> errors) : base(succeeded, errors) => Data = data;

        public static Result<T> Success(T entity) => new Result<T>(true, entity, null);

        public static Result<T> Failure(IEnumerable<string> errors) => new Result<T>(false, default, errors);

        public static Result<T> Failure(string error) => Failure(new[] { error });
    }
}