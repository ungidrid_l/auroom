﻿using AuRooM.Application.Features.Events.Commands.Create;
using AuRooM.Application.Features.Events.Commands.Delete;
using AuRooM.Application.Features.Events.Commands.Subscribe;
using AuRooM.Application.Features.Events.Queries.GetAgenda;
using AuRooM.Application.Features.Events.Queries.GetCreateEventModel;
using AuRooM.Application.Features.Events.Queries.GetRequestChangeModel;
using AuRooM.Application.Features.Events.Queries.GetSubscribeViewModel;
using AuRooM.Application.Services.DTOs;
using AuRooM.Domain.Entities;
using AutoMapper;
using System;
using System.Linq;

namespace AuRooM.Application.Common.Mappings
{
    public class EventMappingProfile : Profile
    {
        public EventMappingProfile()
        {
            CreateMap<CreateEventVM, CreateEventCommand>();

            CreateMap<CreateEventCommand, ScheduleChangeRequest>()
                .ForMember(x => x.Id, opt => opt.MapFrom(s => Guid.NewGuid()));

            CreateMap<ScheduleChangeRequest, Event>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.Type, opt => opt.MapFrom(x => x.EventType))
                .ForMember(x => x.PersonInChargeId, opt => opt.MapFrom(x => x.RequestedById))
                .ForMember(x => x.Auditory, opt => opt.Ignore());

            CreateMap<Event, ScheduleChangeRequest>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.EventType, opt => opt.MapFrom(x => x.Type))
                .ForMember(x => x.RequestedById, opt => opt.MapFrom(x => x.PersonInChargeId))
                .ForMember(x => x.Auditory, opt => opt.Ignore());

            CreateMap<CreateEventInfoDto, Event>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.Type, opt => opt.MapFrom(x => x.EventType))
                .ForMember(x => x.PersonInChargeId, opt => opt.MapFrom(x => x.RequestedBy.Id))
                .ForMember(x => x.Auditory, opt => opt.Ignore());

            CreateMap<Event, EventForAgendaVM>()
                .ForMember(x => x.AuditoryNumber, opt => opt.MapFrom(x => x.Auditory.Number))
                .ForMember(x => x.Teachers, opt => opt.MapFrom(x => x.UserEvents.Where(y => y.IsNarrator == true).Select(z => z.User.GetFullName())))
                .ForMember(x => x.Type, opt => opt.MapFrom(x => x.Type.ToString()));

            CreateMap<GetAgendaVM, GetAgendaQuery>();

            CreateMap<EventSetApplicationTypeVm, SubscribeToEventCommand>();
            CreateMap<EventSetApplicationTypeVm, DeleteEventCommand>();

            CreateMap<Event, RequestChangeVM>()
                .ForMember(x => x.EventId, opt => opt.MapFrom(x => x.Id))
                .ForMember(x => x.AuditoryId, opt => opt.MapFrom(x => x.Auditory.Id))
                .ForMember(x => x.Period, opt => opt.MapFrom(x => x.EventSet.Period))
                .ForMember(x => x.Frequency, opt => opt.MapFrom(x => x.EventSet.Frequency))
                .ForMember(x => x.EventType, opt => opt.MapFrom(x => (int)x.Type))
                .ForMember(x => x.TeacherIds, opt => opt.MapFrom(x => x.UserEvents.Where(y => y.IsNarrator == true).Select(z => z.UserId)))
                .ForMember(x => x.GroupIds, opt => opt.MapFrom(x => x.GroupEvents.Select(y => y.AcademicGroupId)));
        }
    }
}
