﻿using AuRooM.Application.Features.Auth.Commands.Login;
using AuRooM.Application.Features.Auth.Commands.Register;
using AuRooM.Application.Features.Auth.Queries.GetRegistrationModel;
using AuRooM.Application.Features.Users.Commands.Edit;
using AuRooM.Application.Features.Users.Queries.GetUser;
using AuRooM.Application.Features.Users.Queries.GetUsersList;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AutoMapper;
using System;
using System.Linq;

namespace AuRooM.Application.Common.Mappings
{
    public class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            CreateMap<RegisterVM, RegisterCommand>();

            CreateMap<RegisterCommand, User>()
                .ForMember(x => x.Id, opt => opt.MapFrom(s => Guid.NewGuid()))
                .ForMember(x => x.UserName, opt => opt.MapFrom(s => s.Email))
                .ForMember(x => x.AcademicGroupId, opt => opt.MapFrom(s => s.GroupId));

            CreateMap<LoginVM, LoginCommand>();

            CreateMap<LoginCommand, User>()
                .ForMember(x => x.Id, opt => opt.MapFrom(s => Guid.NewGuid()))
                .ForMember(x => x.UserName, opt => opt.MapFrom(s => s.Email));

            CreateMap<User, UserVM>()
                .ForMember(x => x.Status, opt => opt.MapFrom(x => x.UserStatus.ToString()))
                .ForMember(x => x.Group, opt => opt.MapFrom(x => x.AcademicGroup.Title))
                .ForMember(x => x.Role, opt => opt.MapFrom(x => x.UserRoles.First().Role.Name));

            CreateMap<EditUserVM, EditUserCommand>();

            CreateMap<User, EditUserVM>()
                .ForMember(x => x.Status, opt => opt.MapFrom(x => (int)x.UserStatus))
                .ForMember(x => x.GroupId, opt => opt.MapFrom(x => x.AcademicGroupId))
                .ForMember(x => x.RoleId, opt => opt.MapFrom(x => x.UserRoles.First().RoleId))
                .ForMember(x => x.Roles, opt => opt.Ignore());

            CreateMap<EditUserCommand, User>()
                .ForMember(x => x.UserStatus, opt => opt.MapFrom(s => Enum.Parse<UserStatus>(s.Status)))
                .ForMember(x => x.AcademicGroupId, opt => opt.MapFrom(s => s.GroupId))
                .ForMember(x => x.UserName, opt => opt.MapFrom(x => x.Email));
        }
    }
}