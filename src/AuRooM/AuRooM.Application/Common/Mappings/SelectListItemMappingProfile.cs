﻿using AuRooM.Common.DTOs;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AuRooM.Application.Common.Mappings
{
    public class SelectListItemMappingProfile : Profile
    {
        public SelectListItemMappingProfile()
        {
            CreateMap<SelectListItemDto, SelectListItem>();
        }
    }
}