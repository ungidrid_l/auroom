﻿using AuRooM.Application.Features.Events.Commands.Delete;
using AuRooM.Application.Features.Events.Commands.RequestChange;
using AuRooM.Application.Features.Events.Queries.GetChangeRequestsList;
using AuRooM.Application.Features.Events.Queries.GetRequestChangeModel;
using AuRooM.Application.Services.DTOs;
using AuRooM.Domain.Entities;
using AutoMapper;
using System;
using System.Linq;

namespace AuRooM.Application.Common.Mappings
{
    public class ChangeRequestMappingProfile : Profile
    {
        public ChangeRequestMappingProfile()
        {
            CreateMap<EventControlFilterVM, GetChangeRequestsListQuery>();

            CreateMap<ScheduleChangeRequest, ScheduleChangeRequestVM>()
                .ForMember(x => x.AuditoryNumber, opt => opt.MapFrom(x => x.Auditory.Number))
                .ForMember(x => x.RequestedByName, opt => opt.MapFrom(x => x.RequestedBy.GetFullName()))
                .ForMember(x => x.GroupTitles, opt => opt.MapFrom(x => x.GroupScheduleChangeRequests.Select(x => x.AcademicGroup.Title)))
                .ForMember(x => x.EventType, opt => opt.MapFrom(x => x.EventType.ToString()))
                .ForMember(x => x.RequestStatus, opt => opt.MapFrom(x => x.RequestStatus.ToString()))
                .ForMember(x => x.RequestType, opt => opt.MapFrom(x => x.RequestType.ToString()));

            CreateMap<ScheduleChangeRequest, CreateEventInfoDto>()
                .ForMember(x => x.Date, opt => opt.Ignore())
                .ForMember(x => x.TimeStart, opt => opt.MapFrom(x => x.Timing.Start.TimeOfDay))
                .ForMember(x => x.TimeFinish, opt => opt.MapFrom(x => x.Timing.Finish.TimeOfDay))
                .ForMember(x => x.TimeFinish, opt => opt.MapFrom(x => x.Timing.Finish.TimeOfDay));

            CreateMap<ScheduleChangeRequest, EventSet>()
                .ForMember(x => x.Id, opt => opt.Ignore());

            CreateMap<RequestChangeVM, RequestChangeCommand>();

            CreateMap<RequestChangeCommand, ScheduleChangeRequest>()
                .ForMember(x => x.Id, opt => opt.MapFrom(s => Guid.NewGuid()));

            CreateMap<DeleteEventCommand, ScheduleChangeRequest>()
                .ForMember(x => x.RequestedById, opt => opt.MapFrom(x => x.UserId));
        }
    }
}