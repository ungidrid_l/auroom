﻿namespace AuRooM.Application.Common.Permissions
{
    public static class UserPermissions
    {
        public const string ChangeStatus = "change.status";
        public const string LogOut = "log.out";
        public const string ViewUsers = "view.users";
        public const string RemoveUsers = "remove.users";
        public const string EditUsers = "edit.users";
    }
}