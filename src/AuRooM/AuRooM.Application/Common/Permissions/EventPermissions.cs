﻿namespace AuRooM.Application.Common.Permissions
{
    public static class EventPermissions
    {
        public const string RequestCreation = "request.creation";
        public const string Subscribe = "subscribe.to";
        public const string RequestChanges = "request.changes";
        public const string ApproveChanges = "approve.changes";
        public const string BrowseAgenda = "browse.agenda";
        public const string ViewEventRequests = "view.event.requests";
        public const string ViewRelatedEventRequests = "view.related.event.requests";
    }
}