﻿using AuRooM.Application.PipelineBehaviours;
using AuRooM.Application.Services.Implementations;
using AuRooM.Application.Services.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace AuRooM.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingBehaviour<,>));
            services.AddAutoMapper(Assembly.GetExecutingAssembly());

            services.AddTransient<IAuditoryService, AuditoryService>();
            services.AddTransient<IEventService, EventService>();
            services.AddTransient<IUserService, UserService>();
            return services;
        }
    }
}