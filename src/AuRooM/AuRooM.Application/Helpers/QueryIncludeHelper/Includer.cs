﻿using AuRooM.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AuRooM.Application.Helpers.QueryIncludeHelper
{
    public class Includer<T> : IIncluder<T>
    {
        private readonly IList<Func<IQueryable<T>, IQueryable<T>>> _includeChain;

        internal Includer(IList<Func<IQueryable<T>, IQueryable<T>>> includeChain) => _includeChain = includeChain;

        public IQueryable<T> Apply(IQueryable<T> query)
        {
            foreach (var func in _includeChain)
            {
                query = func(query);
            }

            return query;
        }
    }
}