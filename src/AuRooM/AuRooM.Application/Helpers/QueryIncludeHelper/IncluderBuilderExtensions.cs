﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace AuRooM.Application.Helpers.QueryIncludeHelper
{
    public static class IncluderBuilderExtensions
    {
        public static IncluderBuilder<TEntity, TNextProperty> Include<TEntity, TPrevProperty, TNextProperty>(
            this IncluderBuilder<TEntity, TPrevProperty> includerBuilder,
            Expression<Func<TEntity, TNextProperty>> selector)
            where TEntity : class 
            where TPrevProperty : class
            where TNextProperty : class 
        {
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includeQuery = queryable => queryable.Include(selector);
            includerBuilder.Includes.Add(includeQuery);
            return new IncluderBuilder<TEntity, TNextProperty>(includerBuilder.Includes);
        }

        public static IncluderBuilder<TEntity, TNextProperty> ThenInclude<TEntity, TPrevProperty, TNextProperty>(
            this IncluderBuilder<TEntity, TPrevProperty> includerBuilder,
            Expression<Func<TPrevProperty, TNextProperty>> selector)
            where TEntity : class
            where TNextProperty : class 
            where TPrevProperty : class 
        {
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includeQuery = queryable =>
                ((IIncludableQueryable<TEntity, TPrevProperty>)queryable).ThenInclude(selector);

            includerBuilder.Includes.Add(includeQuery);
            return new IncluderBuilder<TEntity, TNextProperty>(includerBuilder.Includes);
        }

        public static IncluderBuilder<TEntity, TNextProperty> ThenInclude<TEntity, TPrevProperty, TNextProperty>(
            this IncluderBuilder<TEntity, ICollection<TPrevProperty>> includerBuilder,
            Expression<Func<TPrevProperty, TNextProperty>> selector) 
            where TEntity : class 
            where TNextProperty : class
        {
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includeQuery = queryable =>
                ((IIncludableQueryable<TEntity, ICollection<TPrevProperty>>)queryable).ThenInclude(selector);

            includerBuilder.Includes.Add(includeQuery);
            return new IncluderBuilder<TEntity, TNextProperty>(includerBuilder.Includes);
        }
    }
}