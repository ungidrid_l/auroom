﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace AuRooM.Application.Helpers.QueryIncludeHelper
{
    public class IncluderBuilder<TEntity> where TEntity : class
    {
        public IncluderBuilder<TEntity, TProperty> Include<TProperty>(Expression<Func<TEntity, TProperty>> selector)
            where TProperty : class
        {
            return new IncluderBuilder<TEntity, TProperty>(queryable => queryable.Include(selector));
        }
    }

    public class IncluderBuilder<TEntity, TProperty>
        where TEntity : class
        where TProperty : class
    {
        internal IList<Func<IQueryable<TEntity>, IQueryable<TEntity>>> Includes { get; } =
            new List<Func<IQueryable<TEntity>, IQueryable<TEntity>>>();

        internal IncluderBuilder(Func<IQueryable<TEntity>, IQueryable<TEntity>> includes)
        {
            Includes.Add(includes);
        }

        internal IncluderBuilder(IList<Func<IQueryable<TEntity>, IQueryable<TEntity>>> includes) => Includes = includes;

        public Includer<TEntity> BuildInclude() => new Includer<TEntity>(Includes);
    }
}