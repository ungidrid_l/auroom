﻿using AuRooM.Domain.Constants;
using AuRooM.Domain.Entities;
using System;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications.RoleSpecifications.Filter
{
    public class AdminRoleFilter : FilteringSpecification<Role>
    {
        protected override Expression<Func<Role, bool>> SpecificationExpression { get; } = x => x.Name == UserRoles.Admin;
    }
}