﻿using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;
using System;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications.EventSpecifications.Order
{
    public class EventTimeStartOrder : OrderingSpecification<Event>
    {
        public EventTimeStartOrder(OrderByType orderByType) : base(orderByType)
        {
            SpecificationExpression = x => x.Timing.Start;
        }

        protected override Expression<Func<Event, object>> SpecificationExpression { get; }
    }
}