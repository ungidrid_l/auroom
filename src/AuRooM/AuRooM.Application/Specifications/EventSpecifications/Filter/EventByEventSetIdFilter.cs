﻿using AuRooM.Domain.Entities;
using System;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications.EventSpecifications.Filter
{
    public class EventByEventSetIdFilter : FilteringSpecification<Event>
    {
        protected override Expression<Func<Event, bool>> SpecificationExpression { get; }

        public EventByEventSetIdFilter(Guid eventSetId)
        {
            SpecificationExpression = x => x.EventSetId == eventSetId;
        }
    }
}