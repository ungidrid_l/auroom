﻿using AuRooM.Domain.Entities;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications.EventSpecifications.Filter
{
    public class EventByNarratorIdFilter : FilteringSpecification<Event>
    {
        protected override Expression<Func<Event, bool>> SpecificationExpression { get; }

        public EventByNarratorIdFilter(Guid? teacherId)
        {
            SpecificationExpression = x => x.UserEvents.Any(y => y.UserId == teacherId && y.IsNarrator);
        }
    }
}