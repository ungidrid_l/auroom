﻿using AuRooM.Domain.Entities;
using System;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications.EventSpecifications.Filter
{
    public class EventByAuditoryIdFilter : FilteringSpecification<Event>
    {
        protected override Expression<Func<Event, bool>> SpecificationExpression { get; }

        public EventByAuditoryIdFilter(Guid? auditoryId)
        {
            SpecificationExpression = x => x.AuditoryId == auditoryId;
        }
    }
}