﻿using AuRooM.Domain.Entities;
using System;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications.EventSpecifications.Filter
{
    public class EventByTimeRangeFilter : FilteringSpecification<Event>
    {
        protected override Expression<Func<Event, bool>> SpecificationExpression { get; }

        public EventByTimeRangeFilter(DateTime start, DateTime finish)
        {
            SpecificationExpression = x => x.Timing.Start >= start && x.Timing.Finish <= finish;
        }
    }
}