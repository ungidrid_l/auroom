﻿using AuRooM.Domain.Entities;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications.EventSpecifications.Filter
{
    public class EventByUserIdFilter : FilteringSpecification<Event>
    {
        protected override Expression<Func<Event, bool>> SpecificationExpression { get; }

        public EventByUserIdFilter(Guid? userId)
        {
            SpecificationExpression = x => x.UserEvents.Any(y => y.UserId == userId)
                                           || x.GroupEvents.Any(y => y.AcademicGroup.Students.Any(z => z.Id == userId));
        }
    }
}