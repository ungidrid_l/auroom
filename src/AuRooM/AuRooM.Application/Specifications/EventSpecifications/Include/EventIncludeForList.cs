﻿using AuRooM.Application.Helpers.QueryIncludeHelper;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;

namespace AuRooM.Application.Specifications.EventSpecifications.Include
{
    public class EventIncludeForList : IIncludeSpecification<Event>
    {
        public IIncluder<Event> Includer { get; set; }

        public EventIncludeForList()
        {
            Includer = new IncluderBuilder<Event>()
                .Include(x => x.Auditory)
                .Include(x => x.EventSet)
                .Include(x => x.GroupEvents)
                .ThenInclude(x => x.AcademicGroup)
                .Include(x => x.UserEvents)
                .ThenInclude(x => x.User)
                .BuildInclude();
        }
    }
}