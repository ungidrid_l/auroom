﻿using AuRooM.Application.Helpers.QueryIncludeHelper;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;

namespace AuRooM.Application.Specifications.EventSpecifications.Include
{
    public class EventIncludeForUpdate : IIncludeSpecification<Event>
    {
        public IIncluder<Event> Includer { get; set; }

        public EventIncludeForUpdate()
        {
            Includer = new IncluderBuilder<Event>()
                .Include(x => x.EventSet)
                .Include(x => x.UserEvents)
                .BuildInclude();
        }
    }
}
