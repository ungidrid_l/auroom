﻿using AuRooM.Application.Helpers.QueryIncludeHelper;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;

namespace AuRooM.Application.Specifications.EventSpecifications.Include
{
    public class EventIncludeUserEvents : IIncludeSpecification<Event>
    {
        public IIncluder<Event> Includer { get; set; }

        public EventIncludeUserEvents()
        {
            Includer = new IncluderBuilder<Event>()
                .Include(x => x.UserEvents)
                .BuildInclude();
        }
    }
}