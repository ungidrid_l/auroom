﻿using AuRooM.Application.Helpers.QueryIncludeHelper;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;

namespace AuRooM.Application.Specifications.EventSpecifications.Include
{
    public class EventIncludeEventSet : IIncludeSpecification<Event>
    {
        public IIncluder<Event> Includer { get; set; } = new IncluderBuilder<Event>().Include(x => x.EventSet)
            .BuildInclude();
    }
}