﻿using AuRooM.Application.Helpers.QueryIncludeHelper;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;

namespace AuRooM.Application.Specifications.UserSpecifications.Include
{
    public class UserIncludeGroupAndRoles : IIncludeSpecification<User>
    {
        public IIncluder<User> Includer { get; set; }

        public UserIncludeGroupAndRoles()
        {
            Includer = new IncluderBuilder<User>().Include(x => x.AcademicGroup)
                .Include(x => x.UserRoles)
                .ThenInclude(x => x.Role)
                .BuildInclude();
        }
    }
}