﻿using AuRooM.Application.Helpers.QueryIncludeHelper;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;

namespace AuRooM.Application.Specifications.UserSpecifications.Include
{
    public class UserIncludeRoles : IIncludeSpecification<User>
    {
        public IIncluder<User> Includer { get; set; }

        public UserIncludeRoles()
        {
            Includer = new IncluderBuilder<User>().Include(x => x.UserRoles)
                .ThenInclude(x => x.Role)
                .BuildInclude();
        }
    }
}