﻿using AuRooM.Domain.Entities;
using System;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications.UserSpecifications.Filter
{
    public class UserByIdFilter : FilteringSpecification<User>
    {
        protected override Expression<Func<User, bool>> SpecificationExpression { get; }

        public UserByIdFilter(Guid id)
        {
            SpecificationExpression = x => x.Id == id;
        }
    }
}