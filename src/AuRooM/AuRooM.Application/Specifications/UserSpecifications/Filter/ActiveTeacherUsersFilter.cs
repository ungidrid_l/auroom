﻿using AuRooM.Domain.Constants;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications.UserSpecifications.Filter
{
    public class ActiveTeacherUsersFilter : FilteringSpecification<User>
    {
        protected override Expression<Func<User, bool>> SpecificationExpression { get; } = x =>
            x.UserStatus == UserStatus.Active && x.UserRoles.Any(x => x.Role.Name == UserRoles.Teacher);
    }
}