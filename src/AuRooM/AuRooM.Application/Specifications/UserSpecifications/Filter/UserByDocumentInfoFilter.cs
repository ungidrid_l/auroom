﻿using AuRooM.Domain.Entities;
using System;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications.UserSpecifications.Filter
{
    internal class UserByDocumentInfoFilter : FilteringSpecification<User>
    {
        protected override Expression<Func<User, bool>> SpecificationExpression { get; }

        public UserByDocumentInfoFilter(string documentInfo)
        {
            SpecificationExpression = x => x.DocumentInfo == documentInfo.ToUpper();
        }
    }
}
