﻿using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using System;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications.UserSpecifications.Filter
{
    public class ActiveUsersFilter : FilteringSpecification<User>
    {
        protected override Expression<Func<User, bool>> SpecificationExpression { get; } = 
            x => x.UserStatus == UserStatus.Active;
    }
}