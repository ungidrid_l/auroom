﻿using AuRooM.Domain.Entities;
using System;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications.UserSpecifications.Filter
{
    public class UserByEmailFilter : FilteringSpecification<User>
    {
        protected override Expression<Func<User, bool>> SpecificationExpression { get; }

        public UserByEmailFilter(string email)
        {
            SpecificationExpression = x => x.NormalizedEmail == email.ToUpper();
        }
    }
}