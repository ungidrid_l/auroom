﻿using AuRooM.Domain.Interfaces;

namespace AuRooM.Application.Specifications
{
    public class PagingSpecification : IPagingSpecification
    {
        public int Skip { get; set; }
        public int Take { get; set; }

        public PagingSpecification(int page, int pageSize)
        {
            Skip = (page - 1) * pageSize;
            Take = pageSize;
        }
    }
}