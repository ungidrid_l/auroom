﻿using AuRooM.Domain.Interfaces;
using System;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications
{
    public abstract class OrderingSpecification<T> : IOrderingSpecification<T>
    {
        protected abstract Expression<Func<T, object>> SpecificationExpression { get; }

        public OrderByType OrderByType { get; }

        public OrderingSpecification(OrderByType orderByType)
        {
            OrderByType = orderByType;
        }

        public Expression<Func<T, object>> ToExpression() => SpecificationExpression;
    }
}