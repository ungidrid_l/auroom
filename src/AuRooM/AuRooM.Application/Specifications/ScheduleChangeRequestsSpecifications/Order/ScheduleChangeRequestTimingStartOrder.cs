﻿using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;
using System;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Order
{
    public class ScheduleChangeRequestTimingStartOrder : OrderingSpecification<ScheduleChangeRequest>
    {
        public ScheduleChangeRequestTimingStartOrder(OrderByType orderByType) : base(orderByType)
        {
            SpecificationExpression = x => x.Timing.Start;
        }

        protected override Expression<Func<ScheduleChangeRequest, object>> SpecificationExpression { get; }
    }
}