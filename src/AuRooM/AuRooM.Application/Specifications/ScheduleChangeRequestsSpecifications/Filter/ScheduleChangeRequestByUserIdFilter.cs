﻿using AuRooM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Filter
{
    public class ScheduleChangeRequestByUserIdFilter : FilteringSpecification<ScheduleChangeRequest>
    {
        protected override Expression<Func<ScheduleChangeRequest, bool>> SpecificationExpression { get; }

        public ScheduleChangeRequestByUserIdFilter(Guid userId)
        {
            SpecificationExpression = x => x.NarratorScheduleChangeRequests.Any(y => y.UserId == userId);
        }
    }
}
