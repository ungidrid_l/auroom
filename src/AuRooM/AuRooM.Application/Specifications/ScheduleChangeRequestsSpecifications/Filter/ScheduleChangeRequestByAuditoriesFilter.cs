﻿using AuRooM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Filter
{
    public class ScheduleChangeRequestByAuditoriesFilter : FilteringSpecification<ScheduleChangeRequest>
    {
        protected override Expression<Func<ScheduleChangeRequest, bool>> SpecificationExpression { get; }

        public ScheduleChangeRequestByAuditoriesFilter(IEnumerable<Guid> auditories)
        {
            SpecificationExpression = x => auditories.Any(y => y == x.AuditoryId);
        }
    }
}