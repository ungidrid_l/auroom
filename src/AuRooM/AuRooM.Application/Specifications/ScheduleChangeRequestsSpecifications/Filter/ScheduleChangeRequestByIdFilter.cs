﻿using AuRooM.Domain.Entities;
using System;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Filter
{
    public class ScheduleChangeRequestByIdFilter : FilteringSpecification<ScheduleChangeRequest>
    {
        protected override Expression<Func<ScheduleChangeRequest, bool>> SpecificationExpression { get; }

        public ScheduleChangeRequestByIdFilter(Guid id)
        {
            SpecificationExpression = x => x.Id == id;
        }
    }
}