﻿using AuRooM.Domain.Entities;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Filter
{
    public class ScheduleChangeRequestByStatusesFilter : FilteringSpecification<ScheduleChangeRequest>
    {
        protected override Expression<Func<ScheduleChangeRequest, bool>> SpecificationExpression { get; }

        public ScheduleChangeRequestByStatusesFilter(params int[] statuses)
        {
            SpecificationExpression = x => statuses.Any(y => y == (int)x.RequestStatus);
        }
    }
}