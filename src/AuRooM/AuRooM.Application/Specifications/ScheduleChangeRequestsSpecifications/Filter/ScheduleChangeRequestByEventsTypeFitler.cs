﻿using AuRooM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Filter
{
    public class ScheduleChangeRequestByEventsTypeFitler : FilteringSpecification<ScheduleChangeRequest>
    {
        protected override Expression<Func<ScheduleChangeRequest, bool>> SpecificationExpression { get; }

        public ScheduleChangeRequestByEventsTypeFitler(IEnumerable<int> eventTypes)
        {
            SpecificationExpression = x => eventTypes.Any(y => y == (int)x.EventType);
        }
    }
}