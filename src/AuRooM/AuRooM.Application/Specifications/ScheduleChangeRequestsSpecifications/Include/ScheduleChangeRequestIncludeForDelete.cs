﻿using AuRooM.Application.Helpers.QueryIncludeHelper;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;

namespace AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Include
{
    public class ScheduleChangeRequestIncludeForDelete : IIncludeSpecification<ScheduleChangeRequest>
    {
        public IIncluder<ScheduleChangeRequest> Includer { get; set; }

        public ScheduleChangeRequestIncludeForDelete()
        {
            var includeBuilder = new IncluderBuilder<ScheduleChangeRequest>()
                .Include(x => x.Event)
                .ThenInclude(x => x.EventSet);

            Includer = includeBuilder.BuildInclude();
        }
    }
}