﻿using AuRooM.Application.Helpers.QueryIncludeHelper;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;

namespace AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Include
{
    public class ScheduleChangeRequestIncludeForCreate : IIncludeSpecification<ScheduleChangeRequest>
    {
        public IIncluder<ScheduleChangeRequest> Includer { get; set; }

        public ScheduleChangeRequestIncludeForCreate()
        {
            Includer = new IncluderBuilder<ScheduleChangeRequest>()
                .Include(x => x.Auditory)
                .Include(x => x.GroupScheduleChangeRequests)
                .ThenInclude(x => x.AcademicGroup)
                .Include(x => x.RequestedBy)
                .Include(x => x.NarratorScheduleChangeRequests)
                .ThenInclude(x => x.User)
                .BuildInclude();
        }
    }
}