﻿using AuRooM.Application.Helpers.QueryIncludeHelper;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;

namespace AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Include
{
    public class ScheduleChangeRequestIncludeForUpdate : IIncludeSpecification<ScheduleChangeRequest>
    {
        public IIncluder<ScheduleChangeRequest> Includer { get; set; }

        public ScheduleChangeRequestIncludeForUpdate()
        {
            Includer = new IncluderBuilder<ScheduleChangeRequest>()
                .Include(x => x.GroupScheduleChangeRequests)
                .ThenInclude(x => x.AcademicGroup)
                .Include(x => x.Auditory)
                .Include(x => x.RequestedBy)
                .Include(x => x.NarratorScheduleChangeRequests)
                .ThenInclude(x => x.User)
                .BuildInclude();
        }
    }
}
