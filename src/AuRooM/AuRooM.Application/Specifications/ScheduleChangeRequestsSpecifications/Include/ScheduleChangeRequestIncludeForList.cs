﻿using AuRooM.Application.Helpers.QueryIncludeHelper;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;

namespace AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Include
{
    public class ScheduleChangeRequestIncludeForList : IIncludeSpecification<ScheduleChangeRequest>
    {
        public IIncluder<ScheduleChangeRequest> Includer { get; set; }

        public ScheduleChangeRequestIncludeForList()
        {
            Includer = new IncluderBuilder<ScheduleChangeRequest>()
                .Include(x => x.StatusChangedBy)
                .Include(x => x.Auditory)
                .Include(x => x.Event)
                .Include(x => x.GroupScheduleChangeRequests)
                .ThenInclude(x => x.AcademicGroup)
                .Include(x => x.RequestedBy)
                .BuildInclude();
        }
    }
}