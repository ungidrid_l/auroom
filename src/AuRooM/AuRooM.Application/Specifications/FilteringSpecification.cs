﻿using AuRooM.Domain.Interfaces;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace AuRooM.Application.Specifications
{
    public abstract class FilteringSpecification<T> : IFilteringSpecification<T>
    {
        private class ConstructedSpecification<TType> : FilteringSpecification<TType>
        {
            protected override Expression<Func<TType, bool>> SpecificationExpression { get; }

            public ConstructedSpecification(Expression<Func<TType, bool>> specificationExpression) =>
                SpecificationExpression = specificationExpression;
        }

        protected abstract Expression<Func<T, bool>> SpecificationExpression { get; }

        public IFilteringSpecification<T> And(IFilteringSpecification<T> right) =>
            CombineSpecification(this, right, Expression.AndAlso);

        public IFilteringSpecification<T> Or(IFilteringSpecification<T> right) =>
            CombineSpecification(this, right, Expression.OrElse);

        public IFilteringSpecification<T> AndIf(bool condition, IFilteringSpecification<T> right) => condition ? And(right) : this;

        public IFilteringSpecification<T> OrIf(bool condition, IFilteringSpecification<T> right) => condition ? Or(right) : this;

        public IFilteringSpecification<T> Negate() =>
            new ConstructedSpecification<T>(
                Expression.Lambda<Func<T, bool>>(
                    Expression.Not(SpecificationExpression.Body),
                    SpecificationExpression.Parameters));

        public Expression<Func<T, bool>> ToExpression() => SpecificationExpression;

        private static IFilteringSpecification<T> CombineSpecification(
            IFilteringSpecification<T> left,
            IFilteringSpecification<T> right,
            Func<Expression, Expression, BinaryExpression> combiner)
        {
            var expr1 = left.ToExpression();
            var expr2 = right.ToExpression();
            var arg = Expression.Parameter(typeof(T));
            var combined = combiner.Invoke(
                new ReplaceParameterVisitor
                {
                    {expr1.Parameters.Single(), arg}
                }.Visit(expr1.Body),
                new ReplaceParameterVisitor
                {
                    {expr2.Parameters.Single(), arg}
                }.Visit(expr2.Body));

            return new ConstructedSpecification<T>(Expression.Lambda<Func<T, bool>>(combined, arg));
        }
    }

    public class MatchAllFilter<T> : FilteringSpecification<T>
    {
        protected override Expression<Func<T, bool>> SpecificationExpression { get; } = x => true;
    }
}