﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AuRooM.Application.Services.Interfaces
{
    public interface IUserService
    {
        Task<bool> HasPermissionAsync(Guid userId, string permissionName);
    }
}
