﻿using System;
using AuRooM.Application.Common.Models;
using AuRooM.Application.Services.DTOs;
using AuRooM.Domain.Entities;
using System.Threading.Tasks;

namespace AuRooM.Application.Services.Interfaces
{
    public interface IEventService
    {
        Task<Result> CreateEvent(CreateEventInfoDto eventInfo);
        Task<Result> UpdateEvent(Event e, ScheduleChangeRequest change);

        Task<Result> ValidateEventForCreation(
            Guid auditoryId,
            DateTime periodStart,
            DateTime periodFinish,
            DateTime timingStart,
            DateTime timingFinish,
            int frequency);
    }
}