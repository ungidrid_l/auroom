﻿using AuRooM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuRooM.Application.Services.Interfaces
{
    public interface IAuditoryService
    {
        Task<IEnumerable<Event>> GetEventsInPeriod(Guid auditoryId, DateTime periodStart, DateTime periodEnd);
    }
}