﻿using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using System;
using System.Collections.Generic;

namespace AuRooM.Application.Services.DTOs
{
    public class CreateEventInfoDto
    {
        public TimeSpan TimeStart { get; set; }
        public TimeSpan TimeFinish { get; set; }
        public string Name { get; set; }
        public EventType EventType { get; set; }
        public Auditory Auditory { get; set; }
        public User RequestedBy { get; set; }

        public DateTime Date { get; set; }
        public EventSet EventSet { get; set; }

        public ICollection<GroupScheduleChangeRequest> GroupScheduleChangeRequests { get; set; }
        public ICollection<NarratorScheduleChangeRequest> NarratorScheduleChangeRequests { get; set; }
    }
}