﻿using AuRooM.Application.Common.Models;
using AuRooM.Application.Services.DTOs;
using AuRooM.Application.Services.Interfaces;
using AuRooM.Common.Extensions;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace AuRooM.Application.Services.Implementations
{
    public class EventService : IEventService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAuditoryService _auditoryService;
        private readonly IMapper _mapper;

        public EventService(IUnitOfWork unitOfWork, IAuditoryService auditoryService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _auditoryService = auditoryService;
            _mapper = mapper;
        }

        public async Task<Result> CreateEvent(CreateEventInfoDto eventInfo)
        {
            var eventStart = eventInfo.Date + eventInfo.TimeStart;
            var eventFinish = eventInfo.Date + eventInfo.TimeFinish;

            var isNotOccupied = await EnsureAuditoryNotOccupied(eventInfo.Auditory, eventStart, eventFinish);

            if (!isNotOccupied.Succeeded)
                return isNotOccupied;

            var newEvent = _mapper.Map<Event>(eventInfo);
            newEvent.EventSet = eventInfo.EventSet;
            newEvent.Timing = new TimePeriod(eventStart, eventFinish);

            _unitOfWork.Events.Add(newEvent);

            var groupScheduleChangeRequests = eventInfo.GroupScheduleChangeRequests;

            if (groupScheduleChangeRequests != null && groupScheduleChangeRequests.Any())
            {
                _unitOfWork.GroupEvents.AddRange(
                    groupScheduleChangeRequests.Select(x => new GroupEvent(x.AcademicGroupId, newEvent.Id))
                        .ToList());
            }

            var narratorScheduleChangeRequests = eventInfo.NarratorScheduleChangeRequests;

            if (narratorScheduleChangeRequests != null && narratorScheduleChangeRequests.Any())
            {
                _unitOfWork.UserEvents.AddRange(
                    narratorScheduleChangeRequests.Select(x => new UserEvent(x.UserId, newEvent.Id, true))
                        .ToList());
            }

            return Result.Success();
        }

        public async Task<Result> UpdateEvent(Event e, ScheduleChangeRequest change)
        {
            var eventStart = e.Timing.Start.Date + change.Timing.Start.TimeOfDay;
            var eventFinish = e.Timing.Finish.Date + change.Timing.Finish.TimeOfDay;
            _mapper.Map<ScheduleChangeRequest, Event>(change, e);

            // N+1 request problem
            var isNotOccupied = await EnsureAuditoryNotOccupied(
                change.Auditory,
                eventStart,
                eventFinish,
                e.Id);
            
            if (!isNotOccupied.Succeeded)
                return isNotOccupied;

            e.Timing = new TimePeriod(eventStart, eventFinish);

            var changeNarrators = change.NarratorScheduleChangeRequests;
            var eventNarrators = e.UserEvents.Where(x => x.IsNarrator)
                .ToList();

            var naratorsToAdd = changeNarrators.Excpet(eventNarrators, (x, y) => x.UserId == y.UserId);

            _unitOfWork.UserEvents.AddRange(
                naratorsToAdd.Select(x => new UserEvent(x.UserId, e.Id, true))
                    .ToList());

            var naratorsToRemove = eventNarrators.Excpet(changeNarrators, (x, y) => x.UserId == y.UserId);
            _unitOfWork.UserEvents.RemoveRange(naratorsToRemove);

            return Result.Success();
        }

        public async Task<Result> ValidateEventForCreation(
            Guid auditoryId, 
            DateTime periodStart, 
            DateTime periodFinish,
            DateTime timingStart, 
            DateTime timingFinish, 
            int frequency)
        {
            var auditory = await _unitOfWork.Auditories.GetByIdAsync(auditoryId);

            Result result = null;
            for (var date = periodStart.Date; date <= periodFinish.Date; date = date.AddDays(frequency * 7))
            {
                // N+1 request problem
                result = await EnsureAuditoryNotOccupied(auditory, date.Add(timingStart.TimeOfDay), date.Add(timingFinish.TimeOfDay));
                if (!result.Succeeded)
                    return result;
            }

            return result;
        }

        private async Task<Result> EnsureAuditoryNotOccupied(
            Auditory auditory,
            DateTime eventStart,
            DateTime eventFinish,
            Guid? currentEventId = null)
        {
            var overlapingEvents = await _auditoryService.GetEventsInPeriod(auditory.Id, eventStart, eventFinish);
            if (!overlapingEvents.Any() || currentEventId.HasValue && overlapingEvents.All(x => x.Id == currentEventId.Value))
                return Result.Success();

            var errorMessage = $"The auditory #{auditory.Number} is occupied in "
                               + string.Join(", ", overlapingEvents.Select(x => $" {x.Timing.Start} by {x.Name}"));

            return Result.Failure(errorMessage);
        }
    }
}