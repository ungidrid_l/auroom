﻿using AuRooM.Application.Common.Extensions;
using AuRooM.Application.Common.Permissions;
using AuRooM.Application.Services.Interfaces;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuRooM.Application.Services.Implementations
{
    public class UserService : IUserService
    {        
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;

        public UserService(UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task<bool> HasPermissionAsync(Guid userId, string permissionName)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());
            var claims = (await _userManager.GetClaimsAsync(user)).ToList();
            var roleName = (await _userManager.GetRolesAsync(user)).First();
            var role = await _roleManager.FindByNameAsync(roleName);
            claims.AddRange(await _roleManager.GetClaimsAsync(role));            
            return claims.ContainsPermission(permissionName);
        }
    }
}
