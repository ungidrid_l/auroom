﻿using AuRooM.Application.Services.Interfaces;
using AuRooM.Application.Specifications.EventSpecifications.Filter;
using AuRooM.Common.Extensions;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuRooM.Application.Services.Implementations
{
    public class AuditoryService : IAuditoryService
    {
        private readonly IUnitOfWork _unitOfWork;

        public AuditoryService(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<IEnumerable<Event>> GetEventsInPeriod(Guid auditoryId, DateTime periodStart, DateTime periodEnd)
        {
            var existingEvents = await _unitOfWork.Events.GetAsync(new EventByAuditoryIdFilter(auditoryId));
            return existingEvents.Where(
                    x => periodStart.Date == x.Timing.Start.Date
                         && (periodStart, periodEnd).OverlapsWith((x.Timing.Start, x.Timing.Finish)))
                .ToList();
        }
    }
}