﻿using AuRooM.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace AuRooM.Infrastructure.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AcademicGroup>()
                .HasData(
                    new AcademicGroup {Id = Guid.Parse("94B83C43-3D9F-472A-AA32-8F2A0CC0C61D"), Title = "PMI-31"},
                    new AcademicGroup {Id = Guid.Parse("05B28DEE-48C6-4029-8CD6-A63A9C47E313"), Title = "PMP-31"},
                    new AcademicGroup {Id = Guid.Parse("74E5720B-48D1-4C56-8B92-DF6D8709C22D"), Title = "PMO-31"},
                    new AcademicGroup {Id = Guid.Parse("232DA882-67E3-4A05-A4A3-14A1286099E3"), Title = "PMA-31"},
                    new AcademicGroup {Id = Guid.Parse("6277CD99-C336-4201-98A8-A0B944610745"), Title = "PMI-32" },
                    new AcademicGroup { Id = Guid.Parse("46D70E2F-B804-4CA3-92EF-F2BEE8F2B6C3"), Title = "PMI-21" },
                    new AcademicGroup { Id = Guid.Parse("A6AADB6F-F907-492E-88FB-2CD8AC6CF8CA"), Title = "PMP-21" },
                    new AcademicGroup { Id = Guid.Parse("13321379-837D-4501-A9E1-C6EB2C56C406"), Title = "PMO-21" },
                    new AcademicGroup { Id = Guid.Parse("6BDEBAC5-A32B-4944-85DA-8CE3D529CA78"), Title = "PMA-21" },
                    new AcademicGroup { Id = Guid.Parse("D3A43740-5EB3-481B-BD8E-EAEE15AC351A"), Title = "PMI-22" },
                    new AcademicGroup { Id = Guid.Parse("0024495B-81FD-4E09-AB2B-39B1D178DC7F"), Title = "PMI-41" },
                    new AcademicGroup { Id = Guid.Parse("B8D91A97-65D4-4943-A71C-FC8A5E4169B8"), Title = "PMP-41" },
                    new AcademicGroup { Id = Guid.Parse("1B1C2D39-283B-4CFA-BA05-F30C4907F64D"), Title = "PMO-41" },
                    new AcademicGroup { Id = Guid.Parse("5ED03158-3549-4EC4-A64C-534194A2AA25"), Title = "PMA-41" },
                    new AcademicGroup { Id = Guid.Parse("011E94BA-FA84-4ABD-B22A-EC31A76D8691"), Title = "PMI-32" });

            modelBuilder.Entity<Auditory>()
                .HasData(
                    new Auditory { Id = Guid.Parse("7244DDD4-6AD6-4985-AC25-D1329D06FE58"), Number = 101},
                    new Auditory { Id = Guid.Parse("41691AAD-9165-47AB-A6D9-876D4D7AABFE"), Number = 102 },
                    new Auditory { Id = Guid.Parse("11E86F1D-7DD9-4001-B56C-28B643DCC1BF"), Number = 103 },
                    new Auditory { Id = Guid.Parse("0A37E9FB-1234-4579-8216-F09D85EC3A43"), Number = 104 },
                    new Auditory { Id = Guid.Parse("AE27FFBD-C77F-4CDA-81F1-35B5B34508DE"), Number = 105 },
                    new Auditory { Id = Guid.Parse("FC398172-34C3-47B6-A97E-6BF3D254F08D"), Number = 106 },
                    new Auditory { Id = Guid.Parse("4DE85803-C248-499D-8301-D0B4C4A984BD"), Number = 107 },
                    new Auditory { Id = Guid.Parse("99D887EF-453C-49B2-9D9B-258DCB1A8F96"), Number = 108 },
                    new Auditory { Id = Guid.Parse("0C31A49E-DA3A-439A-9AEF-F48D35FDBA34"), Number = 109 },
                    new Auditory { Id = Guid.Parse("4CB4DFD1-C36C-4065-8A31-E2A512E39799"), Number = 110 },
                    new Auditory { Id = Guid.Parse("E20B4464-5225-45FB-9B3D-DF03700E159B"), Number = 111 },
                    new Auditory { Id = Guid.Parse("2A73DBF0-180A-49BD-AD11-A17F1E660987"), Number = 112 },
                    new Auditory { Id = Guid.Parse("50AF36AA-FE9F-4435-80BD-8B383A781AF8"), Number = 113 },
                    new Auditory { Id = Guid.Parse("56840050-CBB5-4C74-9762-BC6EC7C6CE87"), Number = 114 },
                    new Auditory { Id = Guid.Parse("95A5E090-8AB2-4A84-B07C-925F3FA69230"), Number = 115 },
                    new Auditory { Id = Guid.Parse("BCD111E1-B305-4D09-A0A5-F86AA4DF2EC1"), Number = 116 },
                    new Auditory { Id = Guid.Parse("035C9C6C-EF3D-4FB4-B0DC-CA340244A876"), Number = 117 },
                    new Auditory { Id = Guid.Parse("E09B648C-245C-4388-8C8A-E858224EE586"), Number = 118 },
                    new Auditory { Id = Guid.Parse("79F8FA3F-8315-40FC-9DC0-0DE025684238"), Number = 119 },
                    new Auditory { Id = Guid.Parse("CA9EDF15-B89A-4C06-8026-4FB3E0BAC3F0"), Number = 120 },
                    new Auditory { Id = Guid.Parse("DCF0FEDB-8057-4B7A-B780-C431B9BBB0E5"), Number = 121 },
                    new Auditory { Id = Guid.Parse("0C4212FB-8A49-402D-9FD1-9F5460EB8934"), Number = 122 },
                    new Auditory { Id = Guid.Parse("1FD8A880-ED4F-4240-8F56-CBC2A8980E1E"), Number = 123 },
                    new Auditory { Id = Guid.Parse("8D79D4DC-259B-4825-9D8C-B40AB3C6183B"), Number = 124 });
        }
    }
}