﻿using AuRooM.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuRooM.Infrastructure.EntityConfigurations
{
    public class EventConfigutation : IEntityTypeConfiguration<Event>
    {
        public void Configure(EntityTypeBuilder<Event> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name)
                .IsRequired();

            builder.OwnsOne(
                x => x.Timing,
                x =>
                {
                    x.Property(timePeriod => timePeriod.Start)
                        .HasColumnName("StartTime");

                    x.Property(timePeriod => timePeriod.Finish)
                        .HasColumnName("FinishTime");
                });

            builder.HasOne(x => x.Auditory)
                .WithMany(x => x.Events)
                .HasForeignKey(x => x.AuditoryId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.PersonInCharge)
                .WithMany(x => x.EventsInCharge)
                .HasForeignKey(x => x.PersonInChargeId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.EventSet)
                .WithMany(x => x.Events)
                .IsRequired()
                .HasForeignKey(x => x.EventSetId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}