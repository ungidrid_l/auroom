﻿using AuRooM.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuRooM.Infrastructure.EntityConfigurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(x => x.Id).ValueGeneratedNever();

            builder.Property(x => x.FirstName)
                .IsRequired();

            builder.Property(x => x.LastName)
                .IsRequired();

            builder.Property(x => x.DocumentInfo)
                .IsRequired();

            builder.HasOne(x => x.AcademicGroup)
                .WithMany(x => x.Students)
                .HasForeignKey(x => x.AcademicGroupId)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}