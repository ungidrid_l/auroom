﻿using AuRooM.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuRooM.Infrastructure.EntityConfigurations
{
    public class NarratorScheduleChangeRequestConfiguration : IEntityTypeConfiguration<NarratorScheduleChangeRequest>
    {
        public void Configure(EntityTypeBuilder<NarratorScheduleChangeRequest> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasAlternateKey(
                x => new
                {
                    x.UserId,
                    x.ScheduleChangeRequestId
                });

            builder.HasOne(x => x.User)
                .WithMany(x => x.NarratorScheduleChangeRequests)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(x => x.ScheduleChangeRequest)
                .WithMany(x => x.NarratorScheduleChangeRequests)
                .HasForeignKey(x => x.ScheduleChangeRequestId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
