﻿using AuRooM.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuRooM.Infrastructure.EntityConfigurations
{
    public class ScheduleChangeRequestConfiguration : IEntityTypeConfiguration<ScheduleChangeRequest>
    {
        public void Configure(EntityTypeBuilder<ScheduleChangeRequest> builder) 
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name)
                .IsRequired();

            builder.OwnsOne(
                x => x.Timing,
                x =>
                {
                    x.Property(timePeriod => timePeriod.Start)
                        .HasColumnName("StartTime");

                    x.Property(timePeriod => timePeriod.Finish)
                        .HasColumnName("FinishTime");
                });

            builder.OwnsOne(
                x => x.Period,
                x =>
                {
                    x.Property(timePeriod => timePeriod.Start)
                        .HasColumnName("StartPeriod");

                    x.Property(timePeriod => timePeriod.Finish)
                        .HasColumnName("FinishPeriod");
                });

            builder.HasOne(x => x.Auditory)
                .WithMany(x => x.ScheduleChangeRequests)
                .IsRequired()
                .HasForeignKey(x => x.AuditoryId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(x => x.RequestedBy)
                .WithMany(x => x.RequestedEvents)
                .HasForeignKey(x => x.RequestedById)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne(x => x.Event)
                .WithMany(x => x.EventScheduleChangeRequests)
                .HasForeignKey(x => x.EventId)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}