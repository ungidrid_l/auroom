﻿using AuRooM.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuRooM.Infrastructure.EntityConfigurations
{
    public class AcademicGroupConfiguration : IEntityTypeConfiguration<AcademicGroup>
    {
        public void Configure(EntityTypeBuilder<AcademicGroup> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Title)
                .IsRequired();

            builder.HasMany(x => x.Students)
               .WithOne(x => x.AcademicGroup)
               .HasForeignKey(x => x.AcademicGroupId);

            builder.HasMany(x => x.GroupEvents)
                .WithOne(x => x.AcademicGroup)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasMany(x => x.GroupScheduleChangeRequests)
                .WithOne(x => x.AcademicGroup)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}