﻿using AuRooM.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuRooM.Infrastructure.EntityConfigurations
{
    public class GroupScheduleChangeRequestConfiguration : IEntityTypeConfiguration<GroupScheduleChangeRequest>
    {
        public void Configure(EntityTypeBuilder<GroupScheduleChangeRequest> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasAlternateKey(
                x => new
                {
                    x.AcademicGroupId,
                    x.ScheduleChangeRequestId
                });

            builder.HasOne(x => x.AcademicGroup)
                .WithMany(x => x.GroupScheduleChangeRequests)
                .IsRequired()
                .HasForeignKey(x => x.AcademicGroupId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(x => x.ScheduleChangeRequest)
                .WithMany(x => x.GroupScheduleChangeRequests)
                .IsRequired()
                .HasForeignKey(x => x.ScheduleChangeRequestId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
