﻿using AuRooM.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuRooM.Infrastructure.EntityConfigurations
{
    public class EventSetConfigutation : IEntityTypeConfiguration<EventSet>
    {
        public void Configure(EntityTypeBuilder<EventSet> builder)
        {
            builder.HasKey(x => x.Id);

            builder.OwnsOne(
                x => x.Period,
                x =>
                {
                    x.Property(timePeriod => timePeriod.Start)
                        .HasColumnName("StartTime");

                    x.Property(timePeriod => timePeriod.Finish)
                        .HasColumnName("FinishTime");
                });
        }
    }
}
