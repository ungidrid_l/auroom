﻿using AuRooM.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuRooM.Infrastructure.EntityConfigurations
{
    public class GroupEventConfiguration : IEntityTypeConfiguration<GroupEvent>
    {
        public void Configure(EntityTypeBuilder<GroupEvent> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasAlternateKey(
                x => new
                {
                    x.AcademicGroupId,
                    x.EventId
                });

            builder.HasOne(x => x.Event)
                .WithMany(x => x.GroupEvents)
                .HasForeignKey(x => x.EventId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(x => x.AcademicGroup)
                .WithMany(x => x.GroupEvents)
                .HasForeignKey(x => x.AcademicGroupId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}