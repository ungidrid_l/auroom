﻿using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces.Repositories;

namespace AuRooM.Infrastructure.Repositories
{
    public class UserEventRepository : Repository<UserEvent>, IUserEventRepository
    {
        public UserEventRepository(AuRooMContext context) : base(context) {}
    }
}