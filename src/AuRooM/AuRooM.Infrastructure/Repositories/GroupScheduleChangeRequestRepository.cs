﻿using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces.Repositories;

namespace AuRooM.Infrastructure.Repositories
{
    public class GroupScheduleChangeRequestRepository : Repository<GroupScheduleChangeRequest>, IGroupScheduleChangeRequestRepository
    {
        public GroupScheduleChangeRequestRepository(AuRooMContext context) : base(context) { }
    }
}
