﻿using AuRooM.Domain.Interfaces;
using AuRooM.Domain.Interfaces.Repositories;
using System;
using System.Threading.Tasks;

namespace AuRooM.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AuRooMContext _context;
        private bool _disposed = false;

        public UnitOfWork(AuRooMContext context)
        {
            _context = context;
            AcademicGroups = new AcademicGroupRepository(context);
            Auditories = new AuditoryRepository(context);
            Events = new EventRepository(context);
            EventSets = new EventSetRepository(context);
            GroupEvents = new GroupEventRepository(context);
            GroupScheduleChangeRequests = new GroupScheduleChangeRequestRepository(context);
            ScheduleChangeRequests = new ScheduleChangeRequestRepository(context);
            UserEvents = new UserEventRepository(context);
            Users = new UserRepository(context);
            Roles = new RoleRepository(context);
            NarratorScheduleChangeRequests = new NarratorScheduleChangeRequestRepository(context);
        }

        public IAcademicGroupRepository AcademicGroups { get; }
        public IAuditoryRepository Auditories { get; }
        public IEventRepository Events { get; }
        public IEventSetRepository EventSets { get; }
        public IGroupEventRepository GroupEvents { get; }
        public IGroupScheduleChangeRequestRepository GroupScheduleChangeRequests { get; }
        public IScheduleChangeRequestRepository ScheduleChangeRequests { get; }
        public IUserEventRepository UserEvents { get; }
        public IUserRepository Users { get; }
        public IRoleRepository Roles { get; }
        public INarratorScheduleChangeRequestRepository NarratorScheduleChangeRequests { get; }

        public async Task CommitAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _context.Dispose();
            }

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
    }
}
