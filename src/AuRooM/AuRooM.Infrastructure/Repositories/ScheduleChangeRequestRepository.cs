﻿using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces.Repositories;

namespace AuRooM.Infrastructure.Repositories
{
    public class ScheduleChangeRequestRepository : Repository<ScheduleChangeRequest>, IScheduleChangeRequestRepository
    {
        public ScheduleChangeRequestRepository(AuRooMContext context) : base(context) {}
    }
}