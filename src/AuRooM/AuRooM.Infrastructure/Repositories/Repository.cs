﻿using AuRooM.Domain.Interfaces;
using AuRooM.Domain.Interfaces.Repositories;
using AuRooM.Infrastructure.Helpers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuRooM.Infrastructure.Repositories
{
    // *Range methods are not virtual in order to be users as TemplateMethod
    public abstract class Repository<T> : IRepository<T> where T : class, IIdentifiable
    {
        protected readonly AuRooMContext Context;
        protected readonly DbSet<T> Set;

        public Repository(AuRooMContext context)
        {
            Context = context;
            Set = context.Set<T>();
        }

        public async Task<T> GetByIdAsync(Guid id) => await Set.FindAsync(id);

        public virtual async Task<IReadOnlyList<T>> GetAllAsync()
        {
            return await Set.ToListAsync();
        }

        public async Task<IReadOnlyList<T>> GetAsync(
            IFilteringSpecification<T> filter = null,
            IIncludeSpecification<T> include = null,
            IPagingSpecification paging = null,
            IOrderingSpecification<T> ordering = null)
        {
            return await ApplySpecification(filter, include, paging, ordering)
                       .ToListAsync();
        }

        public async Task<T> FirstOrDefaultAsync(IFilteringSpecification<T> filter, IIncludeSpecification<T> include)
        {
            return await ApplySpecification(filter, include)
                       .FirstOrDefaultAsync();
        }

        public async Task<int> CountAsync(IFilteringSpecification<T> filter = null)
        {
            return await ApplySpecification(filter)
                       .CountAsync();
        }

        public virtual void Add(T entity)
        {
            Set.Add(entity);
        }

        public void AddRange(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                Add(entity);
            }
        }

        public virtual void Remove(T entity)
        {
            Set.Remove(entity);
        }

        public void RemoveRange(IEnumerable<T> entities) 
        {
            foreach (var entity in entities)
            {
                Remove(entity);
            }
        }

        protected IQueryable<T> ApplySpecification(
            IFilteringSpecification<T> filter = null,
            IIncludeSpecification<T> include = null,
            IPagingSpecification paging = null,
            IOrderingSpecification<T> ordering = null)
        {
            return SpecificationEvaluator<T>.ApplySpecifications(
                Set,
                filter,
                include,
                paging,
                ordering);
        }
    }
}