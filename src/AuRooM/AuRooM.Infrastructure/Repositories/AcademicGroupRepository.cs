﻿using AuRooM.Common.DTOs;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;
using AuRooM.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuRooM.Infrastructure.Repositories
{
    public class AcademicGroupRepository : Repository<AcademicGroup>, IAcademicGroupRepository
    {
        public AcademicGroupRepository(AuRooMContext context) : base(context) {}

        public async Task<IReadOnlyList<SelectListItemDto>> GetKeyValuePairsAsync(IFilteringSpecification<AcademicGroup> specification = null)
        {
            return await ApplySpecification(specification)
                .Select(
                    x => new SelectListItemDto
                    {
                        Text = x.Title,
                        Value = x.Id.ToString()
                    })
                .OrderBy(x => x.Text)
                .ToListAsync();
        }
    }
}