﻿using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces.Repositories;

namespace AuRooM.Infrastructure.Repositories
{
    public class EventSetRepository : Repository<EventSet>, IEventSetRepository
    {
        public EventSetRepository(AuRooMContext context) : base(context) { }
    }
}
