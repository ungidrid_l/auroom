﻿using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces.Repositories;

namespace AuRooM.Infrastructure.Repositories
{
    public class NarratorScheduleChangeRequestRepository : Repository<NarratorScheduleChangeRequest>, INarratorScheduleChangeRequestRepository
    {
        public NarratorScheduleChangeRequestRepository(AuRooMContext context) : base(context) { }
    }
}
