﻿using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces.Repositories;

namespace AuRooM.Infrastructure.Repositories
{
    public class GroupEventRepository : Repository<GroupEvent>, IGroupEventRepository
    {
        public GroupEventRepository(AuRooMContext context) : base(context) {}
    }
}