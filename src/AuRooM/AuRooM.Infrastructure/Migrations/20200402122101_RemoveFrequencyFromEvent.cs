﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AuRooM.Infrastructure.Migrations
{
    public partial class RemoveFrequencyFromEvent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Frequency",
                table: "Events");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Frequency",
                table: "Events",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
