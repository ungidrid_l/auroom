﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace AuRooM.Infrastructure.Migrations
{
    public partial class AddGroupToScheduleChangeRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GroupEvents_AcademicGroups_AcademicGroupId",
                table: "GroupEvents");

            migrationBuilder.CreateTable(
                name: "GroupScheduleChangeRequests",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AcademicGroupId = table.Column<Guid>(nullable: false),
                    ScheduleChangeRequestId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupScheduleChangeRequests", x => x.Id);
                    table.UniqueConstraint("AK_GroupScheduleChangeRequests_AcademicGroupId_ScheduleChangeRequestId", x => new { x.AcademicGroupId, x.ScheduleChangeRequestId });
                    table.ForeignKey(
                        name: "FK_GroupScheduleChangeRequests_AcademicGroups_AcademicGroupId",
                        column: x => x.AcademicGroupId,
                        principalTable: "AcademicGroups",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_GroupScheduleChangeRequests_ScheduleChangeRequests_ScheduleChangeRequestId",
                        column: x => x.ScheduleChangeRequestId,
                        principalTable: "ScheduleChangeRequests",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_GroupScheduleChangeRequests_ScheduleChangeRequestId",
                table: "GroupScheduleChangeRequests",
                column: "ScheduleChangeRequestId");

            migrationBuilder.AddForeignKey(
                name: "FK_GroupEvents_AcademicGroups_AcademicGroupId",
                table: "GroupEvents",
                column: "AcademicGroupId",
                principalTable: "AcademicGroups",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GroupEvents_AcademicGroups_AcademicGroupId",
                table: "GroupEvents");

            migrationBuilder.DropTable(
                name: "GroupScheduleChangeRequests");

            migrationBuilder.AddForeignKey(
                name: "FK_GroupEvents_AcademicGroups_AcademicGroupId",
                table: "GroupEvents",
                column: "AcademicGroupId",
                principalTable: "AcademicGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
