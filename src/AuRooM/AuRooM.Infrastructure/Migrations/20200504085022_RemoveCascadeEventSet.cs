﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AuRooM.Infrastructure.Migrations
{
    public partial class RemoveCascadeEventSet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Events_EventSets_EventSetId",
                table: "Events");

            migrationBuilder.AddForeignKey(
                name: "FK_Events_EventSets_EventSetId",
                table: "Events",
                column: "EventSetId",
                principalTable: "EventSets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Events_EventSets_EventSetId",
                table: "Events");

            migrationBuilder.AddForeignKey(
                name: "FK_Events_EventSets_EventSetId",
                table: "Events",
                column: "EventSetId",
                principalTable: "EventSets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
