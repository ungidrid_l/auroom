﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AuRooM.Infrastructure.Migrations
{
    public partial class AddPropertyToScheduleChangeRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsAppliedForEventSet",
                table: "ScheduleChangeRequests",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsAppliedForEventSet",
                table: "ScheduleChangeRequests");
        }
    }
}
