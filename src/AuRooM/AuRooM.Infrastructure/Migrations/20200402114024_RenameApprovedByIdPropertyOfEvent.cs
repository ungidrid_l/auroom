﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace AuRooM.Infrastructure.Migrations
{
    public partial class RenameApprovedByIdPropertyOfEvent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleChangeRequests_AspNetUsers_ApprovedById",
                table: "ScheduleChangeRequests");

            migrationBuilder.DropIndex(
                name: "IX_ScheduleChangeRequests_ApprovedById",
                table: "ScheduleChangeRequests");

            migrationBuilder.DropColumn(
                name: "ApprovedById",
                table: "ScheduleChangeRequests");

            migrationBuilder.AddColumn<Guid>(
                name: "StatusChangedById",
                table: "ScheduleChangeRequests",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ScheduleChangeRequests_StatusChangedById",
                table: "ScheduleChangeRequests",
                column: "StatusChangedById");

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleChangeRequests_AspNetUsers_StatusChangedById",
                table: "ScheduleChangeRequests",
                column: "StatusChangedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleChangeRequests_AspNetUsers_StatusChangedById",
                table: "ScheduleChangeRequests");

            migrationBuilder.DropIndex(
                name: "IX_ScheduleChangeRequests_StatusChangedById",
                table: "ScheduleChangeRequests");

            migrationBuilder.DropColumn(
                name: "StatusChangedById",
                table: "ScheduleChangeRequests");

            migrationBuilder.AddColumn<Guid>(
                name: "ApprovedById",
                table: "ScheduleChangeRequests",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ScheduleChangeRequests_ApprovedById",
                table: "ScheduleChangeRequests",
                column: "ApprovedById");

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleChangeRequests_AspNetUsers_ApprovedById",
                table: "ScheduleChangeRequests",
                column: "ApprovedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
