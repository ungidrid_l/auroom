﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace AuRooM.Infrastructure.Migrations
{
    public partial class AddEventSetTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EventGroupToken",
                table: "Events");

            migrationBuilder.AddColumn<Guid>(
                name: "EventSetId",
                table: "Events",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "EventSets",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    StartTime = table.Column<DateTime>(nullable: true),
                    FinishTime = table.Column<DateTime>(nullable: true),
                    Frequency = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventSets", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Events_EventSetId",
                table: "Events",
                column: "EventSetId");

            migrationBuilder.AddForeignKey(
                name: "FK_Events_EventSets_EventSetId",
                table: "Events",
                column: "EventSetId",
                principalTable: "EventSets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Events_EventSets_EventSetId",
                table: "Events");

            migrationBuilder.DropTable(
                name: "EventSets");

            migrationBuilder.DropIndex(
                name: "IX_Events_EventSetId",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "EventSetId",
                table: "Events");

            migrationBuilder.AddColumn<Guid>(
                name: "EventGroupToken",
                table: "Events",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }
    }
}
