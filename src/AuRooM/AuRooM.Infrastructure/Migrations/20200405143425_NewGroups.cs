﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace AuRooM.Infrastructure.Migrations
{
    public partial class NewGroups : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AcademicGroups",
                columns: new[] { "Id", "Title" },
                values: new object[,]
                {
                    { new Guid("46d70e2f-b804-4ca3-92ef-f2bee8f2b6c3"), "PMI-21" },
                    { new Guid("a6aadb6f-f907-492e-88fb-2cd8ac6cf8ca"), "PMP-21" },
                    { new Guid("13321379-837d-4501-a9e1-c6eb2c56c406"), "PMO-21" },
                    { new Guid("6bdebac5-a32b-4944-85da-8ce3d529ca78"), "PMA-21" },
                    { new Guid("d3a43740-5eb3-481b-bd8e-eaee15ac351a"), "PMI-22" },
                    { new Guid("0024495b-81fd-4e09-ab2b-39b1d178dc7f"), "PMI-41" },
                    { new Guid("b8d91a97-65d4-4943-a71c-fc8a5e4169b8"), "PMP-41" },
                    { new Guid("1b1c2d39-283b-4cfa-ba05-f30c4907f64d"), "PMO-41" },
                    { new Guid("5ed03158-3549-4ec4-a64c-534194a2aa25"), "PMA-41" },
                    { new Guid("011e94ba-fa84-4abd-b22a-ec31a76d8691"), "PMI-32" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AcademicGroups",
                keyColumn: "Id",
                keyValue: new Guid("0024495b-81fd-4e09-ab2b-39b1d178dc7f"));

            migrationBuilder.DeleteData(
                table: "AcademicGroups",
                keyColumn: "Id",
                keyValue: new Guid("011e94ba-fa84-4abd-b22a-ec31a76d8691"));

            migrationBuilder.DeleteData(
                table: "AcademicGroups",
                keyColumn: "Id",
                keyValue: new Guid("13321379-837d-4501-a9e1-c6eb2c56c406"));

            migrationBuilder.DeleteData(
                table: "AcademicGroups",
                keyColumn: "Id",
                keyValue: new Guid("1b1c2d39-283b-4cfa-ba05-f30c4907f64d"));

            migrationBuilder.DeleteData(
                table: "AcademicGroups",
                keyColumn: "Id",
                keyValue: new Guid("46d70e2f-b804-4ca3-92ef-f2bee8f2b6c3"));

            migrationBuilder.DeleteData(
                table: "AcademicGroups",
                keyColumn: "Id",
                keyValue: new Guid("5ed03158-3549-4ec4-a64c-534194a2aa25"));

            migrationBuilder.DeleteData(
                table: "AcademicGroups",
                keyColumn: "Id",
                keyValue: new Guid("6bdebac5-a32b-4944-85da-8ce3d529ca78"));

            migrationBuilder.DeleteData(
                table: "AcademicGroups",
                keyColumn: "Id",
                keyValue: new Guid("a6aadb6f-f907-492e-88fb-2cd8ac6cf8ca"));

            migrationBuilder.DeleteData(
                table: "AcademicGroups",
                keyColumn: "Id",
                keyValue: new Guid("b8d91a97-65d4-4943-a71c-fc8a5e4169b8"));

            migrationBuilder.DeleteData(
                table: "AcademicGroups",
                keyColumn: "Id",
                keyValue: new Guid("d3a43740-5eb3-481b-bd8e-eaee15ac351a"));
        }
    }
}
