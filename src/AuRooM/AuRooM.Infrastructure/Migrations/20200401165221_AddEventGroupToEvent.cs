﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace AuRooM.Infrastructure.Migrations
{
    public partial class AddEventGroupToEvent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FinishPeriod",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "StartPeriod",
                table: "Events");

            migrationBuilder.AddColumn<Guid>(
                name: "EventGroupToken",
                table: "Events",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EventGroupToken",
                table: "Events");

            migrationBuilder.AddColumn<DateTime>(
                name: "FinishPeriod",
                table: "Events",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartPeriod",
                table: "Events",
                type: "datetime2",
                nullable: true);
        }
    }
}
