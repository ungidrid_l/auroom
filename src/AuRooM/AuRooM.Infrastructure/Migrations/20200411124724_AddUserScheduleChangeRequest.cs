﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace AuRooM.Infrastructure.Migrations
{
    public partial class AddUserScheduleChangeRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsNarrator",
                table: "UserEvents",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "NarratorScheduleChangeRequests",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    ScheduleChangeRequestId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NarratorScheduleChangeRequests", x => x.Id);
                    table.UniqueConstraint("AK_NarratorScheduleChangeRequests_UserId_ScheduleChangeRequestId", x => new { x.UserId, x.ScheduleChangeRequestId });
                    table.ForeignKey(
                        name: "FK_NarratorScheduleChangeRequests_ScheduleChangeRequests_ScheduleChangeRequestId",
                        column: x => x.ScheduleChangeRequestId,
                        principalTable: "ScheduleChangeRequests",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_NarratorScheduleChangeRequests_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_NarratorScheduleChangeRequests_ScheduleChangeRequestId",
                table: "NarratorScheduleChangeRequests",
                column: "ScheduleChangeRequestId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NarratorScheduleChangeRequests");

            migrationBuilder.DropColumn(
                name: "IsNarrator",
                table: "UserEvents");
        }
    }
}
