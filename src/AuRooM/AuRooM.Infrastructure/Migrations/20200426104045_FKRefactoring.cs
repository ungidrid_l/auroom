﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace AuRooM.Infrastructure.Migrations
{
    public partial class FKRefactoring : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_AcademicGroups_AcademicGroupId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_Events_Auditories_AuditoryId",
                table: "Events");

            migrationBuilder.DropForeignKey(
                name: "FK_Events_EventSets_EventSetId",
                table: "Events");

            migrationBuilder.DropForeignKey(
                name: "FK_Events_AspNetUsers_PersonInChargeId",
                table: "Events");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupEvents_AcademicGroups_AcademicGroupId",
                table: "GroupEvents");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupEvents_Events_EventId",
                table: "GroupEvents");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupScheduleChangeRequests_AcademicGroups_AcademicGroupId",
                table: "GroupScheduleChangeRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupScheduleChangeRequests_ScheduleChangeRequests_ScheduleChangeRequestId",
                table: "GroupScheduleChangeRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_NarratorScheduleChangeRequests_ScheduleChangeRequests_ScheduleChangeRequestId",
                table: "NarratorScheduleChangeRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_NarratorScheduleChangeRequests_AspNetUsers_UserId",
                table: "NarratorScheduleChangeRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleChangeRequests_Events_EventId",
                table: "ScheduleChangeRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleChangeRequests_AspNetUsers_RequestedById",
                table: "ScheduleChangeRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_UserEvents_Events_EventId",
                table: "UserEvents");

            migrationBuilder.AlterColumn<Guid>(
                name: "RequestedById",
                table: "ScheduleChangeRequests",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Events",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_AcademicGroups_AcademicGroupId",
                table: "AspNetUsers",
                column: "AcademicGroupId",
                principalTable: "AcademicGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Events_Auditories_AuditoryId",
                table: "Events",
                column: "AuditoryId",
                principalTable: "Auditories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Events_EventSets_EventSetId",
                table: "Events",
                column: "EventSetId",
                principalTable: "EventSets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Events_AspNetUsers_PersonInChargeId",
                table: "Events",
                column: "PersonInChargeId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GroupEvents_AcademicGroups_AcademicGroupId",
                table: "GroupEvents",
                column: "AcademicGroupId",
                principalTable: "AcademicGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GroupEvents_Events_EventId",
                table: "GroupEvents",
                column: "EventId",
                principalTable: "Events",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GroupScheduleChangeRequests_AcademicGroups_AcademicGroupId",
                table: "GroupScheduleChangeRequests",
                column: "AcademicGroupId",
                principalTable: "AcademicGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GroupScheduleChangeRequests_ScheduleChangeRequests_ScheduleChangeRequestId",
                table: "GroupScheduleChangeRequests",
                column: "ScheduleChangeRequestId",
                principalTable: "ScheduleChangeRequests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NarratorScheduleChangeRequests_ScheduleChangeRequests_ScheduleChangeRequestId",
                table: "NarratorScheduleChangeRequests",
                column: "ScheduleChangeRequestId",
                principalTable: "ScheduleChangeRequests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NarratorScheduleChangeRequests_AspNetUsers_UserId",
                table: "NarratorScheduleChangeRequests",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleChangeRequests_Events_EventId",
                table: "ScheduleChangeRequests",
                column: "EventId",
                principalTable: "Events",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleChangeRequests_AspNetUsers_RequestedById",
                table: "ScheduleChangeRequests",
                column: "RequestedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_UserEvents_Events_EventId",
                table: "UserEvents",
                column: "EventId",
                principalTable: "Events",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_AcademicGroups_AcademicGroupId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_Events_Auditories_AuditoryId",
                table: "Events");

            migrationBuilder.DropForeignKey(
                name: "FK_Events_EventSets_EventSetId",
                table: "Events");

            migrationBuilder.DropForeignKey(
                name: "FK_Events_AspNetUsers_PersonInChargeId",
                table: "Events");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupEvents_AcademicGroups_AcademicGroupId",
                table: "GroupEvents");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupEvents_Events_EventId",
                table: "GroupEvents");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupScheduleChangeRequests_AcademicGroups_AcademicGroupId",
                table: "GroupScheduleChangeRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupScheduleChangeRequests_ScheduleChangeRequests_ScheduleChangeRequestId",
                table: "GroupScheduleChangeRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_NarratorScheduleChangeRequests_ScheduleChangeRequests_ScheduleChangeRequestId",
                table: "NarratorScheduleChangeRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_NarratorScheduleChangeRequests_AspNetUsers_UserId",
                table: "NarratorScheduleChangeRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleChangeRequests_Events_EventId",
                table: "ScheduleChangeRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleChangeRequests_AspNetUsers_RequestedById",
                table: "ScheduleChangeRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_UserEvents_Events_EventId",
                table: "UserEvents");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Events");

            migrationBuilder.AlterColumn<Guid>(
                name: "RequestedById",
                table: "ScheduleChangeRequests",
                type: "uniqueidentifier",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_AcademicGroups_AcademicGroupId",
                table: "AspNetUsers",
                column: "AcademicGroupId",
                principalTable: "AcademicGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Events_Auditories_AuditoryId",
                table: "Events",
                column: "AuditoryId",
                principalTable: "Auditories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Events_EventSets_EventSetId",
                table: "Events",
                column: "EventSetId",
                principalTable: "EventSets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Events_AspNetUsers_PersonInChargeId",
                table: "Events",
                column: "PersonInChargeId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GroupEvents_AcademicGroups_AcademicGroupId",
                table: "GroupEvents",
                column: "AcademicGroupId",
                principalTable: "AcademicGroups",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_GroupEvents_Events_EventId",
                table: "GroupEvents",
                column: "EventId",
                principalTable: "Events",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_GroupScheduleChangeRequests_AcademicGroups_AcademicGroupId",
                table: "GroupScheduleChangeRequests",
                column: "AcademicGroupId",
                principalTable: "AcademicGroups",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_GroupScheduleChangeRequests_ScheduleChangeRequests_ScheduleChangeRequestId",
                table: "GroupScheduleChangeRequests",
                column: "ScheduleChangeRequestId",
                principalTable: "ScheduleChangeRequests",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_NarratorScheduleChangeRequests_ScheduleChangeRequests_ScheduleChangeRequestId",
                table: "NarratorScheduleChangeRequests",
                column: "ScheduleChangeRequestId",
                principalTable: "ScheduleChangeRequests",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_NarratorScheduleChangeRequests_AspNetUsers_UserId",
                table: "NarratorScheduleChangeRequests",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleChangeRequests_Events_EventId",
                table: "ScheduleChangeRequests",
                column: "EventId",
                principalTable: "Events",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleChangeRequests_AspNetUsers_RequestedById",
                table: "ScheduleChangeRequests",
                column: "RequestedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserEvents_Events_EventId",
                table: "UserEvents",
                column: "EventId",
                principalTable: "Events",
                principalColumn: "Id");
        }
    }
}
