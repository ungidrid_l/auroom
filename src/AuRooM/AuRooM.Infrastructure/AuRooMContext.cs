﻿using AuRooM.Domain.Entities;
using AuRooM.Infrastructure.Extensions;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Reflection;

namespace AuRooM.Infrastructure
{
    public class AuRooMContext : IdentityDbContext<User, Role, Guid>
    {
        public AuRooMContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<AcademicGroup> AcademicGroups { get; set; }
        public DbSet<Auditory> Auditories { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<EventSet> EventSets { get; set; }
        public DbSet<GroupEvent> GroupEvents { get; set; }
        public DbSet<GroupScheduleChangeRequest> GroupScheduleChangeRequests { get; set; }
        public DbSet<ScheduleChangeRequest> ScheduleChangeRequests { get; set; }
        public DbSet<UserEvent> UserEvents { get; set; }
        public new DbSet<User> Users { get; set; }
        public new DbSet<Role> Roles { get; set; }
        public new DbSet<UserRole> UserRoles { get; set; }
        public DbSet<NarratorScheduleChangeRequest> NarratorScheduleChangeRequests { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            builder.Seed();
        }
    }
}