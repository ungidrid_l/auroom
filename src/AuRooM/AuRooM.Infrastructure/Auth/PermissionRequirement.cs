﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;

namespace AuRooM.Infrastructure.Auth
{
    public class PermissionRequirement : IAuthorizationRequirement
    {
        public IEnumerable<string> Permissions { get; }

        public PermissionRequirement(IEnumerable<string> permissions)
        {
            Permissions = permissions ?? throw new ArgumentNullException(nameof(permissions));
        }
    }
}