﻿namespace AuRooM.Infrastructure.Auth
{
    public static class PermissionConstants
    {
        public const string PermissionPolicyPrefix = "Permission";
        public const string PermissionSplitSymbol = ",";
    }
}