﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace AuRooM.Infrastructure.Auth
{
    public class AuthorizationPolicyProvider : DefaultAuthorizationPolicyProvider
    {
        private readonly ConcurrentDictionary<string, Lazy<AuthorizationPolicy>> _policies =
            new ConcurrentDictionary<string, Lazy<AuthorizationPolicy>>(StringComparer.OrdinalIgnoreCase);

        public AuthorizationPolicyProvider(IOptions<AuthorizationOptions> options) : base(options) {}

        public override Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
        {
            if (!policyName.StartsWith(PermissionConstants.PermissionPolicyPrefix, StringComparison.InvariantCultureIgnoreCase))
                return base.GetPolicyAsync(policyName);

            var policy = _policies.GetOrAdd(
                policyName,
                k => new Lazy<AuthorizationPolicy>(
                    () =>
                    {
                        var permissionNames = policyName.Substring(PermissionConstants.PermissionPolicyPrefix.Length)
                            .Split(PermissionConstants.PermissionSplitSymbol);

                        return new AuthorizationPolicyBuilder().RequireAuthenticatedUser()
                            .AddRequirements(new PermissionRequirement(permissionNames))
                            .Build();
                    },
                    LazyThreadSafetyMode.ExecutionAndPublication));

            return Task.FromResult(policy.Value);
        }
    }
}