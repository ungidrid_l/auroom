﻿using AuRooM.Application.Common.Extensions;
using AuRooM.Common.Constants;
using System;
using System.Linq;
using System.Security.Claims;

namespace AuRooM.Infrastructure.Auth
{
    public static class PrincipalExtensions
    {
        public static bool HasPermission(this ClaimsPrincipal principal, string permissionName)
        {
            if (principal == null) return false;
            return principal.Claims.ContainsPermission(permissionName);
        }
    }
}