﻿using AuRooM.Domain.Interfaces;
using System.Linq;

namespace AuRooM.Infrastructure.Helpers
{
    public static class SpecificationEvaluator<T>
    {
        public static IQueryable<T> ApplySpecifications(
            IQueryable<T> query,
            IFilteringSpecification<T> filter = null,
            IIncludeSpecification<T> include = null,
            IPagingSpecification paging = null,
            IOrderingSpecification<T> ordering = null)
        {
            if (filter != null)
                query = query.Where(filter.ToExpression());

            if (include != null)
                query = include.Includer.Apply(query);

            if (ordering != null)
            {
                if (ordering.OrderByType == OrderByType.Asc)
                    query = query.OrderBy(ordering.ToExpression());
                else 
                    query = query.OrderByDescending(ordering.ToExpression());
            }

            if (paging != null)
            {
                query = query.Skip(paging.Skip)
                    .Take(paging.Take);
            }

            return query;
        }
    }
}