﻿using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;
using AuRooM.Domain.Settings;
using AuRooM.Infrastructure.Auth;
using AuRooM.Infrastructure.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace AuRooM.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            // Entity framework
            serviceCollection.AddDbContext<AuRooMContext>(
                options => options.UseSqlServer(configuration.GetConnectionString("AuRooMDb")));

            // Identity
            serviceCollection.AddIdentity<User, Role>(opts =>
                {
                    opts.Password.RequireUppercase = false;
                    opts.Password.RequireNonAlphanumeric = false;
                    opts.User.RequireUniqueEmail = true;
                })
                .AddUserStore<UserStore<User, Role, AuRooMContext, Guid, IdentityUserClaim<Guid>, UserRole, IdentityUserLogin<Guid>, IdentityUserToken<Guid>, IdentityRoleClaim<Guid>>>()
                .AddRoleStore<RoleStore<Role, AuRooMContext, Guid, UserRole, IdentityRoleClaim<Guid>>>()
                .AddDefaultTokenProviders();

            serviceCollection.AddSingleton<IAuthorizationPolicyProvider, AuthorizationPolicyProvider>();
            serviceCollection.AddSingleton<IAuthorizationHandler, PermissionRequirementHandler>();

            // Services
            serviceCollection.AddScoped<IUnitOfWork, UnitOfWork>();

            // Settings
            serviceCollection.Configure<RoleSettings>(configuration.GetSection("RoleIds"));
            return serviceCollection;
        }
    }
}