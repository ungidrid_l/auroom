﻿using AuRooM.Application.Features.Events.Queries.GetAgenda;
using AuRooM.Application.Specifications;
using AuRooM.Application.Specifications.EventSpecifications.Order;
using AuRooM.Common.DTOs;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AuRooM.Test.Tests.Events
{
    public class GetAgendaTest
    {
        public Mock<IUnitOfWork> _mockUnitOfWork;
        public Mock<IMapper> _mockMapper;
        public List<Auditory> _auditories;
        public List<User> _narrators;
        public List<Event> _events;
        public GetAgendaQueryHandler _getAgendaQueryHandler;

        public GetAgendaTest()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();

            _mockMapper = new Mock<IMapper>();
            _mockMapper.Setup(x => x.Map<SelectListItem>(It.IsAny<SelectListItemDto>()))
                .Returns<SelectListItemDto>(
                    s => new SelectListItem(s.Text, s.Value));

            _getAgendaQueryHandler = new GetAgendaQueryHandler(_mockUnitOfWork.Object, _mockMapper.Object);

            _auditories = new List<Auditory>()
            {
                new Auditory()
                {
                    Id = Guid.Parse("E02FA2D2-1CFD-4B95-BA27-6718684F2509"),
                    Number = 111
                },
                new Auditory()
                {
                    Id = Guid.Parse("1299ED9A-95BE-4020-B2A6-235EB64F4973"),
                    Number = 439
                },
                new Auditory()
                {
                    Id = Guid.Parse("A1AA8785-A1B6-4E59-B64A-C940E1578690"),
                    Number = 117
                }
            };
            _narrators = new List<User>()
            {
                new User()
                {
                    Id = Guid.Parse("DA48370F-6443-4334-938F-51D8A179C330"),
                    FirstName = "Anatoliy",
                    LastName = "Muzychuk",
                    UserStatus = UserStatus.Active,
                    DocumentInfo = "111AM",
                },
                new User()
                {
                    Id = Guid.Parse("3358C87F-56E9-420F-B6FF-3A9A3675A2AA"),
                    FirstName = "Sviatoslav",
                    LastName = "Tarasiuk",
                    UserStatus = UserStatus.Active,
                    DocumentInfo = "111ST"
                },
                new User()
                {
                    Id = Guid.Parse("A78C1993-9159-491A-A9B1-21018EFF25B6"),
                    FirstName = "Admin",
                    LastName = "Admin",
                    UserStatus = UserStatus.Active,
                    DocumentInfo = "Admin"
                }
            };

            _events = new List<Event>()
            {
                new Event()
                {
                    Id = Guid.Parse("532807AD-72FE-4F77-B0D7-4F79DA592A71"),
                    Name = "Programing",
                    Timing = new TimePeriod()
                    {
                        Start = new DateTime(
                            2020,
                            3,
                            1,
                            10,
                            10,
                            0),
                        Finish = new DateTime(
                            2020,
                            3,
                            1,
                            11,
                            30,
                            0)
                    },                   
                    Type = EventType.Lecture,
                    PersonInChargeId = _narrators[0]
                        .Id,
                    PersonInCharge = _narrators[0],
                    AuditoryId = _auditories[0]
                        .Id,
                    Auditory = _auditories[0]
                },
                new Event()
                {
                    Id = Guid.Parse("17340A5F-2BFD-4725-BE6F-09DF6C69964F"),
                    Name = "Math",
                    Timing = new TimePeriod()
                    {
                        Start = new DateTime(
                            2020,
                            3,
                            2,
                            10,
                            10,
                            0),
                        Finish = new DateTime(
                            2021,
                            3,
                            2,
                            11,
                            30,
                            0)
                    },                 
                    Type = EventType.Lecture,
                    PersonInChargeId = _narrators[1]
                        .Id,
                    PersonInCharge = _narrators[1],
                    AuditoryId = _auditories[1]
                        .Id,
                    Auditory = _auditories[1]
                },
                new Event()
                {
                    Id = Guid.Parse("D2992D94-877B-4152-B437-BE3C5DE1F9EF"),
                    Name = "Programing",
                    Timing = new TimePeriod()
                    {
                        Start = new DateTime(
                            2020,
                            3,
                            3,
                            10,
                            10,
                            0),
                        Finish = new DateTime(
                            2021,
                            3,
                            3,
                            11,
                            30,
                            0)
                    },                   
                    Type = EventType.Class,
                    PersonInChargeId = _narrators[0]
                        .Id,
                    PersonInCharge = _narrators[0],
                    AuditoryId = _auditories[2]
                        .Id,
                    Auditory = _auditories[2]
                }
            };
        }

        [Fact]
        public async Task GetAgendaTest_ReturnsSuccesfulResult()
        {
            // Arrange
            _mockMapper.Setup(x => x.Map<EventForAgendaVM>(It.IsAny<Event>()))
                .Returns<Event>(
                s => new EventForAgendaVM()
                { 
                    Id = s.Id,
                    AuditoryNumber = s.Auditory.Number.ToString(),
                    Name = s.Name,
                    Teachers = new List<string>() { s.PersonInChargeId.ToString() },
                    Timing = s.Timing,
                    Type = s.Type.ToString()
                });

            _mockUnitOfWork.Setup(
                x => x.Auditories.GetKeyValuePairsAsync(It.IsAny<IFilteringSpecification<Auditory>>()))
                .Returns(Task.FromResult((IReadOnlyList<SelectListItemDto>)_auditories.Select(
                    x => new SelectListItemDto()
                    {
                        Text = x.Number.ToString(),
                        Value = x.Id.ToString()
                    }).ToList()));

            _mockUnitOfWork.Setup(
                x => x.Users.GetKeyValuePairsAsync(It.IsAny<IFilteringSpecification<User>>()))
                .Returns(Task.FromResult((IReadOnlyList<SelectListItemDto>)_narrators.Select(
                    x => new SelectListItemDto()
                    {
                        Text = x.GetFullName(),
                        Value = x.Id.ToString()
                    }).ToList()));

            _mockUnitOfWork.Setup(
                x => x.Events.GetAsync(
                    It.IsAny<IFilteringSpecification<Event>>(),
                    It.IsAny<IIncludeSpecification<Event>>(),
                    It.IsAny<IPagingSpecification>(),
                    It.IsAny<IOrderingSpecification<Event>>()))
                .Returns(Task.FromResult((IReadOnlyList<Event>)_events));

            var request = new GetAgendaQuery();

            // Act
            var result = await _getAgendaQueryHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Succeeded);

            Assert.Equal(3, result.Data.Auditories.Count());
            Assert.Equal(3, result.Data.Narrators.Count());
            Assert.Equal(3, result.Data.EventList.Count());

            foreach (var i in result.Data.Auditories)
            {
                var auditory = _auditories.FirstOrDefault(x => x.Id.ToString() == i.Value);
                Assert.Equal(auditory.Number.ToString(), i.Text);
            }

            foreach (var i in result.Data.Narrators)
            {
                var narrator = _narrators.FirstOrDefault(x => x.Id.ToString() == i.Value);
                Assert.Equal(narrator.GetFullName(), i.Text);
            }

            foreach (var dayLookup in result.Data.EventList)
            {
                foreach (var i in dayLookup.Take(1))
                {
                    var singleEvent = _events.FirstOrDefault(x => x.Id.ToString() == i.Id.ToString());
                    Assert.Equal(singleEvent.Name, i.Name);
                }
            }
        }
    }
}
