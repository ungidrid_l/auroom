﻿using AuRooM.Application.Features.Events.Commands.Decline;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AuRooM.Test.Tests.Events
{
    public class DeclineEventTest
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;    
        private readonly ScheduleChangeRequest _scheduleChangeRequest;
        private readonly DeclineEventCommandHandler _declineEventCommandHandler;

        public DeclineEventTest()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();
            
            _declineEventCommandHandler = new DeclineEventCommandHandler(_mockUnitOfWork.Object);

            _scheduleChangeRequest = new ScheduleChangeRequest()
            {
                Id = Guid.Parse("E1E25FA0-A2C6-4C10-9C75-D2DBA70A9BBE"),
                Name = "Math",
                Timing = new TimePeriod()
                {
                    Start = new DateTime(2020, 04, 03, 11, 50, 0),
                    Finish = new DateTime(2021, 04, 03, 13, 10, 0)
                },
                Period = new TimePeriod()
                {
                    Start = new DateTime(2020, 04, 03),
                    Finish = new DateTime(2021, 04, 03)
                },
                Frequency = 1,
                RequestDate = new DateTime(2020, 04, 06),
                EventType = EventType.Lecture,
                AuditoryId = Guid.Parse("CD1BED7A-AC38-4423-87CB-F0D540D09E47"),
                Auditory = new Auditory() { Id = Guid.Parse("20B1858D-2134-4CDD-9941-C61BA77738AE"), Number = 117 },
                RequestStatus = ChangeRequestStatus.Pending
            };
        }

        [Fact]
        public async Task DeclineEventTest_ReturnsSuccessfulResult()
        {
            // Arrange
            _mockUnitOfWork.Setup(x => x.ScheduleChangeRequests.GetByIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(_scheduleChangeRequest));

            var request = new DeclineEventCommand() { DeclinedById = Guid.NewGuid() };

            // Act
            var result = await _declineEventCommandHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Succeeded);
            Assert.Equal(ChangeRequestStatus.Declined, _scheduleChangeRequest.RequestStatus);
            Assert.Equal(request.DeclinedById, _scheduleChangeRequest.StatusChangedById);
        }
    }
}
