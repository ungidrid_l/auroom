﻿using AuRooM.Application.Features.Events.Commands.Create;
using AuRooM.Application.Features.Events.Commands.Decline;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using AuRooM.Application.Common.Models;
using AuRooM.Application.Services.Interfaces;
using Xunit;

namespace AuRooM.Test.Tests.Events
{
    public class CreateEventTest
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;
        private readonly Mock<IMapper> _mockMapper;
        private readonly ScheduleChangeRequest _scheduleChangeRequest;
        private readonly CreateEventCommandHandler _createEventCommandHandler;
        private Mock<IEventService> _mockEventService;

        public CreateEventTest()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();

            _mockMapper = new Mock<IMapper>();

            _mockEventService = new Mock<IEventService>();

            _createEventCommandHandler = new CreateEventCommandHandler(_mockUnitOfWork.Object, _mockMapper.Object, _mockEventService.Object);

            _scheduleChangeRequest = new ScheduleChangeRequest()
            {
                Id = Guid.Parse("E1E25FA0-A2C6-4C10-9C75-D2DBA70A9BBE"),
                Name = "Math",
                Timing = new TimePeriod()
                {
                    Start = new DateTime(2020, 04, 03, 11, 50, 0),
                    Finish = new DateTime(2021, 04, 03, 13, 10, 0)
                },
                Period = new TimePeriod()
                {
                    Start = new DateTime(2020, 04, 03),
                    Finish = new DateTime(2021, 04, 03)
                },
                Frequency = 1,
                RequestDate = new DateTime(2020, 04, 06),
                EventType = EventType.Lecture,
                AuditoryId = Guid.Parse("CD1BED7A-AC38-4423-87CB-F0D540D09E47"),
                Auditory = new Auditory() { Id = Guid.Parse("20B1858D-2134-4CDD-9941-C61BA77738AE"), Number = 117 },
                RequestStatus = ChangeRequestStatus.Pending,
                RequestType = ChangeRequestType.Create
            };
        }

        [Fact]
        public async Task CreateEventTest_GroupAndNarratorIdsAreNotNull_ReturnsSuccessfulResult()
        {
            // Arrange
            _mockMapper.Setup(x => x.Map<ScheduleChangeRequest>(It.IsAny<CreateEventCommand>()))
               .Returns<CreateEventCommand>(s => new ScheduleChangeRequest()
               {
                   Id = s.Id,
                   Name = s.Name,
                   Timing = s.Timing,
                   Period = s.Period,
                   Frequency = s.Frequency,
                   RequestDate = s.RequestDate,
                   RequestStatus = s.RequestStatus,
                   RequestType = s.RequestType,
                   EventType = s.EventType,
                   AuditoryId = s.AuditoryId,
                   EventId = s.EventId,
                   RequestedById = s.RequestedById,
                   StatusChangedById = s.ApprovedById,
               });

            var request = new CreateEventCommand()
            {
                Period = _scheduleChangeRequest.Period,
                AuditoryId = _scheduleChangeRequest.AuditoryId,
                Frequency = _scheduleChangeRequest.Frequency
            };

            request.GroupIds = new List<Guid?>() { Guid.Empty };
            request.NarratorIds = new List<Guid?>() { Guid.Empty };
            
            _mockUnitOfWork.Setup(x => x.GroupScheduleChangeRequests.AddRange(It.IsAny<IEnumerable<GroupScheduleChangeRequest>>()));                           
            
            _mockUnitOfWork.Setup(x => x.NarratorScheduleChangeRequests.AddRange(It.IsAny<IEnumerable<NarratorScheduleChangeRequest>>()));           

            _mockUnitOfWork.Setup(x => x.ScheduleChangeRequests.Add(It.IsAny<ScheduleChangeRequest>()));

            _mockEventService.Setup(
                    x => x.ValidateEventForCreation(
                        It.IsAny<Guid>(),
                        It.IsAny<DateTime>(),
                        It.IsAny<DateTime>(),
                        It.IsAny<int>()))
                .ReturnsAsync(Result.Success);

            // Act
            var result = await _createEventCommandHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Succeeded);
            _mockUnitOfWork.Verify(x => x.GroupScheduleChangeRequests.AddRange(It.IsAny<IEnumerable<GroupScheduleChangeRequest>>()), Times.Once);
            _mockUnitOfWork.Verify(x => x.NarratorScheduleChangeRequests.AddRange(It.IsAny<IEnumerable<NarratorScheduleChangeRequest>>()), Times.Once);
        }

        [Fact]
        public async Task CreateEventTest_GroupAndNarratorIdsAreNull_ReturnsSuccessfulResult()
        {
            // Arrange
            _mockMapper.Setup(x => x.Map<ScheduleChangeRequest>(It.IsAny<CreateEventCommand>()))
               .Returns<CreateEventCommand>(s => new ScheduleChangeRequest()
               {
                   Id = s.Id,
                   Name = s.Name,
                   Timing = s.Timing,
                   Period = s.Period,
                   Frequency = s.Frequency,
                   RequestDate = s.RequestDate,
                   RequestStatus = s.RequestStatus,
                   RequestType = s.RequestType,
                   EventType = s.EventType,
                   AuditoryId = s.AuditoryId,
                   EventId = s.EventId,
                   RequestedById = s.RequestedById,
                   StatusChangedById = s.ApprovedById,
               });

            var request = new CreateEventCommand()
            {
                Period = _scheduleChangeRequest.Period,
                AuditoryId = _scheduleChangeRequest.AuditoryId,
                Frequency = _scheduleChangeRequest.Frequency
            };

            _mockUnitOfWork.Setup(x => x.GroupScheduleChangeRequests.AddRange(It.IsAny<IEnumerable<GroupScheduleChangeRequest>>()));

            _mockUnitOfWork.Setup(x => x.NarratorScheduleChangeRequests.AddRange(It.IsAny<IEnumerable<NarratorScheduleChangeRequest>>()));

            _mockUnitOfWork.Setup(x => x.ScheduleChangeRequests.Add(It.IsAny<ScheduleChangeRequest>()));

            _mockEventService.Setup(
                    x => x.ValidateEventForCreation(
                        It.IsAny<Guid>(),
                        It.IsAny<DateTime>(),
                        It.IsAny<DateTime>(),
                        It.IsAny<int>()))
                .ReturnsAsync(Result.Success);

            // Act
            var result = await _createEventCommandHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Succeeded);                    
        }

        [Fact]
        public async Task CreateEventTest_ValidationFails_ReturnsFailedResut()
        {
            // Arrange
            _mockMapper.Setup(x => x.Map<ScheduleChangeRequest>(It.IsAny<CreateEventCommand>()));

            var request = new CreateEventCommand()
            {
                Period = _scheduleChangeRequest.Period,
                AuditoryId = _scheduleChangeRequest.AuditoryId,
                Frequency = _scheduleChangeRequest.Frequency
            };

            _mockEventService.Setup(
                    x => x.ValidateEventForCreation(
                        It.IsAny<Guid>(),
                        It.IsAny<DateTime>(),
                        It.IsAny<DateTime>(),
                        It.IsAny<int>()))
                .ReturnsAsync(Result.Failure("Error"));

            // Act
            var result = await _createEventCommandHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.False(result.Succeeded);
        }
    }
}
