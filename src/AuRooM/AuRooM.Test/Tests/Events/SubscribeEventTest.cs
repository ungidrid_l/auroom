﻿using AuRooM.Application.Features.Events.Commands.Subscribe;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AuRooM.Test.Tests.Events
{
    public class SubscribeEventTest
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;
        private readonly Mock<IMapper> _mockMapper;
        private readonly SubscribeToEventCommandHandler _subscribeToEventCommandHandler;
        private readonly Event _event;

        public SubscribeEventTest()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();

            _mockMapper = new Mock<IMapper>();

            _subscribeToEventCommandHandler = new SubscribeToEventCommandHandler(_mockUnitOfWork.Object, _mockMapper.Object);

            _event = new Event()
            {
                Id = Guid.Parse("D2992D94-877B-4152-B437-BE3C5DE1F9EF"),
                Name = "Programing",
                Timing = new TimePeriod()
                {
                    Start = new DateTime(
                            2020,
                            3,
                            3,
                            10,
                            10,
                            0),
                    Finish = new DateTime(
                            2021,
                            3,
                            3,
                            11,
                            30,
                            0)
                },
                Type = EventType.Class
            };
        }

        [Fact]
        public async Task SubscribeEventTest_ReturnsSuccessfulResult()
        {
            // Arrange
            _mockUnitOfWork.Setup(x => x.Events.FirstOrDefaultAsync(
                It.IsAny<IFilteringSpecification<Event>>(),
                It.IsAny<IIncludeSpecification<Event>>()))
                .Returns(Task.FromResult(_event));

            _mockUnitOfWork.Setup(x => x.Events.GetAsync(
                It.IsAny<IFilteringSpecification<Event>>(),
                It.IsAny<IIncludeSpecification<Event>>(),
                It.IsAny<IPagingSpecification>(),
                It.IsAny<IOrderingSpecification<Event>>()))
                .Returns(Task.FromResult((IReadOnlyList<Event>)new List<Event>() { _event }));

            _mockUnitOfWork.Setup(x => x.UserEvents.AddRange(It.IsAny<IEnumerable<UserEvent>>()));
            var request = new SubscribeToEventCommand();

            // Act
            var result = await _subscribeToEventCommandHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Succeeded);
        }
    }
}
