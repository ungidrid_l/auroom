﻿using AuRooM.Application.Common.Models;
using AuRooM.Application.Features.Events.Commands.ApproveDelete;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using Castle.DynamicProxy.Generators;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AuRooM.Test.Tests
{
    public class ApproveDeleteEventTest
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;
        private readonly ApproveDeleteCommandHandler _approveDeleteCommandHandler;
        private readonly ScheduleChangeRequest _scheduleChangeRequest;

        public ApproveDeleteEventTest()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();

            _approveDeleteCommandHandler = new ApproveDeleteCommandHandler(_mockUnitOfWork.Object);

            _scheduleChangeRequest = new ScheduleChangeRequest()
            {
                Id = Guid.Parse("E1E25FA0-A2C6-4C10-9C75-D2DBA70A9BBE"),
                Name = "Math",
                Timing = new TimePeriod()
                {
                    Start = new DateTime(2020, 04, 03, 11, 50, 0),
                    Finish = new DateTime(2021, 04, 03, 13, 10, 0)
                },
                Period = new TimePeriod()
                {
                    Start = new DateTime(2020, 04, 03),
                    Finish = new DateTime(2021, 04, 03)
                },
                Frequency = 1,
                RequestDate = new DateTime(2020, 04, 06),
                EventType = EventType.Lecture,
                AuditoryId = Guid.Parse("CD1BED7A-AC38-4423-87CB-F0D540D09E47"),
                Auditory = new Auditory() { Id = Guid.Parse("20B1858D-2134-4CDD-9941-C61BA77738AE"), Number = 117 },
                RequestStatus = ChangeRequestStatus.Pending,
                RequestType = ChangeRequestType.Delete,
                Event = new Event()
                {
                    EventSet = new EventSet()
                }
            };
        }

        [Fact]
        public async Task ApproveDeleteEventTest_ChangeRequestTypeIsNotDelete_ReturnsFailure()
        {
            // Arrange
            _scheduleChangeRequest.RequestType = ChangeRequestType.Create;

            _mockUnitOfWork.Setup(x => x.ScheduleChangeRequests.FirstOrDefaultAsync(
                It.IsAny<IFilteringSpecification<ScheduleChangeRequest>>(),
                It.IsAny<IIncludeSpecification<ScheduleChangeRequest>>()))
                .Returns(Task.FromResult(_scheduleChangeRequest));

            var request = new ApproveDeleteCommand() { ApprovedById = new Guid() };

            // Act
            var result = await _approveDeleteCommandHandler.Handle(request, CancellationToken.None);
            
            // Assert
            Assert.False(result.Succeeded);                     
        }

        [Fact]
        public async Task ApproveDeleteEventTest_ForEventSet_ReturnsSuccessfulResult()
        {
            // Arrange            
            Expression<Action<IUnitOfWork>> eventRemoveSingature = x => x.Events.Remove(It.IsAny<Event>());
            Expression<Action<IUnitOfWork>> eventSetRemoveSignature = x => x.EventSets.Remove(It.IsAny<EventSet>());

            _scheduleChangeRequest.IsAppliedForEventSet = true;

            _mockUnitOfWork.Setup(x => x.ScheduleChangeRequests.FirstOrDefaultAsync(
                It.IsAny<IFilteringSpecification<ScheduleChangeRequest>>(),
                It.IsAny<IIncludeSpecification<ScheduleChangeRequest>>()))
                .Returns(Task.FromResult(_scheduleChangeRequest));

            _mockUnitOfWork.Setup(eventRemoveSingature);
            _mockUnitOfWork.Setup(eventSetRemoveSignature);

            var request = new ApproveDeleteCommand() { ApprovedById = new Guid() };

            // Act
            var result = await _approveDeleteCommandHandler.Handle(request, CancellationToken.None);
            
            // Assert
            Assert.True(result.Succeeded);
            _mockUnitOfWork.Verify(eventSetRemoveSignature, Times.Once);
            _mockUnitOfWork.Verify(eventRemoveSingature, Times.Never);
        }

        [Fact]
        public async Task ApproveDeleteEventTest_ForEvent_ReturnsSuccessfulResult()
        {
            // Arrange
            Expression<Action<IUnitOfWork>> eventRemoveSingature = x => x.Events.Remove(It.IsAny<Event>());
            Expression<Action<IUnitOfWork>> eventSetRemoveSignature = x => x.EventSets.Remove(It.IsAny<EventSet>());

            _mockUnitOfWork.Setup(x => x.ScheduleChangeRequests.FirstOrDefaultAsync(
                It.IsAny<IFilteringSpecification<ScheduleChangeRequest>>(),
                It.IsAny<IIncludeSpecification<ScheduleChangeRequest>>()))
                .Returns(Task.FromResult(_scheduleChangeRequest));

            _mockUnitOfWork.Setup(eventRemoveSingature);
            _mockUnitOfWork.Setup(eventSetRemoveSignature);

            var request = new ApproveDeleteCommand() { ApprovedById = new Guid() };
            
            // Act
            var result = await _approveDeleteCommandHandler.Handle(request, CancellationToken.None);
            
            // Assert
            Assert.True(result.Succeeded);      
            _mockUnitOfWork.Verify(eventSetRemoveSignature, Times.Never);
            _mockUnitOfWork.Verify(eventRemoveSingature, Times.Once);
        }
    }
}
