﻿using AuRooM.Application.Features.Events.Commands.RequestChange;
using AuRooM.Common.Extensions;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using Castle.DynamicProxy.Generators;
using Microsoft.AspNetCore.Mvc.Internal;
using Microsoft.VisualBasic;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AuRooM.Test.Tests.Events
{
    public class RequestChangeEventTest
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;
        private readonly Mock<IMapper> _mockMapper;
        private readonly RequestChangeCommand request;
        private readonly RequestChangeCommandHandler _requestChangeCommandHandler;        
        public RequestChangeEventTest()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();

            _mockMapper = new Mock<IMapper>();

            request = new RequestChangeCommand()
            { 
                Period = new TimePeriod() 
                { 
                    Start = new DateTime(2020, 12, 30),
                    Finish = new DateTime(2021, 12, 30)
                },
                Name = String.Empty,
                EventType = 0,
                Frequency = 1,
                AuditoryId = Guid.Empty,
                RequestedById = Guid.Empty
            };

            _requestChangeCommandHandler = new RequestChangeCommandHandler(_mockUnitOfWork.Object, _mockMapper.Object);
        }

        [Fact]
        public async Task RequestChangeEventTest_PeriodIsNotValid_ReturnsFailure()
        {
            // Arrange            
            request.Period.Start = new DateTime(
                year: 2006,
                month: 01,
                day: 01);

            request.Period.Finish = new DateTime(
                year: 2005,
                month: 01,
                day: 01);

            // Act
            var result = await _requestChangeCommandHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.False(result.Succeeded);
        }

        [Fact]
        public async Task RequestChangeEventTest_PeriodFinishIsNotFutureDate_ReturnsFailure()
        {
            // Arrange
            request.Period.Start = new DateTime(
                year: 2005,
                month: 12,
                day: 01
                );
            request.Period.Finish = new DateTime(
                year: 2006,
                month: 12,
                day: 01);

            // Act
            var result = await _requestChangeCommandHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.False(result.Succeeded);
        }

        [Fact]
        public async Task RequestChangeEventTest_GroupAndNarratorIdsAreNull_ReturnsSuccessfulResult()
        {
            // Arrange
            _mockMapper.Setup(x => x.Map<ScheduleChangeRequest>(It.IsAny<RequestChangeCommand>()))
                .Returns<RequestChangeCommand>(x => new ScheduleChangeRequest()
                { 
                    AuditoryId = x.AuditoryId,
                    EventId = x.EventId,
                    Frequency = x.Frequency,
                    Name = x.Name,
                    Period = x.Period,
                    Timing = x.Timing
                });

            _mockUnitOfWork.Setup(x => x.GroupScheduleChangeRequests.AddRange(It.IsAny<IEnumerable<GroupScheduleChangeRequest>>()));
            _mockUnitOfWork.Setup(x => x.NarratorScheduleChangeRequests.AddRange(It.IsAny<IEnumerable<NarratorScheduleChangeRequest>>()));

            _mockUnitOfWork.Setup(x => x.ScheduleChangeRequests.Add(It.IsAny<ScheduleChangeRequest>()));
            
            // Act
            var result = await _requestChangeCommandHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Succeeded);
        }

        [Fact]
        public async Task RequestChangeEventTest_GroupAndNarratorIdsAreNotNull_ReturnsSuccessfulResult()
        {
            // Arrange

            _mockMapper.Setup(x => x.Map<ScheduleChangeRequest>(It.IsAny<RequestChangeCommand>()))
                .Returns<RequestChangeCommand>(x => new ScheduleChangeRequest()
                {
                    AuditoryId = x.AuditoryId,
                    EventId = x.EventId,
                    Frequency = x.Frequency,
                    Name = x.Name,
                    Period = x.Period,
                    Timing = x.Timing
                });

            request.GroupIds = new List<Guid?>() { Guid.Empty };
            request.TeacherIds = new List<Guid?>() { Guid.Empty };

            _mockUnitOfWork.Setup(x => x.GroupScheduleChangeRequests.AddRange(It.IsAny<IEnumerable<GroupScheduleChangeRequest>>()));
            _mockUnitOfWork.Setup(x => x.NarratorScheduleChangeRequests.AddRange(It.IsAny<IEnumerable<NarratorScheduleChangeRequest>>()));

            _mockUnitOfWork.Setup(x => x.ScheduleChangeRequests.Add(It.IsAny<ScheduleChangeRequest>()));
            // Act

            var result = await _requestChangeCommandHandler.Handle(request, CancellationToken.None);

            // Assert

            Assert.True(result.Succeeded);
            _mockUnitOfWork.Verify(x => x.GroupScheduleChangeRequests.AddRange(It.IsAny<IEnumerable<GroupScheduleChangeRequest>>()), Times.Once);
            _mockUnitOfWork.Verify(x => x.NarratorScheduleChangeRequests.AddRange(It.IsAny<IEnumerable<NarratorScheduleChangeRequest>>()), Times.Once);
        }
    }
}
