﻿using AuRooM.Application.Common.Models;
using AuRooM.Application.Features.Events.Commands.Approve;
using AuRooM.Application.Features.Events.Commands.ApproveRequestChange;
using AuRooM.Application.Services.DTOs;
using AuRooM.Application.Services.Interfaces;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using Castle.DynamicProxy.Generators;
using Microsoft.EntityFrameworkCore.Query;
using Moq;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AuRooM.Test.Tests.Events
{
    public class ApproveRequestChangeEventTest
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;
        private readonly Mock<IMapper> _mockMapper;
        private readonly Mock<IEventService> _mockEventService;
        private readonly ApproveRequestChangeCommandHandler _approveRequestChangeCommandHandler;
        private readonly ScheduleChangeRequest _scheduleChangeRequest;
        private readonly List<Auditory> _auditories;
        private readonly List<User> _persons;
        private readonly List<Event> _events;
        
        public ApproveRequestChangeEventTest()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();

            _mockMapper = new Mock<IMapper>();

            _mockEventService = new Mock<IEventService>();

            _approveRequestChangeCommandHandler = new ApproveRequestChangeCommandHandler(_mockUnitOfWork.Object, _mockMapper.Object, _mockEventService.Object);

            _persons = new List<User>()
            {
                new User()
                {
                    Id = Guid.Parse("DA48370F-6443-4334-938F-51D8A179C330"),
                    FirstName = "Anatoliy",
                    LastName = "Muzychuk",
                    UserStatus = UserStatus.Active,
                    DocumentInfo = "111AM",
                },
                new User()
                {
                    Id = Guid.Parse("3358C87F-56E9-420F-B6FF-3A9A3675A2AA"),
                    FirstName = "Sviatoslav",
                    LastName = "Tarasiuk",
                    UserStatus = UserStatus.Active,
                    DocumentInfo = "111ST"
                },
                new User()
                {
                    Id = Guid.Parse("A78C1993-9159-491A-A9B1-21018EFF25B6"),
                    FirstName = "Admin",
                    LastName = "Admin",
                    UserStatus = UserStatus.Active,
                    DocumentInfo = "Admin"
                }
            };

            _auditories = new List<Auditory>()
            {
                new Auditory()
                {
                    Id = Guid.Parse("E02FA2D2-1CFD-4B95-BA27-6718684F2509"),
                    Number = 111
                },
                new Auditory()
                {
                    Id = Guid.Parse("1299ED9A-95BE-4020-B2A6-235EB64F4973"),
                    Number = 439
                },
                new Auditory()
                {
                    Id = Guid.Parse("A1AA8785-A1B6-4E59-B64A-C940E1578690"),
                    Number = 117
                }
            };

            var eventSet = new EventSet()
            {
                Period = new TimePeriod()
                {
                    Start = new DateTime(2021, 3, 1),
                    Finish = new DateTime(2021, 3, 22)
                },
                Frequency = 1
            };

            _events = new List<Event>()
            {
                new Event()
                {
                    Id = Guid.Parse("532807AD-72FE-4F77-B0D7-4F79DA592A71"),
                    Name = "Programing",
                    Timing = new TimePeriod()
                    {
                        Start = new DateTime(2021, 3, 1, 10, 10, 0),
                        Finish = new DateTime(2021, 3, 1, 11, 30, 0)
                    },                   
                    Type = EventType.Lecture,
                    PersonInChargeId = _persons[0]
                        .Id,
                    PersonInCharge = _persons[0],
                    AuditoryId = _auditories[0]
                        .Id,
                    Auditory = _auditories[0],
                    EventSet = eventSet
                },
                new Event()
                {
                    Id = Guid.Parse("17340A5F-2BFD-4725-BE6F-09DF6C69964F"),
                    Name = "Math",
                    Timing = new TimePeriod()
                    {
                        Start = new DateTime(2021, 3, 8, 11, 50, 0),
                        Finish = new DateTime(2021, 3, 8, 13, 10, 0)
                    },          
                    Type = EventType.Lecture,
                    PersonInChargeId = _persons[1]
                        .Id,
                    PersonInCharge = _persons[1],
                    AuditoryId = _auditories[1]
                        .Id,
                    Auditory = _auditories[1],
                    EventSet = eventSet
                },
                new Event()
                {
                    Id = Guid.Parse("D2992D94-877B-4152-B437-BE3C5DE1F9EF"),
                    Name = "Programing",
                    Timing = new TimePeriod()
                    {
                        Start = new DateTime(2021, 3, 16, 10, 10, 0),
                        Finish = new DateTime(2021, 3, 16, 11, 30, 0)
                    },
                    Type = EventType.Class,
                    PersonInChargeId = _persons[0]
                        .Id,
                    PersonInCharge = _persons[0],
                    AuditoryId = _auditories[2]
                        .Id,
                    Auditory = _auditories[2],
                    EventSet = eventSet
                }
            };

            _scheduleChangeRequest = new ScheduleChangeRequest()
            {
                Id = Guid.Parse("7BF14A8F-4009-46D1-AB2E-BAC117B6459B"),
                Name = _events[0]
                        .Name,
                Timing = _events[0]
                        .Timing,
                Period = new TimePeriod()
                {
                    Start = new DateTime(2021, 03, 01),
                    Finish = new DateTime(2021, 03, 16)
                },
                Frequency = 1,
                RequestDate = new DateTime(
                        2020,
                        3,
                        4,
                        12,
                        0,
                        0),
                RequestStatus = ChangeRequestStatus.Approved,
                EventType = _events[0]
                        .Type,
                AuditoryId = _events[0]
                        .AuditoryId,
                Auditory = _events[0]
                        .Auditory,
                EventId = _events[0]
                        .Id,
                Event = _events[0],
                RequestedById = _persons[0]
                        .Id,
                RequestedBy = _persons[0],
                StatusChangedById = _persons[2]
                        .Id,
                StatusChangedBy = _persons[2],
                IsAppliedForEventSet = true,
            };
        }

        [Fact]
        public async Task ApproveRequestChangeEventTest_ChangeIsAppliedForEventSet_ReturnsFailure()
        {
            // Arrange
            _mockUnitOfWork.Setup(x => x.ScheduleChangeRequests.FirstOrDefaultAsync(
               It.IsAny<IFilteringSpecification<ScheduleChangeRequest>>(),
               It.IsAny<IIncludeSpecification<ScheduleChangeRequest>>()))
               .Returns(Task.FromResult(_scheduleChangeRequest));

            _mockUnitOfWork.Setup(x => x.Events.FirstOrDefaultAsync(
                It.IsAny<IFilteringSpecification<Event>>(),
                It.IsAny<IIncludeSpecification<Event>>()))
                .Returns(Task.FromResult(_scheduleChangeRequest.Event));

            _mockUnitOfWork.Setup(x => x.Events.GetAsync(
                It.IsAny<IFilteringSpecification<Event>>(),
                It.IsAny<IIncludeSpecification<Event>>(),
                It.IsAny<IPagingSpecification>(),
                It.IsAny<IOrderingSpecification<Event>>()))
                .Returns(Task.FromResult((IReadOnlyList<Event>)new List<Event>() { _scheduleChangeRequest.Event }));

            _mockMapper.Setup(x => x.Map<ScheduleChangeRequest, EventSet>(It.IsAny<ScheduleChangeRequest>(), It.IsAny<EventSet>()));

            _mockUnitOfWork.Setup(x => x.Events.RemoveRange(It.IsAny<IReadOnlyList<Event>>()));

            _mockMapper.Setup(x => x.Map<CreateEventInfoDto>(It.IsAny<ScheduleChangeRequest>()))
                .Returns<ScheduleChangeRequest>(x => new CreateEventInfoDto()
                {
                    Date = x.Period.Start,
                    EventSet = x.Event.EventSet
                });

            _mockUnitOfWork.Setup(x => x.Events.FirstOrDefaultAsync(
               It.IsAny<IFilteringSpecification<Event>>(),
               It.IsAny<IIncludeSpecification<Event>>()))
               .Returns(Task.FromResult(_scheduleChangeRequest.Event));

            _mockEventService.Setup(x => x.CreateEvent(It.IsAny<CreateEventInfoDto>()))
                .Returns(Task.FromResult(Result.Failure(String.Empty)));            

            var request = new ApproveRequestChangeCommand() { ApprovedById = new Guid() };

            // Act
            var result = await _approveRequestChangeCommandHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.False(result.Succeeded);
        }

        [Fact]
        public async Task ApproveRequestChangeEventTest_UpdateEventIsFailure_ReturnsFailure()
        {
            // Arrange
            _mockUnitOfWork.Setup(x => x.ScheduleChangeRequests.FirstOrDefaultAsync(
              It.IsAny<IFilteringSpecification<ScheduleChangeRequest>>(),
              It.IsAny<IIncludeSpecification<ScheduleChangeRequest>>()))
              .Returns(Task.FromResult(_scheduleChangeRequest));

            _mockUnitOfWork.Setup(x => x.Events.FirstOrDefaultAsync(
                It.IsAny<IFilteringSpecification<Event>>(),
                It.IsAny<IIncludeSpecification<Event>>()))
                .Returns(Task.FromResult(_scheduleChangeRequest.Event));

            _mockUnitOfWork.Setup(x => x.Events.GetAsync(
                It.IsAny<IFilteringSpecification<Event>>(),
                It.IsAny<IIncludeSpecification<Event>>(),
                It.IsAny<IPagingSpecification>(),
                It.IsAny<IOrderingSpecification<Event>>()))
                .Returns(Task.FromResult((IReadOnlyList<Event>)new List<Event>() { _scheduleChangeRequest.Event }));

            _mockMapper.Setup(x => x.Map<ScheduleChangeRequest, EventSet>(It.IsAny<ScheduleChangeRequest>(), It.IsAny<EventSet>()));

            _mockUnitOfWork.Setup(x => x.Events.RemoveRange(It.IsAny<IReadOnlyList<Event>>()));

            _mockMapper.Setup(x => x.Map<CreateEventInfoDto>(It.IsAny<ScheduleChangeRequest>()))
                .Returns<ScheduleChangeRequest>(x => new CreateEventInfoDto()
                {
                    Date = x.Period.Start,
                    EventSet = x.Event.EventSet
                });

            _mockUnitOfWork.Setup(x => x.Events.FirstOrDefaultAsync(
               It.IsAny<IFilteringSpecification<Event>>(),
               It.IsAny<IIncludeSpecification<Event>>()))
               .Returns(Task.FromResult(_scheduleChangeRequest.Event));

            _mockEventService.Setup(x => x.CreateEvent(It.IsAny<CreateEventInfoDto>()))
                .Returns(Task.FromResult(Result.Success()));

            _mockEventService.Setup(x => x.UpdateEvent(It.IsAny<Event>(), It.IsAny<ScheduleChangeRequest>()))
                .Returns(Task.FromResult(Result.Failure(String.Empty)));

            var request = new ApproveRequestChangeCommand() { ApprovedById = Guid.Empty };

            // Act
            var result = await _approveRequestChangeCommandHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.False(result.Succeeded);
        }

        [Fact]
        public async Task ApproveRequestChangeEventTest_ChangeIsAppliedForEventSet_ReturnsSuccessfulResult()
        {
            // Arrange
            _mockUnitOfWork.Setup(x => x.ScheduleChangeRequests.FirstOrDefaultAsync(
               It.IsAny<IFilteringSpecification<ScheduleChangeRequest>>(),
               It.IsAny<IIncludeSpecification<ScheduleChangeRequest>>()))
               .Returns(Task.FromResult(_scheduleChangeRequest));

            _mockUnitOfWork.Setup(x => x.Events.FirstOrDefaultAsync(
                It.IsAny<IFilteringSpecification<Event>>(),
                It.IsAny<IIncludeSpecification<Event>>()))
                .Returns(Task.FromResult(_scheduleChangeRequest.Event));

            _mockUnitOfWork.Setup(x => x.Events.GetAsync(
                It.IsAny<IFilteringSpecification<Event>>(),
                It.IsAny<IIncludeSpecification<Event>>(),
                It.IsAny<IPagingSpecification>(),
                It.IsAny<IOrderingSpecification<Event>>()))
                .Returns(Task.FromResult((IReadOnlyList<Event>)new List<Event>() { _scheduleChangeRequest.Event }));

            _mockMapper.Setup(x => x.Map<ScheduleChangeRequest, EventSet>(It.IsAny<ScheduleChangeRequest>(), It.IsAny<EventSet>()));

            _mockUnitOfWork.Setup(x => x.Events.RemoveRange(It.IsAny<IReadOnlyList<Event>>()));

            _mockMapper.Setup(x => x.Map<CreateEventInfoDto>(It.IsAny<ScheduleChangeRequest>()))
                .Returns<ScheduleChangeRequest>(x => new CreateEventInfoDto()
                {
                    Date = x.Period.Start,
                    EventSet = x.Event.EventSet
                });

            _mockUnitOfWork.Setup(x => x.Events.FirstOrDefaultAsync(
               It.IsAny<IFilteringSpecification<Event>>(),
               It.IsAny<IIncludeSpecification<Event>>()))
               .Returns(Task.FromResult(_scheduleChangeRequest.Event));

            _mockEventService.Setup(x => x.CreateEvent(It.IsAny<CreateEventInfoDto>()))
                .Returns(Task.FromResult(Result.Success()));     

            _mockEventService.Setup(x => x.UpdateEvent(It.IsAny<Event>(), It.IsAny<ScheduleChangeRequest>()))
                .Returns(Task.FromResult(Result.Success()));

            _mockUnitOfWork.Setup(x => x.CommitAsync());

            var request = new ApproveRequestChangeCommand() { ApprovedById = new Guid() };

            // Act
            var result = await _approveRequestChangeCommandHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Succeeded);
        }

        [Fact]
        public async Task ApproveRequestChangeEventTest_ChangeIsNotAppliedForEventSet_ReturnsSuccessfulResult()
        {
            // Arrange
            _events[0].EventSet = null;

            _scheduleChangeRequest.IsAppliedForEventSet = false;

            _mockUnitOfWork.Setup(x => x.ScheduleChangeRequests.FirstOrDefaultAsync(
                It.IsAny<IFilteringSpecification<ScheduleChangeRequest>>(),
                It.IsAny<IIncludeSpecification<ScheduleChangeRequest>>()))
                .Returns(Task.FromResult(_scheduleChangeRequest));

            _mockUnitOfWork.Setup(x => x.Events.FirstOrDefaultAsync(
                It.IsAny<IFilteringSpecification<Event>>(),
                It.IsAny<IIncludeSpecification<Event>>()))
                .Returns(Task.FromResult(_scheduleChangeRequest.Event));

            _mockUnitOfWork.Setup(x => x.Events.GetAsync(
                It.IsAny<IFilteringSpecification<Event>>(),
                It.IsAny<IIncludeSpecification<Event>>(),
                It.IsAny<IPagingSpecification>(),
                It.IsAny<IOrderingSpecification<Event>>()))
                .Returns(Task.FromResult((IReadOnlyList<Event>)new List<Event>() { _scheduleChangeRequest.Event }));

            _mockEventService.Setup(x => x.UpdateEvent(It.IsAny<Event>(), It.IsAny<ScheduleChangeRequest>()))
                .Returns(Task.FromResult(Result.Success()));

            _mockUnitOfWork.Setup(x => x.CommitAsync());

            _mockUnitOfWork.Setup(x => x.Events.RemoveRange(It.IsAny<IReadOnlyList<Event>>()));

            _mockMapper.Setup(x => x.Map<CreateEventInfoDto>(It.IsAny<ScheduleChangeRequest>()))
                .Returns<ScheduleChangeRequest>(x => new CreateEventInfoDto()
                {
                    Date = x.Period.Start,
                    EventSet = x.Event.EventSet
                });

            _mockEventService.Setup(x => x.CreateEvent(It.IsAny<CreateEventInfoDto>()))
                .Returns(Task.FromResult(Result.Success()));

            var request = new ApproveRequestChangeCommand() { ApprovedById = new Guid() };
            
            // Act
            var result = await _approveRequestChangeCommandHandler.Handle(request, CancellationToken.None);
            
            // Assert
            Assert.True(result.Succeeded);
            _mockEventService.Verify(x => x.UpdateEvent(It.IsAny<Event>(), It.IsAny<ScheduleChangeRequest>()), Times.Once());
        }
    }
}
