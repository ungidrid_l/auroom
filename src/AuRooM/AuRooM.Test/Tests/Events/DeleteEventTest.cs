﻿using AuRooM.Application.Features.Events.Commands.Delete;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AuRooM.Test.Tests.Events
{
    public class DeleteEventTest
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;
        private readonly Mock<IMapper> _mockMapper;
        private readonly DeleteEventCommandHandler _deleteEventCommandHandler;
        private readonly Event @event;
        private readonly Event @nullEvent = null;

        public DeleteEventTest()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();

            _mockMapper = new Mock<IMapper>();

            _deleteEventCommandHandler = new DeleteEventCommandHandler(_mockUnitOfWork.Object, _mockMapper.Object);

            @event = new Event()
            {
                Id = Guid.Parse("D2992D94-877B-4152-B437-BE3C5DE1F9EF"),
                Name = "Programing",
                Timing = new TimePeriod()
                {
                    Start = new DateTime(
                            2020,
                            3,
                            3,
                            10,
                            10,
                            0),
                    Finish = new DateTime(
                            2021,
                            3,
                            3,
                            11,
                            30,
                            0)
                },                
                Type = EventType.Class,                
            };
        }

        [Fact]
        public async Task DeleteEventTest_EventIsNull_ReturnsSuccessfulResult()
        {
            // Arrange
            _mockUnitOfWork.Setup(x => x.Events.FirstOrDefaultAsync(
                It.IsAny<IFilteringSpecification<Event>>(),
                It.IsAny<IIncludeSpecification<Event>>()))
                .Returns(Task.FromResult(@nullEvent));

            var request = new DeleteEventCommand();

            // Act
            var result = await _deleteEventCommandHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.False(result.Succeeded);
        }

        [Fact]
        public async Task DeleteEventTest_ReturnsSuccessfulResult()
        {
            // Arrange
            _mockUnitOfWork.Setup(x => x.Events.FirstOrDefaultAsync(
                It.IsAny<IFilteringSpecification<Event>>(),
                It.IsAny<IIncludeSpecification<Event>>()))
                .Returns(Task.FromResult(@event));

            _mockMapper.Setup(x => x.Map<ScheduleChangeRequest>(It.IsAny<Event>()))
                .Returns<Event>(
                x => new ScheduleChangeRequest()
                { 
                    Id = x.Id,
                    Name = x.Name,
                    Period = x.Timing,                    
                    EventType = x.Type,
                    RequestedById = x.PersonInChargeId,
                    Auditory = x.Auditory
                });

            _mockMapper.Setup(x => x.Map<DeleteEventCommand, ScheduleChangeRequest>(
                It.IsAny<DeleteEventCommand>(), 
                It.IsAny<ScheduleChangeRequest>()));

            _mockUnitOfWork.Setup(x => x.ScheduleChangeRequests.Add(It.IsAny<ScheduleChangeRequest>()));
            var request = new DeleteEventCommand();

            // Act
            var result = await _deleteEventCommandHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Succeeded);            
        }
    }
}
