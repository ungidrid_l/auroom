﻿using AuRooM.Application.Features.Events.Queries.GetChangeRequestsList;
using AuRooM.Application.Services.Interfaces;
using AuRooM.Common.DTOs;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AuRooM.Test.Tests.Events
{
    public class GetChangeRequestsListTest
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;
        private readonly Mock<IMapper> _mockMapper;
        private readonly Mock<IUserService> _mockUserService;
        private readonly List<ScheduleChangeRequest> _scheduleChangeRequests;
        private readonly List<Event> _events;
        private readonly List<Auditory> _auditories;
        private readonly List<User> _persons;
        private readonly GetChangeRequestsListQueryHandler _getChangeRequestsListQueryHandler;

        public GetChangeRequestsListTest()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();

            _mockMapper = new Mock<IMapper>();
            _mockMapper.Setup(x => x.Map<SelectListItem>(It.IsAny<SelectListItemDto>()))
                .Returns<SelectListItemDto>(
                    s => new SelectListItem(s.Text, s.Value));

            _mockUserService = new Mock<IUserService>();

            _getChangeRequestsListQueryHandler = new GetChangeRequestsListQueryHandler(_mockUnitOfWork.Object,  _mockMapper.Object, _mockUserService.Object);

            _auditories = new List<Auditory>()
            {
                new Auditory()
                {
                    Id = Guid.Parse("E02FA2D2-1CFD-4B95-BA27-6718684F2509"),
                    Number = 111
                },
                new Auditory()
                {
                    Id = Guid.Parse("1299ED9A-95BE-4020-B2A6-235EB64F4973"),
                    Number = 439
                },
                new Auditory()
                {
                    Id = Guid.Parse("A1AA8785-A1B6-4E59-B64A-C940E1578690"),
                    Number = 117
                }
            };

            _persons = new List<User>()
            {
                new User()
                {
                    Id = Guid.Parse("DA48370F-6443-4334-938F-51D8A179C330"),
                    FirstName = "Anatoliy",
                    LastName = "Muzychuk",
                    UserStatus = UserStatus.Active,
                    DocumentInfo = "111AM",
                },
                new User()
                {
                    Id = Guid.Parse("3358C87F-56E9-420F-B6FF-3A9A3675A2AA"),
                    FirstName = "Sviatoslav",
                    LastName = "Tarasiuk",
                    UserStatus = UserStatus.Active,
                    DocumentInfo = "111ST"
                },
                new User()
                {
                    Id = Guid.Parse("A78C1993-9159-491A-A9B1-21018EFF25B6"),
                    FirstName = "Admin",
                    LastName = "Admin",
                    UserStatus = UserStatus.Active,
                    DocumentInfo = "Admin"
                }
            };

            _events = new List<Event>()
            {
                new Event()
                {
                    Id = Guid.Parse("532807AD-72FE-4F77-B0D7-4F79DA592A71"),
                    Name = "Programing",
                    Timing = new TimePeriod()
                    {
                        Start = new DateTime(
                            2020,
                            3,
                            1,
                            10,
                            10,
                            0),
                        Finish = new DateTime(
                            2020,
                            3,
                            1,
                            11,
                            30,
                            0)
                    },                    
                    Type = EventType.Lecture,
                    PersonInChargeId = _persons[0]
                        .Id,
                    PersonInCharge = _persons[0],
                    AuditoryId = _auditories[0]
                        .Id,
                    Auditory = _auditories[0]
                },
                new Event()
                {
                    Id = Guid.Parse("17340A5F-2BFD-4725-BE6F-09DF6C69964F"),
                    Name = "Math",
                    Timing = new TimePeriod()
                    {
                        Start = new DateTime(
                            2020,
                            3,
                            2,
                            10,
                            10,
                            0),
                        Finish = new DateTime(
                            2021,
                            3,
                            2,
                            11,
                            30,
                            0)
                    },                    
                    Type = EventType.Lecture,
                    PersonInChargeId = _persons[1]
                        .Id,
                    PersonInCharge = _persons[1],
                    AuditoryId = _auditories[1]
                        .Id,
                    Auditory = _auditories[1]
                },
                new Event()
                {
                    Id = Guid.Parse("D2992D94-877B-4152-B437-BE3C5DE1F9EF"),
                    Name = "Programing",
                    Timing = new TimePeriod()
                    {
                        Start = new DateTime(
                            2020,
                            3,
                            3,
                            10,
                            10,
                            0),
                        Finish = new DateTime(
                            2021,
                            3,
                            3,
                            11,
                            30,
                            0)
                    },                   
                    Type = EventType.Class,
                    PersonInChargeId = _persons[0]
                        .Id,
                    PersonInCharge = _persons[0],
                    AuditoryId = _auditories[2]
                        .Id,
                    Auditory = _auditories[2]
                }
            };

            _scheduleChangeRequests = new List<ScheduleChangeRequest>()
            {
                new ScheduleChangeRequest()
                {
                    Id = Guid.Parse("7BF14A8F-4009-46D1-AB2E-BAC117B6459B"),
                    Name = _events[0]
                        .Name,
                    Timing = _events[0]
                        .Timing,
                    Period = new TimePeriod()
                    {
                        Start = new DateTime(
                            _events[0]
                                .Timing.Start.Year,
                            _events[0]
                                .Timing.Start.Month,
                            _events[0]
                                .Timing.Start.Day),
                        Finish = new DateTime(
                            _events[0]
                                .Timing.Finish.Year,
                            _events[0]
                                .Timing.Finish.Month,
                            _events[0]
                                .Timing.Start.Day)
                    },
                    Frequency = 1,
                    RequestDate = new DateTime(
                        2020,
                        3,
                        4,
                        12,
                        0,
                        0),
                    RequestStatus = ChangeRequestStatus.Approved,
                    EventType = _events[0]
                        .Type,
                    AuditoryId = _events[0]
                        .AuditoryId,
                    Auditory = _events[0]
                        .Auditory,
                    EventId = _events[0]
                        .Id,
                    Event = _events[0],
                    RequestedById = _persons[0]
                        .Id,
                    RequestedBy = _persons[0],
                    StatusChangedById = _persons[2]
                        .Id,
                    StatusChangedBy = _persons[2]
                },
                new ScheduleChangeRequest()
                {
                    Id = Guid.Parse("7C796E19-2B46-403B-92EC-FEF0C7BF9D42"),
                    Name = _events[1]
                        .Name,
                    Timing = _events[1]
                        .Timing,
                    Period = new TimePeriod()
                    {
                        Start = new DateTime(
                            _events[1]
                                .Timing.Start.Year,
                            _events[1]
                                .Timing.Start.Month,
                            _events[1]
                                .Timing.Start.Day),
                        Finish = new DateTime(
                            _events[1]
                                .Timing.Finish.Year,
                            _events[1]
                                .Timing.Finish.Month,
                            _events[1]
                                .Timing.Start.Day)
                    },
                    Frequency = 1,
                    RequestDate = new DateTime(
                        2020,
                        3,
                        4,
                        12,
                        0,
                        0),
                    RequestStatus = ChangeRequestStatus.Approved,
                    EventType = _events[1]
                        .Type,
                    AuditoryId = _events[1]
                        .AuditoryId,
                    Auditory = _events[1]
                        .Auditory,
                    EventId = _events[1]
                        .Id,
                    Event = _events[1],
                    RequestedById = _persons[1]
                        .Id,
                    RequestedBy = _persons[1],
                    StatusChangedById = _persons[2]
                        .Id,
                    StatusChangedBy = _persons[2]
                },
                new ScheduleChangeRequest()
                {
                    Id = Guid.Parse("FA0C5842-FF89-48BF-B327-CAB83069ABC0"),
                    Name = _events[2]
                        .Name,
                    Timing = _events[2]
                        .Timing,
                    Period = new TimePeriod()
                    {
                        Start = new DateTime(
                            _events[2]
                                .Timing.Start.Year,
                            _events[2]
                                .Timing.Start.Month,
                            _events[2]
                                .Timing.Start.Day),
                        Finish = new DateTime(
                            _events[2]
                                .Timing.Finish.Year,
                            _events[2]
                                .Timing.Finish.Month,
                            _events[2]
                                .Timing.Start.Day)
                    },
                    Frequency = 1,
                    RequestDate = new DateTime(
                        2020,
                        3,
                        4,
                        12,
                        0,
                        0),
                    RequestStatus = ChangeRequestStatus.Approved,
                    EventType = _events[2]
                        .Type,
                    AuditoryId = _events[2]
                        .AuditoryId,
                    Auditory = _events[2]
                        .Auditory,
                    EventId = _events[2]
                        .Id,
                    Event = _events[2],
                    RequestedById = _persons[0]
                        .Id,
                    RequestedBy = _persons[0],
                    StatusChangedById = _persons[2]
                        .Id,
                    StatusChangedBy = _persons[2]
                },
            };
        }

        [Fact]
        public async Task GetChangeRequestsListTest_ReturnsSuccessfulResult()
        {
            //Arrange
            _mockUnitOfWork.Setup(
                    x => x.ScheduleChangeRequests.GetAsync(
                        It.IsAny<IFilteringSpecification<ScheduleChangeRequest>>(),
                        It.IsAny<IIncludeSpecification<ScheduleChangeRequest>>(),
                        It.IsAny<IPagingSpecification>(),
                        It.IsAny<IOrderingSpecification<ScheduleChangeRequest>>()))
                .Returns(Task.FromResult((IReadOnlyList<ScheduleChangeRequest>)_scheduleChangeRequests));

            _mockMapper.Setup(x => x.Map<ScheduleChangeRequestVM>(It.IsAny<ScheduleChangeRequest>()))
                .Returns<ScheduleChangeRequest>(
                    s => new ScheduleChangeRequestVM()
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Timing = s.Timing,
                        Period = s.Period,
                        Frequency = s.Frequency,
                        RequestStatus = s.RequestStatus.ToString(),
                        RequestType = s.RequestType.ToString(),
                        EventType = s.EventType.ToString(),
                        AuditoryNumber = s.Auditory.Number.ToString(),
                        EventId = s.EventId.Value,
                        RequestedByName = s.RequestedBy.GetFullName()
                    });

            _mockUnitOfWork.Setup(x => x.ScheduleChangeRequests.CountAsync(It.IsAny<IFilteringSpecification<ScheduleChangeRequest>>()))
                .Returns(Task.FromResult(_scheduleChangeRequests.Count));

            _mockUnitOfWork.Setup(x => x.Auditories.GetKeyValuePairsAsync(It.IsAny<IFilteringSpecification<Auditory>>()))
                .Returns(Task.FromResult((IReadOnlyList<SelectListItemDto>)(_auditories.Select(
                                                                                       x => new SelectListItemDto
                                                                                       {
                                                                                           Text = x.Number.ToString(),
                                                                                           Value = x.Id.ToString()
                                                                                       })
                                                                                   .ToList())));

            var request = new GetChangeRequestsListQuery()
            {
                SelectedStatuses = new List<int>(),
                SelectedEventTypes = new List<int>(),
                SelectedAuditories = new List<Guid>()
            };

            // Act
            var result = await _getChangeRequestsListQueryHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Succeeded);
            Assert.Equal(3, result.Data.RequestList.Count());

            foreach(var i in result.Data.RequestList)
            {
                var singleRequest = _scheduleChangeRequests.FirstOrDefault(x => x.Id == i.Id);
                Assert.Equal(singleRequest.Name, i.Name);
                Assert.Equal(singleRequest.Frequency, i.Frequency);
                Assert.Equal(singleRequest.RequestStatus.ToString(), i.RequestStatus);
                Assert.Equal(singleRequest.Period, i.Period);
                Assert.Equal(singleRequest.RequestType.ToString(), i.RequestType);
                Assert.Equal(singleRequest.EventType.ToString(), i.EventType);
                Assert.Equal(singleRequest.Auditory.Number.ToString(), i.AuditoryNumber);
                Assert.Equal(singleRequest.EventId.Value, i.EventId);
                Assert.Equal(singleRequest.RequestedBy.GetFullName(), i.RequestedByName);
            }
        }
    }
}