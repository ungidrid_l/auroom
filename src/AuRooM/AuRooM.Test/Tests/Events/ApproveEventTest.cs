﻿using AuRooM.Application.Features.Events.Commands.Approve;
using AuRooM.Application.Services.DTOs;
using AuRooM.Application.Services.Interfaces;
using AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Filter;
using AuRooM.Application.Specifications.ScheduleChangeRequestsSpecifications.Include;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Authentication.ExtendedProtection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AuRooM.Test.Tests.Events
{
    public class ApproveEventTest
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;
        private readonly Mock<IMapper> _mockMapper;
        private readonly Mock<IEventService> _mockEventService;
        private readonly ScheduleChangeRequest _scheduleChangeRequest;
        private readonly ApproveEventCommandHandler _approveEventCommandHandler;

        public ApproveEventTest()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();

            _mockMapper = new Mock<IMapper>();

            _mockEventService = new Mock<IEventService>();

            _approveEventCommandHandler = new ApproveEventCommandHandler(_mockUnitOfWork.Object, _mockMapper.Object, _mockEventService.Object);

            _scheduleChangeRequest = new ScheduleChangeRequest()
            {
                Id = Guid.Parse("E1E25FA0-A2C6-4C10-9C75-D2DBA70A9BBE"),
                Name = "Math",
                Timing = new TimePeriod()
                {
                    Start = new DateTime(2020, 04, 03, 11, 50, 0),
                    Finish = new DateTime(2021, 04, 03, 13, 10, 0)
                },
                Period = new TimePeriod()
                {
                    Start = new DateTime(2020, 04, 03),
                    Finish = new DateTime(2021, 04, 03)
                },
                Frequency = 1,
                RequestDate = new DateTime(2020, 04, 06),
                EventType = EventType.Lecture,
                AuditoryId = Guid.Parse("CD1BED7A-AC38-4423-87CB-F0D540D09E47"),
                Auditory = new Auditory() { Id = Guid.Parse("20B1858D-2134-4CDD-9941-C61BA77738AE"), Number = 117 },
                RequestStatus = ChangeRequestStatus.Pending
            };
        }

        [Fact]
        public async Task ApproveEventTest_ReturnsSuccessfulResult()
        {
            // Arrange
            _mockUnitOfWork.Setup(x => x.ScheduleChangeRequests.FirstOrDefaultAsync(
                It.IsAny<IFilteringSpecification<ScheduleChangeRequest>>(),
                It.IsAny<IIncludeSpecification<ScheduleChangeRequest>>()))
                .Returns(Task.FromResult(_scheduleChangeRequest));

            _mockMapper.Setup(x => x.Map<EventSet>(It.IsAny<ScheduleChangeRequest>()))
                .Returns<ScheduleChangeRequest>(
                s => new EventSet()
                {
                    Id = s.Id,
                    Frequency = s.Frequency,
                    Period = s.Period
                });

            _mockUnitOfWork.Setup(x => x.EventSets.Add(It.IsAny<EventSet>()));

            _mockMapper.Setup(x => x.Map<CreateEventInfoDto>(It.IsAny<ScheduleChangeRequest>()))
                .Returns<ScheduleChangeRequest>(
                s => new CreateEventInfoDto()
                { 
                    Name = s.Name,
                    Auditory = s.Auditory,
                    Date = s.RequestDate,
                    TimeStart = new TimeSpan(s.Timing.Start.Hour, s.Timing.Start.Minute, s.Timing.Start.Second),
                    TimeFinish = new TimeSpan(s.Timing.Finish.Hour, s.Timing.Finish.Minute, s.Timing.Finish.Second)
                });

            _mockEventService.Setup(x => x.CreateEvent(It.IsAny<CreateEventInfoDto>()));
            var request = new ApproveEventCommand() { ApprovedById = new Guid() };

            // Act
            var result = await _approveEventCommandHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Succeeded);
            Assert.Equal(ChangeRequestStatus.Approved, _scheduleChangeRequest.RequestStatus);
            Assert.Equal(request.ApprovedById, _scheduleChangeRequest.StatusChangedById);
        }
    }
}
