﻿using AuRooM.Application.Features.Events.Queries.GetRequestChangeModel;
using AuRooM.Application.Specifications.EventSpecifications.Filter;
using AuRooM.Common.DTOs;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AuRooM.Test.Tests.Events
{
    public class GetRequestChangeModelTest
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;
        private readonly Mock<IMapper> _mockMapper;
        private readonly GetRequestChangeModelQueryHandler _getRequestChangeModelQueryHandler;
        private readonly List<Auditory> _auditories;
        private readonly List<AcademicGroup> _groups;
        private readonly List<User> _teachers;
        private readonly Event _eventForChange;

        public GetRequestChangeModelTest()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();

            _mockMapper = new Mock<IMapper>();
            _mockMapper.Setup(x => x.Map<SelectListItem>(It.IsAny<SelectListItemDto>()))
               .Returns<SelectListItemDto>(
                   s => new SelectListItem(s.Text, s.Value));

            _getRequestChangeModelQueryHandler = new GetRequestChangeModelQueryHandler(_mockUnitOfWork.Object, _mockMapper.Object);

            _groups = new List<AcademicGroup>()
            {
                new AcademicGroup() { Id = Guid.Parse("5B9CB2A5-2B99-42D3-83C3-B78725B380A6"), Title = "PMI-31" },
                new AcademicGroup() { Id = Guid.Parse("0384FC75-48C1-469D-BDD4-7286E811733A"), Title = "PMI-32" },
                new AcademicGroup() { Id = Guid.Parse("7ECE3CC0-8FBF-40C5-99AE-535A81F8CFF0"), Title = "PMI-33" }
            };

            _auditories = new List<Auditory>()
            {
                new Auditory() { Id = Guid.Parse("643A2A48-CFBA-4C8E-B775-EC54A2B78753"), Number = 111},
                new Auditory() { Id = Guid.Parse("6C063723-9A54-47F7-8D9A-E16BF4A70C05"), Number = 117},
                new Auditory() { Id = Guid.Parse("6A2B8706-ED52-4B3A-A67D-52A054D57EAA"), Number = 439}
            };

            _teachers = new List<User>()
            {
                new User()
                {
                    Id = Guid.Parse("DA48370F-6443-4334-938F-51D8A179C330"),
                    FirstName = "Anatoliy",
                    LastName = "Muzychuk",
                    UserStatus = UserStatus.Active,
                    DocumentInfo = "111AM"
                },
                new User()
                {
                    Id = Guid.Parse("4A25F8BF-078C-435E-BF10-A41EA238AB1C"),
                    FirstName = "Denys",
                    LastName = "Doskach",
                    UserStatus = UserStatus.Active,
                    DocumentInfo = "123DD"
                }
            };

            _eventForChange = new Event()
            {
                Id = Guid.Parse("D2992D94-877B-4152-B437-BE3C5DE1F9EF"),
                Name = "Programing",
                Timing = new TimePeriod()
                {
                    Start = new DateTime(2020, 3, 3, 10, 10, 0),
                    Finish = new DateTime(2021, 3, 3, 11, 30, 0)
                },                
                Type = EventType.Class,
                PersonInChargeId = _teachers[0].Id,
                PersonInCharge = _teachers[0],
                AuditoryId = _auditories[2].Id,
                Auditory = _auditories[2],
                EventSet = new EventSet(),                 
            };
        }

        [Fact]
        public async Task GetRequestChangeModelTest_ReturnsSuccessfulResult()
        {
            // Arrange
            _mockMapper.Setup(x => x.Map<RequestChangeVM>(It.IsAny<Event>()))
               .Returns<Event>(
                   s => new RequestChangeVM()
                   {
                       EventId = s.Id,
                       Name = s.Name,
                       EventType = (int)s.Type,
                       Timing = s.Timing,
                       Period = s.EventSet.Period,
                       Frequency = s.EventSet.Frequency,
                       AuditoryId = s.AuditoryId,                    
                   });

            _mockUnitOfWork.Setup(x => x.Auditories.GetKeyValuePairsAsync(It.IsAny<IFilteringSpecification<Auditory>>()))
                .Returns(Task.FromResult((IReadOnlyList<SelectListItemDto>)_auditories.Select(
                    x => new SelectListItemDto()
                    {
                        Text = x.Number.ToString(),
                        Value = x.Id.ToString()
                    }).ToList()));

            _mockUnitOfWork.Setup(x => x.AcademicGroups.GetKeyValuePairsAsync(It.IsAny<IFilteringSpecification<AcademicGroup>>()))
                .Returns(Task.FromResult((IReadOnlyList<SelectListItemDto>)_groups.Select(
                    x => new SelectListItemDto()
                    {
                        Text = x.Title,
                        Value = x.Id.ToString()
                    }).ToList()));

            _mockUnitOfWork.Setup(x => x.Users.GetKeyValuePairsAsync(It.IsAny<IFilteringSpecification<User>>()))
                .Returns(Task.FromResult((IReadOnlyList<SelectListItemDto>)_teachers.Select(
                    x => new SelectListItemDto()
                    { 
                        Text = x.GetFullName(),
                        Value = x.Id.ToString()
                    }).ToList()));

            _mockUnitOfWork.Setup(x => x.Events.FirstOrDefaultAsync(It.IsAny<EventByIdFilter>(), It.IsAny<IIncludeSpecification<Event>>()))
                .Returns(Task.FromResult(_eventForChange));

            var request = new GetRequestChangeModelQuery(new Guid());

            // Act
            var result = await _getRequestChangeModelQueryHandler.Handle(request, CancellationToken.None);

            Assert.True(result.Succeeded);
            Assert.Equal(3, result.Data.Groups.Count());
            Assert.Equal(3, result.Data.Auditories.Count());
            Assert.Equal(2, result.Data.Teachers.Count());
            Assert.Equal(result.Data.Name, _eventForChange.Name);
            Assert.Equal(result.Data.EventType, (int)_eventForChange.Type);
            Assert.Equal(result.Data.Timing, _eventForChange.Timing);
            Assert.Equal(result.Data.Frequency, _eventForChange.EventSet.Frequency);
            Assert.Equal(result.Data.AuditoryId, _eventForChange.AuditoryId);
        }
    }
}
