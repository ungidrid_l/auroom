﻿using AuRooM.Application.Features.Auth.Queries.GetRegistrationModel;
using AuRooM.Application.Features.Events.Queries.GetCreateEventModel;
using AuRooM.Application.Specifications;
using AuRooM.Application.Specifications.UserSpecifications.Filter;
using AuRooM.Common.DTOs;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AuRooM.Test.Tests.Events
{
    public class GetCreateEventModelTest
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;
        private readonly Mock<IMapper> _mockMapper;
        private readonly List<AcademicGroup> _groups;
        private readonly List<Auditory> _auditories;
        private readonly List<User> _teachers;
        private readonly List<User> _narrators;
        private readonly GetCreateEventModelQueryHandler _getCreateEventModelQueryHandler;
        public GetCreateEventModelTest()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();

            _mockMapper = new Mock<IMapper>();
            _mockMapper.Setup(x => x.Map<SelectListItem>(It.IsAny<SelectListItemDto>()))
               .Returns<SelectListItemDto>(
                   s => new SelectListItem(s.Text, s.Value));

            _getCreateEventModelQueryHandler = new GetCreateEventModelQueryHandler(_mockUnitOfWork.Object, _mockMapper.Object);

            _groups = new List<AcademicGroup>()
            {
                new AcademicGroup() { Id = Guid.Parse("5B9CB2A5-2B99-42D3-83C3-B78725B380A6"), Title = "PMI-31" },
                new AcademicGroup() { Id = Guid.Parse("0384FC75-48C1-469D-BDD4-7286E811733A"), Title = "PMI-32" },
                new AcademicGroup() { Id = Guid.Parse("7ECE3CC0-8FBF-40C5-99AE-535A81F8CFF0"), Title = "PMI-33" }
            };

            _auditories = new List<Auditory>()
            {
                new Auditory() { Id = Guid.Parse("643A2A48-CFBA-4C8E-B775-EC54A2B78753"), Number = 111},
                new Auditory() { Id = Guid.Parse("6C063723-9A54-47F7-8D9A-E16BF4A70C05"), Number = 117},
                new Auditory() { Id = Guid.Parse("6A2B8706-ED52-4B3A-A67D-52A054D57EAA"), Number = 439}
            };

            _teachers = new List<User>()
            {
                new User()
                {
                    Id = Guid.Parse("DA48370F-6443-4334-938F-51D8A179C330"),
                    FirstName = "Anatoliy",
                    LastName = "Muzychuk",
                    UserStatus = UserStatus.Active,
                    DocumentInfo = "111AM",
                },
                new User()
                {
                    Id = Guid.Parse("4A25F8BF-078C-435E-BF10-A41EA238AB1C"),
                    FirstName = "Denys",
                    LastName = "Doskach",
                    UserStatus = UserStatus.Active,
                    DocumentInfo = "123DD"
                }
            };

            _narrators = new List<User>()
            {
                new User()
                {
                    Id = Guid.Parse("3358C87F-56E9-420F-B6FF-3A9A3675A2AA"),
                    FirstName = "Sviatoslav",
                    LastName = "Tarasiuk",
                    UserStatus = UserStatus.Active,
                    DocumentInfo = "111ST"
                },
                new User()
                {
                    Id = Guid.Parse("A78C1993-9159-491A-A9B1-21018EFF25B6"),
                    FirstName = "Admin",
                    LastName = "Admin",
                    UserStatus = UserStatus.Active,
                    DocumentInfo = "Admin"
                }
            };
        }

        [Fact]
        public async Task GetRegistrationModelQueryHandler_ReturnsSuccessfulResult()
        {
            // Arrange              
            _mockUnitOfWork.Setup(x => x.AcademicGroups.GetKeyValuePairsAsync(It.IsAny<IFilteringSpecification<AcademicGroup>>()))
                .Returns(Task.FromResult((IReadOnlyList<SelectListItemDto>)_groups.Select(
                    x => new SelectListItemDto()
                    { 
                        Text = x.Title,
                        Value = x.Id.ToString()
                    }).ToList()));

            _mockUnitOfWork.Setup(x => x.Auditories.GetKeyValuePairsAsync(It.IsAny<IFilteringSpecification<Auditory>>()))
                .Returns(Task.FromResult((IReadOnlyList<SelectListItemDto>)_auditories.Select(
                    x => new SelectListItemDto()
                    { 
                        Text = x.Number.ToString(),
                        Value = x.Id.ToString()
                    }).ToList()));

            _mockUnitOfWork.Setup(x => x.Users.GetKeyValuePairsAsync(It.IsAny<ActiveTeacherUsersFilter>()))
                .Returns(Task.FromResult((IReadOnlyList<SelectListItemDto>)_teachers.Select(
                    x => new SelectListItemDto()
                    {
                        Text = x.GetFullName(),
                        Value = x.Id.ToString()
                    }).ToList()));

            _mockUnitOfWork.Setup(x => x.Users.GetKeyValuePairsAsync(It.IsAny<ActiveUsersFilter>()))
                .Returns(Task.FromResult((IReadOnlyList<SelectListItemDto>)_narrators.Select(
                    x => new SelectListItemDto()
                    { 
                        Text = x.GetFullName(),
                        Value = x.Id.ToString()
                    }).ToList()));

            var request = new GetCreateEventModelQuery();

            // Act
            var result = await _getCreateEventModelQueryHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Succeeded);
            Assert.Equal(3, result.Data.Groups.Count());
            Assert.Equal(3, result.Data.Auditories.Count());
            Assert.Equal(2, result.Data.Teachers.Count());
            Assert.Equal(2, result.Data.Narrators.Count());

            foreach (var i in result.Data.Groups)
            {
                var group = _groups.FirstOrDefault(x => x.Id.ToString() == i.Value);
                Assert.Equal(group.Title, i.Text);
            }

            foreach (var i in result.Data.Auditories)
            {
                var auditory = _auditories.FirstOrDefault(x => x.Id.ToString() == i.Value);
                Assert.Equal(auditory.Number.ToString(), i.Text);
            }

            foreach (var i in result.Data.Teachers)
            {
                var teacher = _teachers.FirstOrDefault(x => x.Id.ToString() == i.Value);
                Assert.Equal(teacher.GetFullName(), i.Text);
            }

            foreach (var i in result.Data.Narrators)
            {
                var narrator = _narrators.FirstOrDefault(x => x.Id.ToString() == i.Value);
                Assert.Equal(narrator.GetFullName(), i.Text);
            }
        }
    }
}
