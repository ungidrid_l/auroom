﻿using AuRooM.Application.Features.Auth.Queries.GetRegistrationModel;
using AuRooM.Application.Specifications;
using AuRooM.Application.Specifications.RoleSpecifications.Filter;
using AuRooM.Common.DTOs;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AuRooM.Test.Tests.Auth
{    
    public class GetRegistrationModelTest
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;
        private readonly List<AcademicGroup> _groups;
        private readonly List<Role> _roles;
        private readonly GetRegistrationModelQueryHandler _getRegistrationModelQueryHandler;
        private readonly Mock<IMapper> _mockMapper;
        public GetRegistrationModelTest()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();

            _mockMapper = new Mock<IMapper>();
            _mockMapper.Setup(x => x.Map<SelectListItem>(It.IsAny<SelectListItemDto>()))
                .Returns<SelectListItemDto>(
                    s => new SelectListItem(s.Text, s.Value));

            _getRegistrationModelQueryHandler = new GetRegistrationModelQueryHandler(_mockUnitOfWork.Object, _mockMapper.Object);

            _groups = new List<AcademicGroup>()
            {
                new AcademicGroup() { Id = Guid.Parse("5B9CB2A5-2B99-42D3-83C3-B78725B380A6"), Title = "PMI-31" },
                new AcademicGroup() { Id = Guid.Parse("0384FC75-48C1-469D-BDD4-7286E811733A"), Title = "PMI-32" },
                new AcademicGroup() { Id = Guid.Parse("7ECE3CC0-8FBF-40C5-99AE-535A81F8CFF0"), Title = "PMI-33" }
            };

            _roles = new List<Role>()
            {
                new Role() { Id = Guid.Parse("643A2A48-CFBA-4C8E-B775-EC54A2B78753"), Name = "Teacher" },
                new Role() { Id = Guid.Parse("6C063723-9A54-47F7-8D9A-E16BF4A70C05"), Name = "Prefect" },
                new Role() { Id = Guid.Parse("6A2B8706-ED52-4B3A-A67D-52A054D57EAA"), Name = "Student" }
            };
        }

        [Fact]
        public async Task GetRegistrationModelQueryHandler_ReturnsSuccessfulResult()
        {
            // Arrange                   
            _mockUnitOfWork.Setup(x => x.AcademicGroups.GetKeyValuePairsAsync(It.IsAny<IFilteringSpecification<AcademicGroup>>()))
                .Returns(Task.FromResult((IReadOnlyList<SelectListItemDto>)
                    _groups.Select(
                        x => new SelectListItemDto
                        { 
                            Text = x.Title.ToString(),
                            Value = x.Id.ToString()
                        }).ToList()));
            
            _mockUnitOfWork.Setup(x => x.Roles.GetKeyValuePairsAsync(It.IsAny<IFilteringSpecification<Role>>()))
                .Returns(Task.FromResult((IReadOnlyList<SelectListItemDto>)
                    _roles.Select(
                        x => new SelectListItemDto
                        { 
                            Text = x.Name.ToString(),
                            Value = x.Id.ToString()
                        }).ToList()));            

            var request = new GetRegistrationModelQuery();

            // Act
            var result = await _getRegistrationModelQueryHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Succeeded);
            Assert.Equal(3, result.Data.Groups.Count());            
            Assert.Equal(3, result.Data.Roles.Count());
            
            foreach (var i in result.Data.Groups.ToList())
            {
                var group = _groups.FirstOrDefault(x => x.Id.ToString() == i.Value);
                Assert.Equal(group.Title, i.Text);
            }

            foreach (var i in result.Data.Roles)
            {
                var role = _roles.FirstOrDefault(x => x.Id.ToString() == i.Value);
                Assert.Equal(role.Name.ToString(), i.Text.ToString());
            }
        }
    }
}
