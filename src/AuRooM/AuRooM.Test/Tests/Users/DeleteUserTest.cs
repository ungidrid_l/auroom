﻿using AuRooM.Application.Features.Events.Commands.Delete;
using AuRooM.Application.Features.Users.Commands.Delete;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AuRooM.Domain.Settings;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AuRooM.Test.Tests.Users
{
    public class DeleteUserTest
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;        
        private readonly Mock<IOptions<RoleSettings>> _mockRoleSettings;
        private readonly DeleteUserCommandHandler _deleteUserCommandHandler;
        private readonly User _userToDelete;        
        public DeleteUserTest()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();                      

            _mockRoleSettings = new Mock<IOptions<RoleSettings>>();
            _mockRoleSettings.Setup(x => x.Value)
               .Returns(new RoleSettings() { 
                   AdminRoleId = Guid.Parse("343364C2-6CCE-46EA-A2BF-D2A67C15188A"),
                   StudentRoleId = Guid.Parse("C4986454-65DE-48AB-BE5A-BC6A0361DC00")
               });

            _deleteUserCommandHandler = new DeleteUserCommandHandler(_mockUnitOfWork.Object, _mockRoleSettings.Object);

            _userToDelete = new User()
            {
                Id = Guid.Parse("A78C1993-9159-491A-A9B1-21018EFF25B6"),
                FirstName = "Admin",
                LastName = "Admin",
                UserStatus = UserStatus.Active,
                DocumentInfo = "Admin",
                UserRoles = new List<UserRole>()
                { 
                    new UserRole() { UserId = Guid.Parse("A78C1993-9159-491A-A9B1-21018EFF25B6"), RoleId = Guid.Parse("343364C2-6CCE-46EA-A2BF-D2A67C15188A") }
                }
            };           
        }

        [Fact]
        public async Task DeleteUserTest_UserIsAdmin_ReturnsFailure()
        {
            // Arrange
            _mockUnitOfWork.Setup(x => x.Users.FirstOrDefaultAsync(
                It.IsAny<IFilteringSpecification<User>>(),
                It.IsAny<IIncludeSpecification<User>>()))
                .Returns(Task.FromResult(_userToDelete));

            var request = new DeleteUserCommand();
            
            // Act
            var result = await _deleteUserCommandHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.False(result.Succeeded);
        }

        [Fact]
        public async Task DeleteUserTest_ReturnsSuccessfulResult()
        {
            // Arrange
            _userToDelete.UserRoles.First().RoleId = Guid.Parse("C4986454-65DE-48AB-BE5A-BC6A0361DC00");

            _mockUnitOfWork.Setup(x => x.Users.FirstOrDefaultAsync(
                It.IsAny<IFilteringSpecification<User>>(),
                It.IsAny<IIncludeSpecification<User>>()))
                .Returns(Task.FromResult(_userToDelete));

            var request = new DeleteUserCommand();
            
            // Act
            var result = await _deleteUserCommandHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Succeeded);
        }
    }
}