﻿using AuRooM.Application.Features.Users.Queries.GetUser;
using AuRooM.Common.DTOs;
using AuRooM.Domain.Constants;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AuRooM.Domain.Settings;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AuRooM.Test.Tests.Users
{
    public class GetUserTest
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;
        private readonly Mock<IMapper> _mockMapper;
        private readonly Mock<IOptions<RoleSettings>> _mockRoleSettings;
        private readonly GetUserQueryHandler _getUserQueryHandler;
        private readonly User _user;
        private readonly List<UserRole> _userRoles;
        private readonly List<AcademicGroup> _groups;
        public GetUserTest()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();

            _mockMapper = new Mock<IMapper>();
            _mockMapper.Setup(x => x.Map<SelectListItem>(It.IsAny<SelectListItemDto>()))
               .Returns<SelectListItemDto>(
                   s => new SelectListItem(s.Text, s.Value));

            _mockRoleSettings = new Mock<IOptions<RoleSettings>>();
            _mockRoleSettings.Setup(x => x.Value)
               .Returns(new RoleSettings() { 
                   AdminRoleId = Guid.Parse("343364C2-6CCE-46EA-A2BF-D2A67C15188A"),
                   StudentRoleId = Guid.Parse("F2E8EA0E-5E75-42F1-9C26-81B3D04030DD")
               });

            _getUserQueryHandler = new GetUserQueryHandler(_mockUnitOfWork.Object, _mockMapper.Object, _mockRoleSettings.Object);

            _userRoles = new List<UserRole>()
            {
                new UserRole() { 
                    UserId = Guid.Parse("DA48370F-6443-4334-938F-51D8A179C330"),
                    RoleId = Guid.Parse("343364C2-6CCE-46EA-A2BF-D2A67C15188A"),
                    Role = new Role() { Name = "Admin" } 
                }
            };

            _user = new User()
            {
                Id = Guid.Parse("DA48370F-6443-4334-938F-51D8A179C330"),
                FirstName = "Anatoliy",
                LastName = "Muzychuk",
                UserStatus = UserStatus.Active,
                DocumentInfo = "111AM", 
                UserRoles  = _userRoles
            };

            _groups = new List<AcademicGroup>()
            {
                new AcademicGroup() { Id = Guid.Parse("5B9CB2A5-2B99-42D3-83C3-B78725B380A6"), Title = "PMI-31" },
                new AcademicGroup() { Id = Guid.Parse("0384FC75-48C1-469D-BDD4-7286E811733A"), Title = "PMI-32" },
                new AcademicGroup() { Id = Guid.Parse("7ECE3CC0-8FBF-40C5-99AE-535A81F8CFF0"), Title = "PMI-33" }
            };
        }

        [Fact]
        public async Task GetUserTest_ReturnsSuccessfulResult()
        {
            // Arrange
            _userRoles[0].RoleId = Guid.Parse("F2E8EA0E-5E75-42F1-9C26-81B3D04030DD");
            _user.AcademicGroup = _groups[0];

            _mockUnitOfWork.Setup(x => x.Users.FirstOrDefaultAsync(
                It.IsAny<IFilteringSpecification<User>>(),
                It.IsAny<IIncludeSpecification<User>>()
                ))
                .Returns(Task.FromResult(_user));

            _mockMapper.Setup(x => x.Map<EditUserVM>(It.IsAny<User>()))
                .Returns<User>(
                s => new EditUserVM()
                {
                    Id = s.Id,
                    DocumentInfo = s.DocumentInfo,
                    Email = s.Email,
                    FirstName = s.FirstName,
                    LastName = s.LastName,
                    GroupId = s.AcademicGroupId                    
                });

            _mockUnitOfWork.Setup(x => x.AcademicGroups.GetKeyValuePairsAsync(It.IsAny<IFilteringSpecification<AcademicGroup>>()))
                .Returns(Task.FromResult((IReadOnlyList<SelectListItemDto>)_groups.Select(
                    x => new SelectListItemDto()
                    {
                        Text = x.Title,
                        Value = x.Id.ToString()
                    }).ToList()));

            _mockUnitOfWork.Setup(x => x.Roles.GetKeyValuePairsAsync(It.IsAny<IFilteringSpecification<Role>>()))
                .Returns(Task.FromResult((IReadOnlyList<SelectListItemDto>)_userRoles.Select(
                    x => new SelectListItemDto()
                    {
                        Text = x.Role.Name,
                        Value = x.RoleId.ToString()
                    }).ToList()));

            var request = new GetUserQuery();

            // Act
            var result = await _getUserQueryHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Succeeded);
            Assert.Equal(3, result.Data.Groups.Count());
            Assert.Single(result.Data.Roles);
             
            foreach (var i in result.Data.Groups)
            {
                var group = _groups.FirstOrDefault(x => x.Id.ToString() == i.Value);
                Assert.Equal(group.Title, i.Text);          
            }

            foreach (var i in result.Data.Roles)
            {
                var role = _userRoles.FirstOrDefault(x => x.RoleId.ToString() == i.Value);
                Assert.Equal(role.Role.Name, i.Text);
            }
        }
    }
}
