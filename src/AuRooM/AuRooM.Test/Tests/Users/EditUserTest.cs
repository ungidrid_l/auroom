﻿using AuRooM.Application.Features.Users.Commands.Edit;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Settings;
using AutoMapper;
using Castle.DynamicProxy.Generators;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AuRooM.Test.Tests.Users
{
    public class EditUserTest
    {
        private readonly Mock<UserManager<User>> _mockUserManager;
        private readonly Mock<IMapper> _mockMapper;
        private readonly Mock<IOptions<RoleSettings>> _mockRoleSettings;
        private readonly EditUserCommandHandler _editUserCommandHandler;
        private readonly User _userToUpdate;
        private readonly List<UserRole> _roles;

        public EditUserTest()
        {
            _mockUserManager = new Mock<UserManager<User>>(
                new Mock<IUserStore<User>>().Object, null, null, null, null, null, null, null, null);
            
            _mockMapper = new Mock<IMapper>();

            _mockRoleSettings = new Mock<IOptions<RoleSettings>>();
            _mockRoleSettings.Setup(x => x.Value)
               .Returns(new RoleSettings()
               {
                   AdminRoleId = Guid.Parse("343364C2-6CCE-46EA-A2BF-D2A67C15188A"),
                   StudentRoleId = Guid.Parse("C4986454-65DE-48AB-BE5A-BC6A0361DC00"),
                   TeacherRoleId = Guid.Parse("5D22D7EC-6632-4D40-80C3-8B5803690720")
               });

            _editUserCommandHandler = new EditUserCommandHandler(_mockUserManager.Object, _mockMapper.Object, _mockRoleSettings.Object);

            _roles = new List<UserRole>()
            {
                new UserRole() { UserId = Guid.Parse("A78C1993-9159-491A-A9B1-21018EFF25B6"), RoleId = Guid.Parse("C4986454-65DE-48AB-BE5A-BC6A0361DC00") }
            };

            _userToUpdate = new User()
            {
                Id = Guid.Parse("A78C1993-9159-491A-A9B1-21018EFF25B6"),
                FirstName = "Admin",
                LastName = "Admin",
                UserStatus = UserStatus.Active,
                DocumentInfo = "Admin",
                AcademicGroupId = Guid.Empty,
                UserRoles = _roles
            };
        }

        [Fact]
        public async Task EditUserTest_IsTeacherAndHasGroupId_ReturnsFailure()
        {
            // Arrange
            var command = new EditUserCommand();

            command.RoleId = Guid.Parse("5D22D7EC-6632-4D40-80C3-8B5803690720");
            command.GroupId = Guid.Empty;

            // Act            
            var result = await _editUserCommandHandler.Handle(command, CancellationToken.None);

            // Assert
            Assert.False(result.Succeeded);
        }

        [Fact]
        public async Task EditUserTest_IsAdminAndHasGroupId_ReturnsFailure()
        {
            // Arrange
            var command = new EditUserCommand();

            command.RoleId = Guid.Parse("343364C2-6CCE-46EA-A2BF-D2A67C15188A");
            command.GroupId = Guid.Empty;

            // Act
            var result = await _editUserCommandHandler.Handle(command, CancellationToken.None);

            // Assert
            Assert.False(result.Succeeded);
        }

        [Fact]
        public async Task EditUserTest_ReturnsSuccessfulResult()
        {
            // Arrange
            _mockUserManager.Setup(x => x.FindByIdAsync(It.IsAny<string>()))
                .Returns(Task.FromResult(_userToUpdate));

            _mockUserManager.Setup(x => x.GetRolesAsync(It.IsAny<User>()))
                .Returns(Task.FromResult((IList<string>)new List<string>() { String.Empty, String.Empty }));

            _mockUserManager.Setup(x => x.RemoveFromRoleAsync(It.IsAny<User>(), It.IsAny<string>()));

            _mockMapper.Setup(x => x.Map(It.IsAny<EditUserCommand>(), It.IsAny<User>()))
                .Returns<EditUserCommand, User>((x, y) => new User()
                { 
                    Id = y.Id,                                                            
                });

            _mockUserManager.Setup(x => x.UpdateNormalizedEmailAsync(It.IsAny<User>()));

            _mockUserManager.Setup(x => x.UpdateNormalizedUserNameAsync(It.IsAny<User>()));

            _mockUserManager.Setup(x => x.UpdateAsync(It.IsAny<User>()))
                .Returns(Task.FromResult(new IdentityResult()));

            var command = new EditUserCommand();

            // Act
            var result = await _editUserCommandHandler.Handle(command, CancellationToken.None);

            // Assert
            Assert.True(result.Succeeded);
        }
    }
}
