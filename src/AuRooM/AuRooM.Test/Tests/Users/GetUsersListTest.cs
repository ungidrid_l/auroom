﻿using AuRooM.Application.Features.Users.Queries.GetUsersList;
using AuRooM.Application.Specifications;
using AuRooM.Domain.Entities;
using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using AutoMapper;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AuRooM.Test.Tests.Users
{
    public class GetUsersListTest
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;
        private readonly Mock<IMapper> _mockMapper;
        private readonly GetUsersListQueryHandler _getUsersListQueryHandler;
        private readonly List<User> _users;
        private readonly List<Role> _roles;
        private readonly List<Auditory> _auditories;
        private readonly List<UserRole> _userRoles;
        /*private readonly List<UserEvent> _userEvents;*/
        private readonly List<Event> _events;

        public GetUsersListTest()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();

            _mockMapper = new Mock<IMapper>();
            _mockMapper.Setup(x => x.Map<UserVM>(It.IsAny<User>()))
                .Returns<User>(u => new UserVM()
                {
                    Id = u.Id,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Email = u.Email,
                    DocumentInfo = u.DocumentInfo,
                    Group = u.AcademicGroup.Title,
                    Status = u.UserStatus.ToString(),
                });

            _getUsersListQueryHandler = new GetUsersListQueryHandler(_mockUnitOfWork.Object, _mockMapper.Object);            

            _users = new List<User>()
            {
                new User()
                {
                    Id = Guid.Parse("DA48370F-6443-4334-938F-51D8A179C330"),
                    FirstName = "Anatoliy",
                    LastName = "Muzychuk",
                    UserStatus = UserStatus.Active,
                    DocumentInfo = "111AM",                    
                },
                new User()
                {
                    Id = Guid.Parse("3358C87F-56E9-420F-B6FF-3A9A3675A2AA"),
                    FirstName = "Sviatoslav",
                    LastName = "Tarasiuk",
                    UserStatus = UserStatus.Active,
                    DocumentInfo = "111ST",                    
                },
                new User()
                {
                    Id = Guid.Parse("A78C1993-9159-491A-A9B1-21018EFF25B6"),
                    FirstName = "Admin",
                    LastName = "Admin",
                    UserStatus = UserStatus.Active,
                    DocumentInfo = "Admin",                    
                }
            };
        }

        [Fact]
        public async Task GetUsersListTest_ReturnsSuccessfulResult()
        {
            // Arrange
            _mockUnitOfWork.Setup(x => x.Users.GetAsync(
                It.IsAny<IFilteringSpecification<User>>(),
                It.IsAny<IIncludeSpecification<User>>(),
                It.IsAny<IPagingSpecification>(),
                It.IsAny<IOrderingSpecification<User>>()))
                .Returns(Task.FromResult((IReadOnlyList<User>)_users));

            _mockMapper.Setup(x => x.Map<UserVM>(It.IsAny<User>()))
                .Returns<User>(x => new UserVM()
                { 
                    Id = x.Id,
                    DocumentInfo = x.DocumentInfo,
                    FirstName  = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,                    
                });

            _mockUnitOfWork.Setup(x => x.Users.CountAsync(It.IsAny<IFilteringSpecification<User>>()))
                .Returns(Task.FromResult(_users.Count()));

            var request = new GetUsersListQuery();

            // Act
            var result = await _getUsersListQueryHandler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Succeeded);
            Assert.Equal(3, result.Data.UserList.Count());

            foreach (var i in result.Data.UserList)
            {
                var user = _users.FirstOrDefault(x => x.Id == i.Id);
                Assert.Equal(user.LastName, i.LastName);
                Assert.Equal(user.FirstName, i.FirstName);
                Assert.Equal(user.Email, i.Email);
                Assert.Equal(user.DocumentInfo, i.DocumentInfo);               
            }
        }
    }
}
