﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AuRooM.Common.Extensions
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<T1> Excpet<T1, T2>(this IEnumerable<T1> firstCollection, IEnumerable<T2> secondCollection, Func<T1, T2, bool> equalityComparer)
        {
            return firstCollection.Where(x => secondCollection.All(y => !equalityComparer(x, y)))
                .ToArray();
        }
    }
}