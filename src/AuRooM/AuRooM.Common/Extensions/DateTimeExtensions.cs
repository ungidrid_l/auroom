﻿using System;
using System.Collections.Generic;

namespace AuRooM.Common.Extensions
{
    public static class DateTimeExtensions
    {
        private static Dictionary<DayOfWeek, int> _daysFromMonday = new Dictionary<DayOfWeek, int> {
            { DayOfWeek.Monday, 0},
            {DayOfWeek.Tuesday, 1 },
            {DayOfWeek.Wednesday, 2 },
            {DayOfWeek.Thursday, 3 },
            {DayOfWeek.Friday, 4 },
            {DayOfWeek.Saturday, 5 },
            {DayOfWeek.Sunday, 6 }
        };

        public static (DateTime WeekStart, DateTime WeekEnd) WeekRange(this DateTime dateTime)
        {
            var weekStart = dateTime.AddDays(-_daysFromMonday[dateTime.DayOfWeek])
                .Date;

            var weekEnd = weekStart.AddDays(7)
                .AddSeconds(-1);

            return (weekStart, weekEnd);
        }

        public static bool OverlapsWith(
            this (DateTime Start, DateTime Finish) firstPeriod,
            (DateTime Start, DateTime Finish) secondPeriod) =>
            !(firstPeriod.Start >= secondPeriod.Finish || secondPeriod.Start >= firstPeriod.Finish);

        public static bool IsValidPeriod(this (DateTime Start, DateTime Finish) period) 
            => period.Start <= period.Finish;

        public static bool IsFutureDate(this DateTime date) 
            => date.Date >= DateTime.Now.Date;
    }
}