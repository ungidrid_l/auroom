﻿namespace AuRooM.Common.DTOs
{
    public class SelectListItemDto
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}