﻿using AuRooM.Common.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AuRooM.Common.Helpers
{
    public static class EnumHelper
    {
        public static IReadOnlyList<SelectListItemDto> EnumAsKeyValuePair<T>() where T : struct, Enum
        {
            return Enum.GetNames(typeof(T))
                .Select(
                    s => new SelectListItemDto()
                    {
                        Text = s,
                        Value = ((int)Enum.Parse(typeof(T), s)).ToString()
                    })
                .OrderBy(x => x.Text)
                .ToList();
        }
    }
}