﻿using System;

namespace AuRooM.Domain.Settings
{
    public class RoleSettings
    {
        public Guid AdminRoleId { get; set; }
        public Guid StudentRoleId { get; set; }
        public Guid PrefectRoleId { get; set; }
        public Guid TeacherRoleId { get; set; }
    }
}