﻿namespace AuRooM.Domain.Enums
{
    public enum ChangeRequestStatus
    {
        Pending,
        Approved,
        Declined
    }
}