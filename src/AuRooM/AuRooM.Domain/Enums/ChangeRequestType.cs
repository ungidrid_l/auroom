﻿namespace AuRooM.Domain.Enums
{
    public enum ChangeRequestType
    {
        Create,
        Update,
        Delete
    }
}