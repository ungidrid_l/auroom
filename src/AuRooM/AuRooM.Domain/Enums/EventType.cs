﻿namespace AuRooM.Domain.Enums
{
    public enum EventType
    {
        Class,
        Lecture,
        Other
    }
}