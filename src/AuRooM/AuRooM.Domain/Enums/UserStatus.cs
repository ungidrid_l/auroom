﻿namespace AuRooM.Domain.Enums
{
    public enum UserStatus
    {
        Pending,
        Active,
        Blocked
    }
}