﻿using System;

namespace AuRooM.Domain.Interfaces
{
    public interface IIdentifiable
    {
        Guid Id { get; set; }
    }
}