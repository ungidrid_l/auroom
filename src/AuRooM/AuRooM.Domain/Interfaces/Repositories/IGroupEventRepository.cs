﻿using AuRooM.Domain.Entities;

namespace AuRooM.Domain.Interfaces.Repositories
{
    public interface IGroupEventRepository : IRepository<GroupEvent>
    {
    }
}