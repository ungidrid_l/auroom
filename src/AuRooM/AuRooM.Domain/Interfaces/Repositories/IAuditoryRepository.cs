﻿using AuRooM.Common.DTOs;
using AuRooM.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuRooM.Domain.Interfaces.Repositories
{
    public interface IAuditoryRepository : IRepository<Auditory>
    {
        Task<IReadOnlyList<SelectListItemDto>> GetKeyValuePairsAsync(IFilteringSpecification<Auditory> specification = null);
    }
}