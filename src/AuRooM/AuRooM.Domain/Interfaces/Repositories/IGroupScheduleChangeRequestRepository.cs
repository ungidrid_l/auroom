﻿using AuRooM.Domain.Entities;

namespace AuRooM.Domain.Interfaces.Repositories
{
    public interface IGroupScheduleChangeRequestRepository : IRepository<GroupScheduleChangeRequest>
    {
    }
}
