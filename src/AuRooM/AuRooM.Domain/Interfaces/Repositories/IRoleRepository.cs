﻿using AuRooM.Common.DTOs;
using AuRooM.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuRooM.Domain.Interfaces.Repositories
{
    public interface IRoleRepository : IRepository<Role>
    {
        Task<IReadOnlyList<SelectListItemDto>> GetKeyValuePairsAsync(IFilteringSpecification<Role> specification = null);
    }
}
