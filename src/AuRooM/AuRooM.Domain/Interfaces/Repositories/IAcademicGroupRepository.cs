﻿using AuRooM.Common.DTOs;
using AuRooM.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuRooM.Domain.Interfaces.Repositories
{
    public interface IAcademicGroupRepository : IRepository<AcademicGroup>
    {
        Task<IReadOnlyList<SelectListItemDto>> GetKeyValuePairsAsync(IFilteringSpecification<AcademicGroup> specification = null);
    }
}