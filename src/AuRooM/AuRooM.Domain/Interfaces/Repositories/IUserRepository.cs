﻿using AuRooM.Common.DTOs;
using AuRooM.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuRooM.Domain.Interfaces.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        Task<IReadOnlyList<SelectListItemDto>> GetKeyValuePairsAsync(IFilteringSpecification<User> specification = null);
    }
}