﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuRooM.Domain.Interfaces.Repositories
{
    public interface IRepository<T> where T : class, IIdentifiable
    {
        Task<T> GetByIdAsync(Guid id);
        Task<IReadOnlyList<T>> GetAllAsync();

        Task<IReadOnlyList<T>> GetAsync(
            IFilteringSpecification<T> filter = null,
            IIncludeSpecification<T> include = null,
            IPagingSpecification paging = null,
            IOrderingSpecification<T> ordering = null);

        Task<T> FirstOrDefaultAsync(IFilteringSpecification<T> filter, IIncludeSpecification<T> include = null);
        Task<int> CountAsync(IFilteringSpecification<T> filter = null);
        void Add(T entity);
        void AddRange(IEnumerable<T> entities);
        void Remove(T entity);
        void RemoveRange(IEnumerable<T> entities);
    }
}