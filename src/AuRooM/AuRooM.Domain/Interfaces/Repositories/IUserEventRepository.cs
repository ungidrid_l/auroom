﻿using AuRooM.Domain.Entities;

namespace AuRooM.Domain.Interfaces.Repositories
{
    public interface IUserEventRepository : IRepository<UserEvent>
    {
    }
}