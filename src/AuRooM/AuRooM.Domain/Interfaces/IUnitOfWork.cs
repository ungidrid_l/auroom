﻿using AuRooM.Domain.Interfaces.Repositories;
using System;
using System.Threading.Tasks;

namespace AuRooM.Domain.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IAcademicGroupRepository AcademicGroups { get; }
        IAuditoryRepository Auditories { get; }
        IEventRepository Events { get; }
        IEventSetRepository EventSets { get; }
        IGroupEventRepository GroupEvents { get; }
        IGroupScheduleChangeRequestRepository GroupScheduleChangeRequests { get; }
        IScheduleChangeRequestRepository ScheduleChangeRequests { get; }
        IUserEventRepository UserEvents { get; }
        IUserRepository Users { get; }
        IRoleRepository Roles { get; }
        INarratorScheduleChangeRequestRepository NarratorScheduleChangeRequests { get; }

        Task CommitAsync();
    }
}