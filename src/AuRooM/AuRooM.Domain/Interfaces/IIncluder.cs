﻿using System.Linq;

namespace AuRooM.Domain.Interfaces
{
    public interface IIncluder<T>
    {
        IQueryable<T> Apply(IQueryable<T> query);
    }
}