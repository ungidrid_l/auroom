﻿namespace AuRooM.Domain.Interfaces
{
    public interface IPagingSpecification
    {
        int Skip { get; set; }
        int Take { get; set; }
    }
}