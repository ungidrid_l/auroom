﻿using System;
using System.Linq.Expressions;

namespace AuRooM.Domain.Interfaces
{
    public interface IOrderingSpecification<T>
    {
        Expression<Func<T, object>> ToExpression();
        OrderByType OrderByType { get; }
    }

    public enum OrderByType
    {
        Asc,
        Desc
    }
}