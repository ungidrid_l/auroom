﻿using System;
using System.Linq.Expressions;

namespace AuRooM.Domain.Interfaces
{
    public interface IFilteringSpecification<T>
    {
        IFilteringSpecification<T> And(IFilteringSpecification<T> right);
        IFilteringSpecification<T> Or(IFilteringSpecification<T> right);
        IFilteringSpecification<T> AndIf(bool condition, IFilteringSpecification<T> right);
        IFilteringSpecification<T> OrIf(bool condition, IFilteringSpecification<T> right);
        IFilteringSpecification<T> Negate();
        Expression<Func<T, bool>> ToExpression();
    }
}