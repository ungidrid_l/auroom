﻿namespace AuRooM.Domain.Interfaces
{
    public interface IIncludeSpecification<T>
    {
        IIncluder<T> Includer { get; set; }
    }
}