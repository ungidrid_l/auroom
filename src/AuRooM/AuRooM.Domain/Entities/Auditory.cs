﻿using AuRooM.Domain.Interfaces;
using System;
using System.Collections.Generic;

namespace AuRooM.Domain.Entities
{
    public class Auditory : IIdentifiable
    {
        public Guid Id { get; set; }
        public int Number { get; set; }

        public ICollection<Event> Events { get; set; } = new HashSet<Event>();
        public ICollection<ScheduleChangeRequest> ScheduleChangeRequests { get; set; } = new HashSet<ScheduleChangeRequest>();
    }
}