﻿using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace AuRooM.Domain.Entities
{
    public class User : IdentityUser<Guid>, IIdentifiable
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public UserStatus UserStatus { get; set; }

        public string DocumentInfo { get; set; }

        public Guid? AcademicGroupId { get; set; }

        public AcademicGroup AcademicGroup { get; set; }

        public ICollection<UserEvent> UserEvents { get; set; } = new HashSet<UserEvent>();

        public ICollection<UserRole> UserRoles { get; set; } = new HashSet<UserRole>();

        public ICollection<NarratorScheduleChangeRequest> NarratorScheduleChangeRequests { get; set; } = new HashSet<NarratorScheduleChangeRequest>();

        public ICollection<Event> EventsInCharge { get; set; } = new HashSet<Event>();

        public ICollection<ScheduleChangeRequest> RequestedEvents { get; set; } = new HashSet<ScheduleChangeRequest>();

        public string GetFullName()
        {
            return $"{FirstName} {LastName}";
        }
    }
}