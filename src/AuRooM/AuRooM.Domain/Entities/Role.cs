﻿using AuRooM.Domain.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace AuRooM.Domain.Entities
{
    public class Role : IdentityRole<Guid>, IIdentifiable
    {
        public Role() : base() {}
        public Role(string name) : base(name) {}

        public ICollection<UserRole> UserRoles { get; set; }
    }
}
