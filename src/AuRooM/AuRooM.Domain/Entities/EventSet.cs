﻿using AuRooM.Domain.Interfaces;
using System;
using System.Collections.Generic;

namespace AuRooM.Domain.Entities
{
    public class EventSet : IIdentifiable
    {
        public Guid Id { get; set; }
        public TimePeriod Period { get; set; }
        public int Frequency { get; set; }

        public ICollection<Event> Events { get; set; } = new HashSet<Event>();
    }
}
