﻿using AuRooM.Domain.Interfaces;
using System;

namespace AuRooM.Domain.Entities
{
    public class NarratorScheduleChangeRequest : IIdentifiable
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }

        public Guid ScheduleChangeRequestId { get; set; }
        public ScheduleChangeRequest ScheduleChangeRequest { get; set; }
    }
}
