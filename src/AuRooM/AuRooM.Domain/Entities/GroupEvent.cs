﻿using AuRooM.Domain.Interfaces;
using System;

namespace AuRooM.Domain.Entities
{
    public class GroupEvent : IIdentifiable
    {
        public Guid Id { get; set; }

        public Guid AcademicGroupId { get; set; }
        public AcademicGroup AcademicGroup { get; set; }

        public Guid EventId { get; set; }
        public Event Event { get; set; }

        public GroupEvent()
        {
        }

        public GroupEvent(Guid groupId, Guid eventId)
        {
            AcademicGroupId = groupId;
            EventId = eventId;
        }
    }
}