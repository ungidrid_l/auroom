﻿using System;

namespace AuRooM.Domain.Entities
{
    public class TimePeriod
    {
        public TimePeriod() {}

        public TimePeriod(DateTime start, DateTime finish)
        {
            Start = start;
            Finish = finish;
        }

        public DateTime Start { get; set; }
        public DateTime Finish { get; set; }
    }
}