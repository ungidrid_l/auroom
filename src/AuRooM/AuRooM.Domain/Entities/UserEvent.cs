﻿using AuRooM.Domain.Interfaces;
using System;

namespace AuRooM.Domain.Entities
{
    public class UserEvent : IIdentifiable
    {
        public Guid Id { get; set; }
        public bool IsNarrator { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }

        public Guid EventId { get; set; }
        public Event Event { get; set; }

        public UserEvent()
        {
        }

        public UserEvent(Guid userId, Guid eventId, bool isNarrator)
        {
            UserId = userId;
            EventId = eventId;
            IsNarrator = isNarrator;
        }
    }
}