﻿using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using System;
using System.Collections.Generic;

namespace AuRooM.Domain.Entities
{
    public class Event : IIdentifiable
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public TimePeriod Timing { get; set; }       
        public EventType Type { get; set; }

        public Guid PersonInChargeId { get; set; }
        public User PersonInCharge { get; set; }

        public Guid AuditoryId { get; set; }
        public Auditory Auditory { get; set; }

        public Guid EventSetId { get; set; }
        public EventSet EventSet { get; set; }

        public ICollection<GroupEvent> GroupEvents { get; set; } = new HashSet<GroupEvent>();
        public ICollection<UserEvent> UserEvents { get; set; } = new HashSet<UserEvent>();
        public ICollection<ScheduleChangeRequest> EventScheduleChangeRequests { get; set; } = new HashSet<ScheduleChangeRequest>();
    }
}