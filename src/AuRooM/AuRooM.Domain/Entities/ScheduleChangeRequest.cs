﻿using AuRooM.Domain.Enums;
using AuRooM.Domain.Interfaces;
using System;
using System.Collections.Generic;

namespace AuRooM.Domain.Entities
{
    public class ScheduleChangeRequest : IIdentifiable
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public TimePeriod Timing { get; set; }
        public TimePeriod Period { get; set; }
        public int Frequency { get; set; }
        public DateTime RequestDate { get; set; }
        public ChangeRequestStatus RequestStatus { get; set; }
        public ChangeRequestType RequestType { get; set; }
        public EventType EventType { get; set; }
        public bool IsAppliedForEventSet { get; set; }

        public Guid AuditoryId { get; set; }
        public Auditory Auditory { get; set; }

        public Guid? EventId { get; set; }
        public Event Event { get; set; }

        public Guid? RequestedById { get; set; }
        public User RequestedBy { get; set; }

        public Guid? StatusChangedById { get; set; }
        public User StatusChangedBy { get; set; }

        public ICollection<GroupScheduleChangeRequest> GroupScheduleChangeRequests { get; set; } = new HashSet<GroupScheduleChangeRequest>();
        public ICollection<NarratorScheduleChangeRequest> NarratorScheduleChangeRequests { get; set; } = new HashSet<NarratorScheduleChangeRequest>();
    }
}