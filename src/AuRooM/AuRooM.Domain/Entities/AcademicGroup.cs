﻿using AuRooM.Domain.Interfaces;
using System;
using System.Collections.Generic;

namespace AuRooM.Domain.Entities
{
    public class AcademicGroup : IIdentifiable
    {
        public Guid Id { get; set; }
        public string Title { get; set; }

        public ICollection<User> Students { get; set; } = new HashSet<User>();
        public ICollection<GroupEvent> GroupEvents { get; set; } = new HashSet<GroupEvent>();
        public ICollection<GroupScheduleChangeRequest> GroupScheduleChangeRequests { get; set; } = new HashSet<GroupScheduleChangeRequest>();
    }
}