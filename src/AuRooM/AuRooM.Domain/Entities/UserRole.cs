﻿using Microsoft.AspNetCore.Identity;
using System;

namespace AuRooM.Domain.Entities
{
    public sealed class UserRole : IdentityUserRole<Guid>
    {
        public UserRole() {}

        public UserRole(Guid userId, Guid roleId)
        {
            UserId = userId;
            RoleId = roleId;
        }

        public Role Role { get; set; }

        public User User { get; set; }
    }
}