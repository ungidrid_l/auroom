﻿using AuRooM.Domain.Interfaces;
using System;

namespace AuRooM.Domain.Entities
{
    public class GroupScheduleChangeRequest : IIdentifiable
    {
        public Guid Id { get; set; }

        public Guid AcademicGroupId { get; set; }
        public AcademicGroup AcademicGroup { get; set; }

        public Guid ScheduleChangeRequestId { get; set; }
        public ScheduleChangeRequest ScheduleChangeRequest { get; set; }
    }
}
