using AuRooM.Application.Features.System.Commands.Seed;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using System.Threading.Tasks;

namespace AuRooM.Web
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args)
                .UseSerilog()
                .Build();

            using (var scope = host.Services.CreateScope())
            {
                // Seed default users and roles
                var services = scope.ServiceProvider;
                var mediator = services.GetRequiredService<IMediator>();
                await mediator.Send(new SeedIdentityCommand());

                // Configure Logger
                var configuration = services.GetRequiredService<IConfiguration>();
                Log.Logger = new LoggerConfiguration()
                    .ReadFrom.Configuration(configuration)
                    .CreateLogger();
            }

            try
            {
                host.Run();
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(
                    webBuilder =>
                    {
                        webBuilder.UseStartup<Startup>();
                    });
    }
}