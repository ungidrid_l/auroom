﻿using AuRooM.Application.Common.Models;
using AuRooM.Application.Common.Permissions;
using AuRooM.Application.Features.Events.Commands.Approve;
using AuRooM.Application.Features.Events.Commands.ApproveDelete;
using AuRooM.Application.Features.Events.Commands.ApproveRequestChange;
using AuRooM.Application.Features.Events.Commands.Decline;
using AuRooM.Application.Features.Events.Queries.GetChangeRequestsList;
using AuRooM.Web.Extensions;
using AuRooM.Web.Filters;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AuRooM.Web.Controllers
{
    public class EventControlController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public EventControlController(IMediator mediator, IToastNotification toastNotification, IMapper mapper) : base(toastNotification)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        [HttpGet]
        [ImportModelState]
        [HasPermission(EventPermissions.ViewEventRequests, EventPermissions.ViewRelatedEventRequests)]
        public async Task<IActionResult> Control([FromQuery]EventControlFilterVM eventControlFilterVm, [FromQuery]PaginationModel pagination)
        {
            var query = _mapper.Map<EventControlFilterVM, GetChangeRequestsListQuery>(eventControlFilterVm);
            query.UserId = User.Identity.GetUserId();
            query.Page = pagination.Page;
            query.PageSize = pagination.PageSize;
            var result = await _mediator.Send(query);
            return View(result.Data);
        }

        [HttpPost]
        [ExportModelState]
        [HasPermission(EventPermissions.ApproveChanges)]
        public async Task<IActionResult> Approve(Guid id)
        {
            var command = new ApproveEventCommand() { Id = id, ApprovedById = User.Identity.GetUserId() };
            var result = await _mediator.Send(command);

            result.AddErrorsToModelState(ModelState);
            return RedirectToAction(nameof(Control));
        }

        [HttpPost]
        [ExportModelState]
        [HasPermission(EventPermissions.ApproveChanges)]
        public async Task<IActionResult> Decline(Guid id)
        {
            var command = new DeclineEventCommand() { Id = id, DeclinedById = User.Identity.GetUserId() };
            var result = await _mediator.Send(command);

            result.AddErrorsToModelState(ModelState);
            return RedirectToAction(nameof(Control));
        }

        [HttpPost]
        [ExportModelState]
        [HasPermission(EventPermissions.ApproveChanges)]
        public async Task<IActionResult> ApproveRequestChange(Guid id)
        {
            var command = new ApproveRequestChangeCommand() { Id = id, ApprovedById = User.Identity.GetUserId() };
            var result = await _mediator.Send(command);

            result.AddErrorsToModelState(ModelState);
            return RedirectToAction(nameof(Control));
        }

        [HttpPost]
        [ExportModelState]
        [HasPermission(EventPermissions.ApproveChanges)]
        public async Task<IActionResult> ApproveDelete(Guid id)
        {
            var command = new ApproveDeleteCommand() { ScheduleChangeRequestId = id, ApprovedById = User.Identity.GetUserId() };
            var result = await _mediator.Send(command);
            result.AddErrorsToModelState(ModelState);
            return RedirectToAction(nameof(Control));
        }
    }
}
