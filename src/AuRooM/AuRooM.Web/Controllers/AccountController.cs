﻿using AuRooM.Application.Common.Permissions;
using AuRooM.Application.Features.Auth.Commands.Login;
using AuRooM.Application.Features.Auth.Commands.Logout;
using AuRooM.Application.Features.Auth.Commands.Register;
using AuRooM.Application.Features.Auth.Queries.GetRegistrationModel;
using AuRooM.Web.Extensions;
using AuRooM.Web.Filters;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using System.Threading.Tasks;

namespace AuRooM.Web.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public AccountController(IMediator mediator, IMapper mapper, IToastNotification toastNotification) : base(toastNotification)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        // GET: /<controller>/
        [HttpGet]
        [ImportModelState]
        public async Task<IActionResult> Register()
        {
            var query = new GetRegistrationModelQuery();
            var result = await _mediator.Send(query);
            var model = result.Succeeded ? result.Data : null;
            return View(model);
        }

        [HttpPost]
        [ExportModelState]
        [Validate(nameof(Register))]
        public async Task<IActionResult> Register(RegisterVM vm)
        {
            var command = _mapper.Map<RegisterCommand>(vm);
            var result = await _mediator.Send(command);

            result.AddErrorsToModelState(ModelState);
            return result.Succeeded 
                       ? RedirectToAction("Agenda", "Event") 
                       : RedirectToAction(nameof(Register));
        }

        // GET: /<controller>/
        [HttpGet]
        [ImportModelState]
        public IActionResult Login(string returnUrl)
        {
            var model = new LoginVM
            {
                ReturnUrl = returnUrl
            };

            return View(model);
        }

        [HttpPost]
        [ExportModelState]
        [Validate(nameof(Login))]
        public async Task<IActionResult> Login(LoginVM vm)
        {
            var command = _mapper.Map<LoginCommand>(vm);
            var result = await _mediator.Send(command);

            if (!result.Succeeded)
            {
                result.AddErrorsToModelState(ModelState);
                return RedirectToAction(nameof(Login));
            }

            var returnUrlExists = ValidateReturnUrl(vm.ReturnUrl);

            return returnUrlExists ? Redirect(vm.ReturnUrl) : (IActionResult)RedirectToAction("Agenda", "Event");
        }

        [HttpPost]
        [HasPermission(UserPermissions.LogOut)]
        public async Task<IActionResult> Logout()
        {
            await _mediator.Send(new LogoutCommand());
            return RedirectToAction("Agenda", "Event");
        }
    }
}