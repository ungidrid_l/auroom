using AuRooM.Application.Common.Models;
using AuRooM.Application.Common.Permissions;
using AuRooM.Application.Features.Users.Commands.Delete;
using AuRooM.Application.Features.Users.Commands.Edit;
using AuRooM.Application.Features.Users.Queries.GetUser;
using AuRooM.Application.Features.Users.Queries.GetUsersList;
using AuRooM.Web.Extensions;
using AuRooM.Web.Filters;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using System;
using System.Threading.Tasks;

namespace AuRooM.Web.Controllers
{
    public class UserController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        public UserController(IMediator mediator, IToastNotification toastNotification, IMapper mapper) : base(toastNotification)
        {
            _mediator = mediator;
            _mapper = mapper;
        }
        
        [HttpGet]
        [ImportModelState]
        [HasPermission(UserPermissions.ViewUsers)]
        public async Task<IActionResult> Index([FromQuery]PaginationModel pagination)
        {
            var query = new GetUsersListQuery
            {
                Page = pagination.Page,
                PageSize = pagination.PageSize
            };
            var result = await _mediator.Send(query);
            return View(result.Data);
        }
        
        [HttpPost]
        [ExportModelState]
        [HasPermission(UserPermissions.RemoveUsers)]
        public async Task<IActionResult> Delete(Guid id)
        {
            var command = new DeleteUserCommand {Id = id};
            var result = await _mediator.Send(command);
            result.AddErrorsToModelState(ModelState);
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        [ImportModelState]
        [HasPermission(UserPermissions.EditUsers)]
        public async Task<IActionResult> Edit(Guid id)
        {
            var query = new GetUserQuery {Id = id};
            var result = await _mediator.Send(query);
            return result.Succeeded
                ?  View(result.Data)
                : (IActionResult)RedirectToAction("Index", "User");
        }

        [HttpPost]
        [ExportModelState]
        [HasPermission(UserPermissions.EditUsers)]
        public async Task<IActionResult> Edit(EditUserVM user)
        {
            var command = _mapper.Map<EditUserCommand>(user);
            var result = await _mediator.Send(command);
            result.AddErrorsToModelState(ModelState);
            return RedirectToAction("Index", "User");
        }
    }
}