using AuRooM.Application.Common.Models;
using AuRooM.Application.Common.Permissions;
using AuRooM.Application.Features.Events.Commands.Create;
using AuRooM.Application.Features.Events.Commands.Delete;
using AuRooM.Application.Features.Events.Commands.RequestChange;
using AuRooM.Application.Features.Events.Commands.Subscribe;
using AuRooM.Application.Features.Events.Queries.GetAgenda;
using AuRooM.Application.Features.Events.Queries.GetCreateEventModel;
using AuRooM.Application.Features.Events.Queries.GetRequestChangeModel;
using AuRooM.Application.Features.Events.Queries.GetSubscribeViewModel;
using AuRooM.Web.Extensions;
using AuRooM.Web.Filters;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using System;
using System.Threading.Tasks;

namespace AuRooM.Web.Controllers
{
    public class EventController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public EventController(IMediator mediator, IToastNotification toastNotification, 
            IMapper mapper) : base(toastNotification)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        [HttpGet]
        [ImportModelState]
        [HasPermission(EventPermissions.RequestCreation)]
        public async Task<IActionResult> Create()
        {
            var query = new GetCreateEventModelQuery();
            var result = await _mediator.Send(query);
            var model = result.Succeeded ? result.Data : null;
            return View(model);
        }

        [HttpPost]
        [ExportModelState]
        [HasPermission(EventPermissions.RequestCreation)]
        [Validate(nameof(Create))]
        public async Task<IActionResult> Create(CreateEventVM vm)
        {
            var command = _mapper.Map<CreateEventCommand>(vm);
            command.RequestedById = User.Identity.GetUserId();
            var result = await _mediator.Send(command);

            result.AddErrorsToModelState(ModelState);
            return result.Succeeded
                       ? RedirectToAction("Agenda", "Event")
                       : RedirectToAction(nameof(Create));
        }

        [HttpGet]
        [ImportModelState]
        public async Task<IActionResult> Subscribe(Guid id)
        {
            var query = new GetEventSetApplicationTypeVmQuery(id);
            var result = await _mediator.Send(query);
            result.Data.SubmitAction = nameof(Subscribe);
            result.Data.SubmitController = "Event";
            var modalConfig = new ModalConfig
            {
                Header = "Subscribe to event",
                SubmitText = "Subscribe",
                BodyName = "_SelectEventSetApplicationTypeModal",
                ModalId = "...",
                BodyModel = result.Data
            };
            return PartialView("_Modal", modalConfig);
        }

        [HttpPost]
        [ExportModelState]
        [HasPermission(EventPermissions.Subscribe)]
        public async Task<IActionResult> Subscribe(EventSetApplicationTypeVm eventSetApplicationTypeVm)
        {
            var command = _mapper.Map<SubscribeToEventCommand>(eventSetApplicationTypeVm);
            command.UserId = User.Identity.GetUserId();
            var result = await _mediator.Send(command);

            result.AddErrorsToModelState(ModelState);
            return RedirectToAction("Agenda", "Event");
        }

        [HttpGet]
        [ImportModelState]
        public async Task<IActionResult> Delete(Guid id)
        {
            var query = new GetEventSetApplicationTypeVmQuery(id);
            var result = await _mediator.Send(query);
            result.Data.SubmitAction = nameof(Delete);
            result.Data.SubmitController = "Event";
            var modalConfig = new ModalConfig
            {
                Header = "Delete Event",
                SubmitText = "Delete",
                BodyName = "_SelectEventSetApplicationTypeModal",
                ModalId = "...",
                BodyModel = result.Data
            };
            return PartialView("_Modal", modalConfig);
        }

        [HttpPost]
        [ExportModelState]
        [HasPermission(EventPermissions.RequestChanges)]
        public async Task<IActionResult> Delete(EventSetApplicationTypeVm eventSetApplicationTypeVm)
        {
            var command = _mapper.Map<DeleteEventCommand>(eventSetApplicationTypeVm);
            command.UserId = User.Identity.GetUserId();
            var result = await _mediator.Send(command);

            result.AddErrorsToModelState(ModelState);
            return RedirectToAction(nameof(Agenda));
        }

        [HttpGet]
        [ImportModelState]
        public async Task<IActionResult> Agenda([FromQuery]GetAgendaVM vm)      
        {            
            var query = _mapper.Map<GetAgendaQuery>(vm);
            query.UserId = User.Identity.GetUserId();            
            var result = await _mediator.Send(query);
            var model = result.Succeeded ? result.Data : null;
            return View(model);
        }

        [HttpGet]
        [ImportModelState]
        [HasPermission(EventPermissions.RequestChanges)]
        public async Task<IActionResult> RequestChange(Guid id)
        {
            var query = new GetRequestChangeModelQuery(id);
            var result = await _mediator.Send(query);
            var model = result.Succeeded ? result.Data : null;
            return View(model);
        }

        [HttpPost]
        [ExportModelState]
        [HasPermission(EventPermissions.RequestChanges)]
        [Validate(nameof(RequestChange))]
        public async Task<IActionResult> RequestChange(RequestChangeVM vm)
        {
            var command = _mapper.Map<RequestChangeCommand>(vm);
            command.RequestedById = User.Identity.GetUserId();
            var result = await _mediator.Send(command);

            result.AddErrorsToModelState(ModelState);
            return result.Succeeded
                       ? RedirectToAction("Agenda", "Event")
                       : RedirectToAction(nameof(RequestChange));
        }
    }    
}