﻿using AuRooM.Web.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using NToastNotify;
using System;
using System.Linq;
using System.Security.Claims;

namespace AuRooM.Web.Controllers
{
    public class BaseController : Controller
    {
        private readonly IToastNotification _toastNotification;

        protected BaseController(IToastNotification toastNotification)
        {
            _toastNotification = toastNotification;
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);

            if (!context.Filters.Contains(new ImportModelStateAttribute()) || context.ModelState.ErrorCount == 0)
                return;

            ModelState.Values.Where(x => x.ValidationState == ModelValidationState.Invalid)
                .SelectMany(x => x.Errors.Select(e => e.ErrorMessage))
                .ToList()
                .ForEach(e => _toastNotification.AddErrorToastMessage(e));
        }

        protected bool ValidateReturnUrl(string returnUrl)
        {
            return !string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl);
        }
    }
}