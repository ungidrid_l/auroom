﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System;
using System.ComponentModel;

namespace AuRooM.Web.Extensions
{
    public static class UrlExtensions
    {
        public static string ActionPreserveQueryParams(this IUrlHelper helper, string action, String controller, object substitutes)
        {
            var routeData = new RouteValueDictionary(helper.ActionContext.RouteData.Values)
            {
                ["controller"] = controller,
                ["action"] = action
            };

            foreach (var queryValue in helper.ActionContext.HttpContext.Request.Query)
            {
                if (routeData.ContainsKey(queryValue.Key))
                    continue;

                routeData.Add(queryValue.Key, queryValue.Value);
            }

            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(substitutes.GetType()))
            {
                var value = property.GetValue(substitutes);
                if (string.IsNullOrEmpty(value?.ToString()))
                {
                    routeData.Remove(property.Name);
                    continue;
                }

                if (routeData.ContainsKey(property.Name))
                    routeData[property.Name] = value;
                else
                    routeData.Add(property.Name, value);
            }

            return helper.RouteUrl(routeData);
        }
    }
}