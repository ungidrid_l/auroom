﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace AuRooM.Web.Extensions
{
    public static class ClaimsIdentityExtensions
    {
        public static Guid GetUserId(this IIdentity identity)
        {                    
            var result = Guid.TryParse((identity as ClaimsIdentity).Claims
                .FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value, out var contactId);
            return result ? contactId : Guid.Empty;
        }
    }
}
