﻿using AuRooM.Application.Common.Models;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Linq;

namespace AuRooM.Web.Extensions
{
    public static class ResultExtensions
    {
        public static void AddErrorsToModelState(this Result result, ModelStateDictionary modelState)
        {
            if (!result.Succeeded)
            {
                result.Errors.ToList()
                    .ForEach(x => modelState.AddModelError(string.Empty, x));
            }
        }
    }
}