using AuRooM.Application;
using AuRooM.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NToastNotify;
using Serilog;

namespace AuRooM.Web
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IWebHostEnvironment env)
        {
            var configuration = new ConfigurationBuilder().SetBasePath(basePath: env.ContentRootPath)
                .AddJsonFile("Settings/role_settings.json", false, true);

            if (env.IsDevelopment())
            {
                configuration.AddJsonFile("appsettings.json", false, true);
            }

            if (env.IsProduction())
            {
                configuration.AddJsonFile("appsettings.Production.json", false, true);
            }

            Configuration = configuration.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddNToastNotifyToastr(
                    new ToastrOptions
                    {
                        ProgressBar = false,
                        PositionClass = ToastPositions.TopRight
                    });

            services.AddInfrastructure(Configuration);
            services.AddApplication();
            services.AddControllersWithViews()
                .AddRazorRuntimeCompilation();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseSerilogRequestLogging();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseNToastNotify();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Event}/{action=Agenda}/{id?}");
            });
        }
    }
}
