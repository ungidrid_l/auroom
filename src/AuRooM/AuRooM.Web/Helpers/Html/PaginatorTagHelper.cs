﻿using AuRooM.Web.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.IO;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace AuRooM.Web.Helpers.Html
{
    [HtmlTargetElement("paginator")]
    public class PaginatorTagHelper : TagHelper
    {
        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewContext { get; set; }

        private readonly IHtmlHelper _htmlHelper;
        private readonly HtmlEncoder _htmlEncoder;

        public PaginatorTagHelper(IHtmlHelper htmlHelper, HtmlEncoder htmlEncoder)
        {
            _htmlHelper = htmlHelper;
            _htmlEncoder = htmlEncoder;
        }

        public int Page { get; set; }

        public int Count { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            (_htmlHelper as IViewContextAware)?.Contextualize(ViewContext);

            var potentialMinPage = Page - 1;
            var potentialMaxPage = Page + 1;

            var model = new PaginatorPartialModel()
            {
                MinPage = potentialMinPage < 1 ? 1 : potentialMinPage,
                MaxPage = potentialMaxPage > Count ? Count : potentialMaxPage,
                ShowPrev = Page != 1,
                ShowNext = Page != Count,
                ShowFirst = Page > 2,
                ShowLast = Page <= Count - 2,
                Controller = Controller,
                Action = Action,
                Page = Page,
                Count = Count
            };

            var partial = await _htmlHelper.PartialAsync("_Paginator", model);
            var writter = new StringWriter();
            partial.WriteTo(writter, _htmlEncoder);
            output.Content.SetHtmlContent(writter.ToString());
        }
    }
}