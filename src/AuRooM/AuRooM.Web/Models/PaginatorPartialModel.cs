﻿namespace AuRooM.Web.Models
{
    public class PaginatorPartialModel
    {
        public string Action { get; set; }
        public string Controller { get; set; }
        public int Page { get; set; }
        public int Count { get; set; }
        public bool ShowFirst { get; set; }
        public bool ShowLast { get; set; }
        public bool ShowNext { get; set; }
        public bool ShowPrev { get; set; }
        public int MinPage { get; set; }
        public int MaxPage { get; set; }
    }
}