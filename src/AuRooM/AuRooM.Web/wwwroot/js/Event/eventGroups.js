
const eventGroups = function () {
    const init = () => {
        const groupLists = $(".eventGroupColumn");
        for(let i =0; i< groupLists.length; i++ ){
            if(groupLists[i].children.length > 2){
                const groups = groupLists[i].cloneNode(true);
                groupLists[i].innerHTML = `<div class='showMore' id='showMore${i}'><span >...</span><div class="showMoreGroups">${groups.innerHTML}</div></div>`;
                let showMore = $(`#showMore${i}> `);
                let showMoreDiv = $(`#showMore${i} div`);
                showMore.on('mouseenter', ()=> showMoreDiv.css({display: 'block'}));
                showMore.on('mouseleave', ()=> showMoreDiv.css({display: 'none'}));
            }
        }
    };

    return {init}
}();