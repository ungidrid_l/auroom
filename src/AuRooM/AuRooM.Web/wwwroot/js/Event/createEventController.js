﻿const createEventController = function () {
    
    const select2Init = () => {
        $('#auditorySelect').select2({
            placeholder: 'Select an auditory'
        });

        $('#groupSelect').select2({
            placeholder: 'Select a group',
            multiple: true
        });

        $('#eventTypeSelect').select2({
            placeholder: 'Select event type'
        });

        $('#teacherSelect').select2({
            placeholder: 'Select a teacher',
            multiple: true
        });

        $('#narratorsSelect').select2({
            placeholder: 'Select a narrator',
            multiple: true
        });

        const teacherSelect = $('#teacherSelect').closest('div');
        const narratorSelect = $('#narratorsSelect').closest('div');
        narratorSelect.addClass('d-none');

        $('#eventTypeSelect').on('change',
            (event) => {
                const currentValue = event.target.value;
                

                switch (currentValue) {
                case '2':
                    teacherSelect.addClass('d-none');
                    narratorSelect.removeClass('d-none');
                    break;
                default:
                    teacherSelect.removeClass('d-none');
                    narratorSelect.addClass('d-none');
                };
            });
    };
    const init = () => {
        
        select2Init();
        const today = new Date().toISOString().split('T')[0];
        const [periodStart,periodFinish, frequency,eventSetToggler, periodStartGroup, periodFinishGroup,frequencyGroup] = 
            [$("#Period_Start"),$("#Period_Finish"),$('#Frequency'),$('#changeEventSetToggler'),
                $('#startPeriodGroup'),$('#finishPeriodGroup'), $('#frequencyGroup')];
        periodStart[0].setAttribute('min', today);
        eventSetToggler.on('change', (e) => {
            periodFinish.prop("disabled", () => !e.target.checked);
            frequency.prop("disabled", () => !e.target.checked);
            periodStartGroup.toggle(e.target.checked);
            periodFinishGroup.toggle(e.target.checked);
            frequencyGroup.toggle(e.target.checked);
        });
        periodStart.on('change', (e) => periodFinish[0].setAttribute('min', e.target.value));
        periodFinish[0].setAttribute('min', periodFinish[0].value);
        eventSetToggler.trigger('change');
        
    };

    return {init}
}();