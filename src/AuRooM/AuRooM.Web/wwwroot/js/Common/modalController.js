﻿const modalController = function () {
    let modalplaceholderElement;

    const onSave = (event) => {
        event.preventDefault();
        const form = $(event.target).parents('.modal').find('form');
        form.submit();
    }

    const showModal = (event) => {
        const url = $(event.target).data('url');
        $.get(url).done(data => {
            modalplaceholderElement.html(data);
            modalplaceholderElement.find('.modal').modal('show');
        });
    }

    const init = () => {
        modalplaceholderElement = $('#modal-placeholder');
        $('button[data-toggle="ajax-modal"]').click(showModal);
        $('a[data-toggle="ajax-modal"]').click(showModal);
        modalplaceholderElement.on('click', '[data-save="modal"]', onSave);
    };

    return { init };
}();