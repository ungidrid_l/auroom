const roleService = function () {
    const isTeacherRole = (id) => {
        return id === "9b41f137-e19d-4bdb-a86c-f9191b5cd730".toUpperCase();
    };
    const isAdminRole = (id) => {
        return id === "343364c2-6cce-46ea-a2bf-d2a67c15188a".toUpperCase();
    };
    return { isTeacherRole, isAdminRole };
}();
