const scheduleController = function () {
    const values = {
        auditorySelect: undefined,
        teacherSelect : undefined
    };
    const teacherSelect = $('#teacherSelect');
    const auditorySelect = $('#auditorySelect');
    const notMyCheckbox = $('#notMyContainer input');
    const toggleNotMyBtn = (select) => {
        values[select.id] = select.value;
        const isActiveFilter = Object.values(values).find(value => value);
        $('#notMyContainer').css({'visibility': isActiveFilter? "visible" : "hidden"});
        notMyCheckbox.prop("disabled", (i, v) => !isActiveFilter);
        
    };
    const select2Init = () => {
        teacherSelect.select2({
            placeholder: 'Select a narrator',
            allowClear: true
        });

        auditorySelect.select2({
            placeholder: 'Select an auditory',
            allowClear: true
        });
        
    };

    const init = () => {
        select2Init();
        const groupLists = $(".teacherList");
        const notifyBtn = $('#notifyBtn');
        for(let i =0; i< groupLists.length; i++ ){
            if(groupLists[i].children.length > 2){
                const groups = groupLists[i].cloneNode(true);
                groupLists[i].innerHTML = `<div class='showMore' id='showMore${i}' style="position: relative"><span >...</span><div class="showMoreTeachers">${groups.innerHTML}</div></div>`;
                let showMore = $(`#showMore${i}`);
                let showMoreDiv = $(`#showMore${i} div`);
                groupLists.on('mouseenter', ()=> showMoreDiv.css({display: 'flex'}).css({flexDirection: "column"}));
                groupLists.on('mouseleave', ()=> showMoreDiv.css({display: 'none'}));
            }
        }
        notifyBtn.on('mouseenter',()=> {
            $('#notifyBtn div')[0].classList.remove('d-none');
        })
        notifyBtn.on('mouseleave',()=> {
            $('#notifyBtn div')[0].classList.add('d-none');
        });
        teacherSelect.on("select2:select", function(){
            toggleNotMyBtn({id: this.id, value: this.value});
        });
        auditorySelect.on("select2:select", function(){
            toggleNotMyBtn({id: this.id, value: this.value});
        });
        auditorySelect.on("select2:clear", function(){
            toggleNotMyBtn({id: this.id, value: null});
        });
        teacherSelect.on("select2:clear", function(){
            toggleNotMyBtn({id: this.id, value: null});
        });

        teacherSelect.trigger('select2:select')
        auditorySelect.trigger('select2:select')
    };

    return {init}
}();