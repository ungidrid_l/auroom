﻿const editUserController = function (roleService) {

    const disableGroupIfRoleTeacherOrAdmin = (role) => {
        const groupSelect = $("#groupSelect");
        const isTeacher = roleService.isTeacherRole(role);
        const isAdmin = roleService.isAdminRole(role);
        groupSelect.prop("disabled", (i, v) => isTeacher || isAdmin);
        groupSelect.parent().parent().toggle(!isTeacher && !isAdmin);
    };

    const select2Init = () => {
        $('#roleSelect').select2({
            placeholder: 'Select a role'
        });

        $('#statusSelect').select2({
            placeholder: 'Select a status'
        });

        $('#groupSelect').select2({
            placeholder: 'Select a group'
        });
    }

    const init = () => {
        select2Init();
        const roleSelect = $("#roleSelect");
        roleSelect.on("select2:select", function(){
            disableGroupIfRoleTeacherOrAdmin(this.value);
        });

        roleSelect.trigger("select2:select");
        
    };

    return { init };
}(roleService);