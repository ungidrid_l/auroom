const registerController = function(roleService) {
    const disableGroupForTeacher = () => {
        $("#roleSelect").on('select2:select',
            function (){
                const groupSelect = $("#groupSelect");
                const isTeacher = roleService.isTeacherRole(this.value);
                groupSelect.prop("disabled", (i, v) => isTeacher);
                groupSelect.parent().parent().toggle(!isTeacher);
            });
    };

    const select2Init = () => {
        $('#roleSelect').select2({
            placeholder: 'Select a role'
        });

        $('#groupSelect').select2({
            placeholder: 'Select a group'
        });
    }

    const init = () => {
        select2Init();
        disableGroupForTeacher();
        $("#roleSelect").trigger('select2:select');
        
    };
    return { init };
}(roleService);