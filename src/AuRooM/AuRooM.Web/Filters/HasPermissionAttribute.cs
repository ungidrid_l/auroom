﻿using AuRooM.Infrastructure.Auth;
using Microsoft.AspNetCore.Authorization;

namespace AuRooM.Web.Filters
{
    public class HasPermissionAttribute : AuthorizeAttribute
    {
        public HasPermissionAttribute(params string[] permissions)
        {
            Policy = $"{PermissionConstants.PermissionPolicyPrefix}{string.Join(PermissionConstants.PermissionSplitSymbol, permissions)}";
        }
    }
}