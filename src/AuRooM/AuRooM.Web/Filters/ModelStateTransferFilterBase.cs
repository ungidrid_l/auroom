﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace AuRooM.Web.Filters
{
    public abstract class ModelStateTransferFilterBase : ActionFilterAttribute
    {
        protected const string Key = nameof(ModelStateTransferFilterBase);
    }
}