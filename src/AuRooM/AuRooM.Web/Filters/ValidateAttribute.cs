﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace AuRooM.Web.Filters
{
    public class ValidateAttribute : ActionFilterAttribute
    {
        private readonly string _action;
        private readonly string _controller;

        public ValidateAttribute(string action, string controller = null)
        {
            _action = action;
            _controller = controller;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            if (!context.ModelState.IsValid)
            {
                context.Result = new RedirectToActionResult(_action, _controller, context.RouteData.Values);
            }
        }
    }
}